#include <stdio.h>
#include <stdint.h>

/*
python >>>

n = 5
s = 2065

s2 = s-(n*8+22)//16*16
p = (s2+7)//8*8
e2 = p -s2
e1 = s - s2 - e2 - n*8

s2, p, e1, e2
(2017, 2024, 1, 7)



n = 6
s = 2064

s2 = s-(n*8+22)//16*16
p = (s2+7)//8*8
e2 = p -s2
e1 = s - s2 - e2 - n*8

s2, p, e1, e2
(2000, 2000, 16, 0)

long vframe(long n, long idx, long *q)
{
    long i;
    long *p[n];
    p[0] = &i;
    for (i = 1; i < n; i++)
        p[i] = q;
    return *p[idx];
}
*/

int64_t vframe(int64_t n, int64_t idx, int64_t *q)
{
   int64_t i;
   int64_t *p[n];
   p[0] = &i;
   for (i = 1; i < n; i++)
      p[i] = q;
   return *p[idx];
}

int main(int argc, char *argv[])
{
   printf("Hello, World!\n");
   for (int i = 0; i < argc; i++)
   {
      printf("%d\t>[%s]\n", i, argv[i]);
   }

   int64_t n = 3;
   int64_t idx = 2;
   int64_t q = 1, rst;

   rst = vframe(n, idx, &q);

   printf("vframe(%ld,%ld,%p[%ld]) return:%ld!\n", n, idx, &q, q, rst);
}

/*
0x00007fffffffd8e0 - 24*8
0x8e0 - 0x18*0x8
0x8e0 - 0xc0
0x820
0x00007fffffffd820

gef➤  memory watch 0x00007fffffffd820 24 pointers

gef➤  memory watch 0x00007fffffffd850 16 pointers

redirect to:gef_20230303215443.txt!
* reg             :cc/csapp/3.49.c@62|cc/csapp/3.49.c@62|cc/csapp/3.49.c@64|cc/csapp/3.49.c@43|cc/csapp/3.49.c@43|cc/csapp/3.49.c@43|cc/csapp/3.49.c@43|* reg             :cc/csapp/3.49.c@43|cc/csapp/3.49.c@43|cc/csapp/3.49.c@43|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45|cc/csapp/3.49.c@45 * reg             :
$rax              :0x0000000000000037|                  |                  |                  |                  |                  |                  |$rax              :                  |                  |0xe39baa99032a7b00|0x0000000000000027|0x0000000000000000|                  |                  |                  |                  |                  |                  |$rax              :
$rbx              :0x0000000000000001|0x00007fffffffd8a0| address of q     |                  |                  |                  |                  |$rbx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbx              :
$rcx              :0x0000000000000001|                  |                  |                  |                  |                  |0x0000000000000003|$rcx              :0x0000000000000003| n                |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :
$rdx              :0x0000000000000000|                  |0x00007fffffffd8a0| address of q     |                  |                  |                  |$rdx              :0x00007fffffffd8a0| address of q     |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rdx              :
$rsp              :0x00007fffffffd8a0|                  |                  |0x00007fffffffd898|                  |0x00007fffffffd890|                  |$rsp              :                  |0x00007fffffffd880|                  |                  |                  |                  |                  |0x00007fffffffd860|                  |                  |                  |$rsp              :
$rbp              :0x0000000000000001|                  |                  |                  |                  |                  |0x0000000000000001|$rbp              :0x00007fffffffd890|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbp              :
$rsi              :0x00005555555592a0|0x0000000000000002| idx              |                  |                  |                  |                  |$rsi              :                  |                  |                  |0x0000000000000027|                  |0x0000000000000020|                  |                  |                  |0x0000000000000020|0x00007fffffffd867|$rsi              :
$rdi              :0x00007fffffffd340|0x0000000000000003| n                |                  |                  |                  |0x0000000000000003|$rdi              :0x0000000000000002| idx              |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rdi              :
$rip              :0x00005555555550fe|0x000055555555510b|0x0000555555555116|0x0000555555555260|0x0000555555555264|0x0000555555555265|0x0000555555555268|$rip              :0x000055555555526e|0x0000555555555272|0x000055555555527f|0x000055555555528f|0x0000555555555295|0x000055555555529f|0x00005555555552bc|0x00005555555552bf|0x00005555555552c2|0x000055555555532e|0x00005555555552d5|$rip              :
$r8               :0x0000000000000000|                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |0x00007fffffffd880|                  |                  |                  |                  |                  |                  |                  |$r8               :
$r9               :0x000000007fffffff|                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r9               :
$r10              :0x0000000000000000|                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r10              :
$r11              :0x0000000000000246|                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r11              :
$r12              :0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r12              :
$r13              :0x0000555555556012|                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r13              :
$r14              :0x0000555555557db0|                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r14              :
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r15              :
$eflags           :0x0000000000000246|                  |                  |                  |                  |                  |                  |$eflags           :                  |0x0000000000000202|                  |0x0000000000000246|                  |                  |0x0000000000000202|0x0000000000000206|0x0000000000000202|                  |                  |$eflags           :
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$cs               :
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ss               :
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ds               :
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$es               :
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$fs               :
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$gs               :
* stk                                                    call vframe        endbr64            push %rbp          move %rdi, %rcx                                                                                                                                                                                                                                                                             :
0x00007fffffffd850:0x0000555555556004|                  |                  |                  |                  |                  |                  |0x00007fffffffd850:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd850:
0x00007fffffffd858:0x00007ffff7e0502a|                  |                  |                  |                  |                  |                  |0x00007fffffffd858:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd858:
0x00007fffffffd860:0x0000000000000001|                  |                  |                  |                  |                  |                  |0x00007fffffffd860:                  |                  |                  |                  |                  |                  |                  |                  +                  +                  +                  +0x00007fffffffd860:
0x00007fffffffd868:0x0000000000000001|                  |                  |                  |                  |                  |                  |0x00007fffffffd868:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd868:
0x00007fffffffd870:0x0000000000000000|                  |                  |                  |                  |                  |                  |0x00007fffffffd870:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd870:
0x00007fffffffd878:0x0000000000000001|                  |                  |                  |                  |                  |                  |0x00007fffffffd878:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd878:
0x00007fffffffd880:0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |0x00007fffffffd880:                  |                  +                  +                  +                  +                  +                  +                  |                  |                  |0x0000000000000001|0x00007fffffffd880:
0x00007fffffffd888:0x00005555555550a0|                  |                  |                  |                  |                  |                  |0x00007fffffffd888:                  |                  |0xe39baa99032a7b00| canary           |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd888:
0x00007fffffffd890:0x0000555555557db0|                  |                  |                  |                  |0x0000000000000001+ saved %rbp       +0x00007fffffffd890:                  + saved %rbp       =                  =                  =                  =                  =                  =                  =                  =                  =                  =0x00007fffffffd890:
0x00007fffffffd898:0x00005555555550f9|                  |                  |0x000055555555511b+ return address   +                  |                  |0x00007fffffffd898:                  | return address   |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd898:
0x00007fffffffd8a0:0x0000000000000002+                  +0x0000000000000001+ q                |                  |                  |                  |0x00007fffffffd8a0:0x0000000000000001| q                |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a0:
0x00007fffffffd8a8:0xe39baa99032a7b00|                  |                  |                  |                  |                  |                  |0x00007fffffffd8a8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a8:
0x00007fffffffd8b0:0x00007fffffffddb9|                  |                  |                  |                  |                  |                  |0x00007fffffffd8b0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8b0:
0x00007fffffffd8b8:0x0000000000000000|                  |                  |                  |                  |                  |                  |0x00007fffffffd8b8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8b8:
0x00007fffffffd8c0:0x0000000000000001|                  |                  |                  |                  |                  |                  |0x00007fffffffd8c0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8c0:
0x00007fffffffd8c8:0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |0x00007fffffffd8c8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8c8:



(gdb) disassemble /s main
Dump of assembler code for function main:
3.49.c:
53      {
=> 0x00005555555550a0 <+0>:     endbr64
   0x00005555555550a4 <+4>:     push   %r13
   0x00005555555550a6 <+6>:     push   %r12
   0x00005555555550a8 <+8>:     mov    %rsi,%r12
   0x00005555555550ab <+11>:    push   %rbp
   0x00005555555550ac <+12>:    movslq %edi,%rbp

/usr/include/x86_64-linux-gnu/bits/stdio2.h:
112       return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
   0x00005555555550af <+15>:    lea    0xf4e(%rip),%rdi        # 0x555555556004

3.49.c:
53      {
   0x00005555555550b6 <+22>:    push   %rbx
   0x00005555555550b7 <+23>:    sub    $0x18,%rsp
   0x00005555555550bb <+27>:    mov    %fs:0x28,%rax
   0x00005555555550c4 <+36>:    mov    %rax,0x8(%rsp)
   0x00005555555550c9 <+41>:    xor    %eax,%eax

/usr/include/x86_64-linux-gnu/bits/stdio2.h:
112       return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
   0x00005555555550cb <+43>:    call   0x555555555070 <puts@plt>

3.49.c:
55          for (int i = 0; i < argc; i++)
   0x00005555555550d0 <+48>:    test   %ebp,%ebp
   0x00005555555550d2 <+50>:    jle    0x5555555550fe <main+94>
   0x00005555555550d4 <+52>:    xor    %ebx,%ebx
   0x00005555555550d6 <+54>:    lea    0xf35(%rip),%r13        # 0x555555556012
   0x00005555555550dd <+61>:    nopl   (%rax)

/usr/include/x86_64-linux-gnu/bits/stdio2.h:
112       return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
   0x00005555555550e0 <+64>:    mov    (%r12,%rbx,8),%rcx
   0x00005555555550e4 <+68>:    mov    %ebx,%edx
   0x00005555555550e6 <+70>:    mov    %r13,%rsi
   0x00005555555550e9 <+73>:    mov    $0x1,%edi
   0x00005555555550ee <+78>:    xor    %eax,%eax

3.49.c:
55          for (int i = 0; i < argc; i++)
   0x00005555555550f0 <+80>:    add    $0x1,%rbx

/usr/include/x86_64-linux-gnu/bits/stdio2.h:
112       return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
   0x00005555555550f4 <+84>:    call   0x555555555090 <__printf_chk@plt>

3.49.c:
55          for (int i = 0; i < argc; i++)
   0x00005555555550f9 <+89>:    cmp    %rbp,%rbx
   0x00005555555550fc <+92>:    jne    0x5555555550e0 <main+64>

56          {
57              printf("%d\t>[%s]\n", i, argv[i]);
58          }
59
60          int64_t n = 3;
61          int64_t idx = 2;
62          int64_t q = 1, rst;
   0x00005555555550fe <+94>:    mov    %rsp,%rbx
   0x0000555555555101 <+97>:    mov    $0x2,%esi
   0x0000555555555106 <+102>:   mov    $0x3,%edi
   0x000055555555510b <+107>:   movq   $0x1,(%rsp)

63
64          rst = vframe(n, idx, &q);
   0x0000555555555113 <+115>:   mov    %rbx,%rdx
   0x0000555555555116 <+118>:   call   0x555555555260 <vframe>

/usr/include/x86_64-linux-gnu/bits/stdio2.h:
112       return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
   0x000055555555511b <+123>:   sub    $0x8,%rsp
   0x000055555555511f <+127>:   mov    $0x1,%r9d
   0x0000555555555125 <+133>:   mov    %rbx,%r8
   0x0000555555555128 <+136>:   push   %rax
   0x0000555555555129 <+137>:   mov    $0x2,%ecx
   0x000055555555512e <+142>:   mov    $0x3,%edx
   0x0000555555555133 <+147>:   mov    $0x1,%edi
   0x0000555555555138 <+152>:   lea    0xee1(%rip),%rsi        # 0x555555556020
   0x000055555555513f <+159>:   xor    %eax,%eax
   0x0000555555555141 <+161>:   call   0x555555555090 <__printf_chk@plt>
   0x0000555555555146 <+166>:   mov    %rbx,%rsp

3.49.c:
67      }
   0x0000555555555149 <+169>:   mov    0x8(%rsp),%rax
--Type <RET> for more, q to quit, c to continue without paging--
   0x000055555555514e <+174>:   sub    %fs:0x28,%rax
   0x0000555555555157 <+183>:   jne    0x555555555166 <main+198>
   0x0000555555555159 <+185>:   add    $0x18,%rsp
   0x000055555555515d <+189>:   xor    %eax,%eax
   0x000055555555515f <+191>:   pop    %rbx
   0x0000555555555160 <+192>:   pop    %rbp
   0x0000555555555161 <+193>:   pop    %r12
   0x0000555555555163 <+195>:   pop    %r13
   0x0000555555555165 <+197>:   ret
   0x0000555555555166 <+198>:   call   0x555555555080 <__stack_chk_fail@plt>
End of assembler dump.
(gdb) disassemble /s vframe
Dump of assembler code for function vframe:
3.49.c:
43      {
   0x0000555555555260 <+0>:     endbr64
   0x0000555555555264 <+4>:     push   %rbp
   0x0000555555555265 <+5>:     mov    %rdi,%rcx                     # n
   0x0000555555555268 <+8>:     mov    %rsi,%rdi                     # idx
   0x000055555555526b <+11>:    mov    %rsp,%rbp                     # &q
   0x000055555555526e <+14>:    sub    $0x10,%rsp
   0x0000555555555272 <+18>:    mov    %fs:0x28,%rax
   0x000055555555527b <+27>:    mov    %rax,-0x8(%rbp)
   0x000055555555527f <+31>:    xor    %eax,%eax

44          int64_t i;
45          int64_t *p[n];
   0x0000555555555281 <+33>:    lea    0xf(,%rcx,8),%rax             # 15+n*8                when n=3 is 39 0x27
   0x0000555555555289 <+41>:    mov    %rsp,%r8
   0x000055555555528c <+44>:    mov    %rax,%rsi
   0x000055555555528f <+47>:    and    $0xfffffffffffff000,%rax      # (15+n*8)//4096*4096   when n=3 is 0
   0x0000555555555295 <+53>:    sub    %rax,%r8                      # %rsp-(15+n*8)//4096*4096
   0x0000555555555298 <+56>:    and    $0xfffffffffffffff0,%rsi      # (15+n*8)//16*16       when n=3 is 32 0x20
   0x000055555555529c <+60>:    cmp    %r8,%rsp
   0x000055555555529f <+63>:    je     0x5555555552b6 <vframe+86>
   0x00005555555552a1 <+65>:    sub    $0x1000,%rsp                  # if alloca > 4096 then alloca 0x1000=4096
   0x00005555555552a8 <+72>:    orq    $0x0,0xff8(%rsp)              # 0x1000 - 0xff8 = 0x8
   0x00005555555552b1 <+81>:    cmp    %r8,%rsp
   0x00005555555552b4 <+84>:    jne    0x5555555552a1 <vframe+65>    # if
   0x00005555555552b6 <+86>:    and    $0xfff,%esi                   # (15+n*8)//16 * 16 % 4096 alloca max is 4096
   0x00005555555552bc <+92>:    sub    %rsi,%rsp                     # alloca 0x20
   0x00005555555552bf <+95>:    test   %rsi,%rsi
   0x00005555555552c2 <+98>:    jne    0x555555555328 <vframe+200>   # if alloca is not zero
   0x00005555555552c4 <+100>:   lea    0x7(%rsp),%rsi                # %rsp+0x7

46          p[0] = &i;
   0x00005555555552c9 <+105>:   lea    -0x10(%rbp),%r8               # %r8 = &p[0]

47          for (i = 1; i < n; i++)
   0x00005555555552cd <+109>:   movq   $0x1,-0x10(%rbp)              # *p[0] = 1

45          int64_t *p[n];
   0x00005555555552d5 <+117>:   mov    %rsi,%rax
   0x00005555555552d8 <+120>:   and    $0xfffffffffffffff8,%rsi      # %rsi = (%rsp+0x7)//8*8 = %rsp

46          p[0] = &i;
   0x00005555555552dc <+124>:   shr    $0x3,%rax                     # (%rsp+0x7)//8
   0x00005555555552e0 <+128>:   mov    %r8,0x0(,%rax,8)              # (%rsp+0x7)//8*8 = %r8

47          for (i = 1; i < n; i++)
   0x00005555555552e8 <+136>:   cmp    $0x1,%rcx
   0x00005555555552ec <+140>:   jle    0x555555555309 <vframe+169>   # if n < 1 then goto end
   0x00005555555552ee <+142>:   mov    $0x1,%eax
   0x00005555555552f3 <+147>:   nopl   0x0(%rax,%rax,1)

48              p[i] = q;
   0x00005555555552f8 <+152>:   mov    %rdx,(%rsi,%rax,8)            # p[i] = q

47          for (i = 1; i < n; i++)
   0x00005555555552fc <+156>:   add    $0x1,%rax                     # i++
   0x0000555555555300 <+160>:   cmp    %rax,%rcx
   0x0000555555555303 <+163>:   jne    0x5555555552f8 <vframe+152>   # if i < n then goto 0x00005555555552f8
   0x0000555555555305 <+165>:   mov    %rcx,-0x10(%rbp)

49          return *p[idx];
   0x0000555555555309 <+169>:   mov    (%rsi,%rdi,8),%rax
   0x000055555555530d <+173>:   mov    (%rax),%rax

50      }
   0x0000555555555310 <+176>:   mov    -0x8(%rbp),%rdx
   0x0000555555555314 <+180>:   sub    %fs:0x28,%rdx
   0x000055555555531d <+189>:   jne    0x555555555330 <vframe+208>
   0x000055555555531f <+191>:   leave
   0x0000555555555320 <+192>:   ret
   0x0000555555555321 <+193>:   nopl   0x0(%rax)

45          int64_t *p[n];
   0x0000555555555328 <+200>:   orq    $0x0,-0x8(%rsp,%rsi,1)        # %rsp-0x8 + 0x20
   0x000055555555532e <+206>:   jmp    0x5555555552c4 <vframe+100>

50      }
   0x0000555555555330 <+208>:   call   0x555555555080 <__stack_chk_fail@plt>
End of assembler dump.

*/
