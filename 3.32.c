#include <stdint.h>
#include <stdio.h>

/*
*p = d;
return x-c;

1 movsbl 12(%ebp),%edx	// M[12(%ebp)] = d
2 movl   16(%ebp), %eax	// M[16(%ebp)] = p
3 movl   %edx, (%eax)	// d, p
4 movswl 8(%ebp),%eax	// M[ 8(%ebp)] = c
5 movl   20(%ebp), %edx	// M[20(%ebp)] = x
6 subl   %eax, %edx		// c, x
7 movl   %edx, %eax


%ebp|
----+--
20	|x
16	|p
12	|d
8	|c
4	|
0	|

int16_t c, int8_t d, int8_t *p, int32_t x
*/
int fn3_32(int16_t c, int8_t d, int8_t *p, int32_t x)
{
    *p = d;
    return x - c;
}
/*
29      {
   0x0000000000001169 <+0>:     endbr64
   0x000000000000116d <+4>:     push   %rbp
   0x000000000000116e <+5>:     mov    %rsp,%rbp
   0x0000000000001171 <+8>:     mov    %edi,%eax        // d
   0x0000000000001173 <+10>:    mov    %rdx,-0x10(%rbp) // p
   0x0000000000001177 <+14>:    mov    %ecx,-0x14(%rbp) // x
   0x000000000000117a <+17>:    mov    %ax,-0x4(%rbp)   // c
   0x000000000000117e <+21>:    mov    %esi,%eax
   0x0000000000001180 <+23>:    mov    %al,-0x8(%rbp)

30          *p = d;
   0x0000000000001183 <+26>:    mov    -0x10(%rbp),%rax // p
   0x0000000000001187 <+30>:    movzbl -0x8(%rbp),%edx  // d
   0x000000000000118b <+34>:    mov    %dl,(%rax)

31          return x - c;
   0x000000000000118d <+36>:    movswl -0x4(%rbp),%edx  // c
   0x0000000000001191 <+40>:    mov    -0x14(%rbp),%eax // x
   0x0000000000001194 <+43>:    sub    %edx,%eax        // c, x

32      }
   0x0000000000001196 <+45>:    pop    %rbp
   0x0000000000001197 <+46>:    ret

%rbp    |
--------+---
-0x00	|c,
-0x08	| ,d
-0x10	|p
-0x18	| ,x

*/

int fn3_32_2(int32_t x, int16_t *p, int8_t d, int16_t c)
{
    *p = d;
    return x - c;
}
/*
69      {
   0x0000000000001198 <+0>:     endbr64
   0x000000000000119c <+4>:     push   %rbp
   0x000000000000119d <+5>:     mov    %rsp,%rbp
   0x00000000000011a0 <+8>:     mov    %edi,-0x4(%rbp)  // x
   0x00000000000011a3 <+11>:    mov    %rsi,-0x10(%rbp) // p
   0x00000000000011a7 <+15>:    mov    %ecx,%eax        // c
   0x00000000000011a9 <+17>:    mov    %dl,-0x8(%rbp)   // d
   0x00000000000011ac <+20>:    mov    %ax,-0x14(%rbp)

70          *p = d;
   0x00000000000011b0 <+24>:    movsbw -0x8(%rbp),%dx
   0x00000000000011b5 <+29>:    mov    -0x10(%rbp),%rax
   0x00000000000011b9 <+33>:    mov    %dx,(%rax)

71          return x - c;
   0x00000000000011bc <+36>:    movswl -0x14(%rbp),%edx
   0x00000000000011c0 <+40>:    mov    -0x4(%rbp),%eax
   0x00000000000011c3 <+43>:    sub    %edx,%eax

72      }
   0x00000000000011c5 <+45>:    pop    %rbp
   0x00000000000011c6 <+46>:    ret

*/

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
}

int proc(void)
{
    int x, y;
    scanf("%x %x", &y, &x);
    return x - y;
}
/*
3.33

disass /m proc

1   proc:
2   pushl   %ebp
3   movl    %esp, %ebp
4   subl    $40, %esp
5   leal    -4(%ebp), %eax
6   movl    %eax, 8(%esp)
7   leal    -8(%ebp), %eax
8   movl    %eax, 4(%esp)
9   movl    $.LC0, (%esp)   // Pointer to string "%x %x"
10  call    scanf           // Diagram stack frame at this point
11  movl    -4(%ebp), %eax
12  subl    -8(%ebp), %eax
13  leave
14  ret

init
i r
%esp    0x800040
%ebp    0x800060

scanf reads values 0x46 and 0x53 from the standard input.
Assume that the string “%x %x” is stored at memory location 0x300070.

0x300070
*/
