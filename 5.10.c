#include <stdio.h>

#include <stdlib.h> // malloc ...

typedef struct ELE
{
    struct ELE *next;
    long data;
} list_ele, *list_ptr;

long list_len(list_ptr ls)
{
    long len = 0;
    while (ls)
    {
        len++;
        ls = ls->next;
    }
    return len;
}

#include <stdint.h>
#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

void test_load_latency(long len)
{

    list_ptr list = (list_ptr)malloc(sizeof(list_ele));
    list->next = NULL;
    list->data = 0;
    list_ptr new_ele = list;
    for (long i = 1; i < len; i++)
    {
        list_ptr new_ele_nxt = (list_ptr)malloc(sizeof(list_ele));
        new_ele_nxt->next = NULL;
        new_ele_nxt->data = i;

        new_ele->next = new_ele_nxt;
        new_ele = new_ele_nxt;
    }
    long len2 = list_len(list);
    printf("malloc list len is:%ld, and current list len is:%ld\n", len, len2);
    len2 = 0;
    const long TIMES = 1000000;

    uint64_t tsc0, tsc1;
    tsc0 = perf_counter();
    for (long i = 0; i < TIMES; i++)
    {
        len2 += list_len(list);
    }
    tsc1 = perf_counter();
    printf("malloc list len is:%ld/%ld=%ld used cycle:%lf,\t%lu\n", len2, TIMES, len2 / TIMES,
           (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);
}

/* Set elements of array to 0 */
void clear_array(long *dest, long n)
{
    long i;
    for (i = 0; i < n; i++)
        dest[i] = 0;
}

/* Write to dest, read from src */
void write_read(long *src, long *dst, long n)
{
    long cnt = n;
    long val = 0;
    while (cnt)
    {
        *dst = val;
        val = (*src) + 1;
        cnt--;
    }
}

void test_write_read()
{
    long a[] = {-10, 17};
    uint64_t tsc0, tsc1;
    const long TIMES = 3;

    tsc0 = perf_counter();
    write_read(&a[0], &a[1], TIMES);
    tsc1 = perf_counter();
    printf("after write_read(&a[0], &a[1], 3):a={%ld,%ld} used cycle:%lf,\t%lu\n", a[0], a[1],
           (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);

    a[0] = -10;
    a[1] = 17;

    tsc0 = perf_counter();
    write_read(&a[0], &a[0], TIMES);
    tsc1 = perf_counter();
    printf("after write_read(&a[0], &a[0], 3):a={%ld,%ld} used cycle:%lf,\t%lu\n", a[0], a[1],
           (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);
}

/*
000000000000154f <copy_array>:
    154f:       f3 0f 1e fa             endbr64
    1553:       55                      push   %rbp
    1554:       48 89 e5                mov    %rsp,%rbp
    1557:       ff 15 8b 2a 00 00       call   *0x2a8b(%rip)        # 3fe8 <mcount@GLIBC_2.2.5>
    155d:       b8 00 00 00 00          mov    $0x0,%eax
    1562:       eb 0c                   jmp    1570 <copy_array+0x21>
    1564:       48 8b 0c c7             mov    (%rdi,%rax,8),%rcx
    1568:       48 89 0c c6             mov    %rcx,(%rsi,%rax,8)
    156c:       48 83 c0 01             add    $0x1,%rax
    1570:       48 39 d0                cmp    %rdx,%rax
    1573:       7c ef                   jl     1564 <copy_array+0x15>
    1575:       5d                      pop    %rbp
    1576:       c3                      ret

*/
void copy_array(long *src, long *dest, long n)
{
    long i;
    for (i = 0; i < n; i++)
        dest[i] = src[i];
}

void test_copy_array()
{
    const long LENGTH = 1000;
    long a[LENGTH];
    for (long i = 0; i < LENGTH; i++)
        a[i] = i;
    uint64_t tsc0, tsc1;
    tsc0 = perf_counter();
    copy_array(a + 1, a, LENGTH - 1); // a[i] = i+1
    tsc1 = perf_counter();
    for (long i = 0; i < LENGTH; i++)
    {
        if (a[i] != i + 1)
        {
            printf("err: a[%ld]=%ld!=%ld+1=%ld\n", i, a[i], i, i + 1);
        }
    }
    printf("after copy_array(a + 1, a, %ld) used cycle:%lf,\t%lu\n", LENGTH - 1, (double)(tsc1 - tsc0) / LENGTH, tsc1 - tsc0);

    for (long i = 0; i < LENGTH; i++)
        a[i] = i;
    tsc0 = perf_counter();
    copy_array(a, a + 1, LENGTH - 1); // a[i] = 0
    tsc1 = perf_counter();
    for (long i = 0; i < LENGTH; i++)
    {
        if (a[i] != 0)
        {
            printf("err: a[%ld]=%ld!=0\n", i, a[i]);
        }
    }
    printf("after copy_array(a, a + 1, %ld) used cycle:%lf,\t%lu\n", LENGTH - 1, (double)(tsc1 - tsc0) / LENGTH, tsc1 - tsc0);

    for (long i = 0; i < LENGTH; i++)
        a[i] = i;
    tsc0 = perf_counter();
    copy_array(a, a, LENGTH - 1); // a[i] = 0
    tsc1 = perf_counter();
    for (long i = 0; i < LENGTH; i++)
    {
        if (a[i] != i)
        {
            printf("err: a[%ld]=%ld!=%ld\n", i, a[i], i);
        }
    }
    printf("after copy_array(a, a, %ld) used cycle:%lf,\t%lu\n", LENGTH - 1, (double)(tsc1 - tsc0) / LENGTH, tsc1 - tsc0);
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }

    test_load_latency(1024);
    test_write_read();
    test_copy_array();
    return 0;
}
