
default:	all

clean:
	# rm -rf csapp
	# ls |grep -v .[chs]$$|grep -v .py|grep -v Makefile|grep -v readme.txt|grep -v README.md|xargs rm
	find . -maxdepth 1 -type f|grep -v .[chs]$$|grep -v .py|grep -v Makefile|grep -v readme.txt|grep -v README.md|xargs rm

.PHONY:	default clean

build:
	gcc -g main.c -o csapp

run: build
	./csapp

all: build run


