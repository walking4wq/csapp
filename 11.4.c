
#include "csapp.h"

int main(int argc, char **argv)
{
    char *host, *port;
    switch (argc)
    {
    case 3:
        if (argv[1][0] == 'h')
        {
            host = argv[2];
            port = NULL;
        }
        else
        {
            host = NULL;
            port = argv[2];
        }
        break;
    case 4:
        host = argv[2];
        port = argv[3];
        break;
    default:
        fprintf(stderr, "\tusage:\nhost: %s h <domain name>\nport: %s p <port>\nport: %s a <domain name> <port>\n",
                argv[0], argv[0], argv[0]);
        exit(0);
    }

    struct addrinfo *p, *listp, hints;
    char buf[MAXLINE];
    int rc, flags;

    /* Get a list of addrinfo records */
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    /* IPv4 only */
    hints.ai_socktype = SOCK_STREAM; /* Connections only */
    if ((rc = getaddrinfo(host, port, &hints, &listp)) != 0)
    {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(rc));
        exit(1);
    }

    /* Walk the list and display each IP address */
    flags = NI_NUMERICHOST; /* Display address string instead of domain name */

    struct sockaddr_in *sockp;

    for (p = listp; p; p = p->ai_next)
    {
        Getnameinfo(p->ai_addr, p->ai_addrlen, buf, MAXLINE, NULL, 0, flags);
        printf("getnameinfo:%s\n", buf);

        sockp = (struct sockaddr_in *)p->ai_addr;
        Inet_ntop(AF_INET, &(sockp->sin_addr), buf, MAXLINE);
        printf("inet_ntop:%s:%d\n", buf, sockp->sin_port);
    }

    /* Clean up */
    Freeaddrinfo(listp);

    exit(0);
}
