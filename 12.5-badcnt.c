/* WARNING: This code is buggy! */
#include "csapp.h"

void *thread(void *vargp);
/* Thread routine prototype */

/* Global shared variable */
volatile long cnt = 0; /* Counter */
sem_t mutex;           /* Semaphore that protects counter */

int main(int argc, char **argv)
{
    long niters;
    pthread_t tid1, tid2;

    /* Check input argument */
    if (argc != 2)
    {
        printf("usage: %s <niters>\n", argv[0]);
        exit(0);
    }
    niters = atoi(argv[1]);

    Sem_init(&mutex, 0, 1); /* mutex = 1 */

    P(&mutex);
    printf("after P\n");
    V(&mutex);
    printf("after V\n");
    for (int i = 0; i < 3; i++)
    {
        V(&mutex);
        printf("after V again\n");
    }

    Sem_init(&mutex, 0, 1); /* mutex = 1 */

    /* Create threads and wait for them to finish */
    Pthread_create(&tid1, NULL, thread, &niters);
    Pthread_create(&tid2, NULL, thread, &niters);
    Pthread_join(tid1, NULL);
    Pthread_join(tid2, NULL);

    /* Check result */
    if (cnt != (2 * niters))
        printf("BOOM! cnt=%ld\n", cnt);
    else
        printf("OK cnt=%ld\n", cnt);
    exit(0);
}

/* Thread routine */
void *thread(void *vargp)
{
    long i, niters = *((long *)vargp);

    for (i = 0; i < niters; i++)
    {
        // P(&mutex);
        cnt++;
        // V(&mutex);
    }

    return NULL;
}

/*

0000000000002be0 <thread>:
    2be0:       f3 0f 1e fa             endbr64
    2be4:       48 8b 0f                mov    (%rdi),%rcx
    2be7:       48 85 c9                test   %rcx,%rcx                    # if niters < 0
    2bea:       7e 1f                   jle    2c0b <thread+0x2b>
    2bec:       31 d2                   xor    %edx,%edx
    2bee:       66 90                   xchg   %ax,%ax
    2bf0:       48 8b 05 39 54 00 00    mov    0x5439(%rip),%rax        # 8030 <cnt>
    2bf7:       48 83 c2 01             add    $0x1,%rdx                    # i++
    2bfb:       48 83 c0 01             add    $0x1,%rax                    # cnt++
    2bff:       48 89 05 2a 54 00 00    mov    %rax,0x542a(%rip)        # 8030 <cnt>
    2c06:       48 39 d1                cmp    %rdx,%rcx                    # if i != niters
    2c09:       75 e5                   jne    2bf0 <thread+0x10>
    2c0b:       31 c0                   xor    %eax,%eax
    2c0d:       c3                      ret
    2c0e:       66 90                   xchg   %ax,%ax



C code for thread i
===========================
for (i = 0; i < niters; i++)
    cnt++;
===========================



Asm code for thread i
===========================+
    movq    (%rdi), %rcx   |\
    testq   %rcx, %rcx     | |
    jle     .L2            | Hi : Head
    movl    $0, %eax       |/
---------------------------+
.L3:                       |\
    movq    cnt(%rip),%rdx | Li : Load cnt
    addq    %eax           | Ui : Update cnt
    movq    %eax,cnt(%rip) | Si : Store cnt
---------------------------+/
    addq    $1, %rax       |\
    cmpq    %rcx, %rax     | Ti : Tail
    jne     .L3            | |
.L2:                       |/
===========================+

*/
