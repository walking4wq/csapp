/*
4.1
                                #        0  1  2  3  4  5  6  7  8  9
.pos 0x100                      # Start code at address 0x100
    irmovq  $15, %rbx           # 0x100: 30 f3 0f 00 00 00 00 00 00 00
    rrmovq  %rbx, %rcx          # 0x10a: 20 31
 loop:                          # 0x10c:
    rmmovq  %rcx, -3(%rbx)      # 0x10c: 40 13 fd ff ff ff ff ff ff ff
    addq    %rbx, %rcx          # 0x116: 60 31
    jmp     loop                # 0x118: 70 0c 01 00 00 00 00 00 00 00

4.2
0x100: 30f3fcffffffffffffff40630008000000000000
       0  1  2  3  4  5  6  7  8  9
0x100: 30 f3 fc ff ff ff ff ff ff ff | irmovq $-4, %rbx
0x10a: 40 63 00 08 00 00 00 00 00 00 | rmmovq %rsi, 0x800(%rbx) # 2048

0x200: a06f800c020000000000000030f30a0000000000000090
       0  1  2  3  4  5  6  7  8  9
0x200: a0 6f                         | pushq %rsi
0x202: 80 0c 02 00 00 00 00 00 00    | call 0x20c
0x20b: 00                            | halt
0x20c: 30 f3 0a 00 00 00 00 00 00 00 | irmovq $10, %rbx
0x216: 90                            | ret

0x300: 5054070000000000000010f0b01f
       0  1  2  3  4  5  6  7  8  9
0x300: 50 54 07 00 00 00 00 00 00 00 | mrmovq $7(%rsp), %rbp
0x30a: 10                            | nop
0x30b: f0                            | .byte 0xf0 #Invalid instruction code
0x30c: b0 1f                         | popq %rcx

0x400: 611373000400000000000000
       0  1  2  3  4  5  6  7  8  9
0x400: 61 13                         | subq %rcx, %rbx
0x402: 73 00 04 00 00 00 00 00 00    | je 0x400
0x40b: 00                            | halt

0x500: 6362a0f0
       0  1  2  3  4  5  6  7  8  9
0x500: 63 62                         | xorq %rsi, %rdx
0x502: a0                            | .byte 0xa0 # pushq instruction
0x503: f0                            | .byte 0xf0 # Invalid register specifier byte

*/

long array[] = {1L, 2L, 3L, 4L};

/*
gcc -O2 -g -o 4.4 4.4.c
objdump -D 4.4

0000000000001140 <rsum>:
    1140:	f3 0f 1e fa          	endbr64
    1144:	31 c0                	xor    %eax,%eax
    1146:	45 31 c0             	xor    %r8d,%r8d
    1149:	48 85 f6             	test   %rsi,%rsi
    114c:	7e 1a                	jle    1168 <rsum+0x28>     # if %rsi <= 0 then goto 1168
    114e:	66 90                	xchg   %ax,%ax              # nop
    1150:	4c 03 04 c7          	add    (%rdi,%rax,8),%r8    # %r = %rdi + 8*%rax    # %rdi is start
    1154:	48 83 c0 01          	add    $0x1,%rax            # %rax++
    1158:	48 39 c6             	cmp    %rax,%rsi
    115b:	75 f3                	jne    1150 <rsum+0x10>     # if %rax != %rsi       # %rsi is count
    115d:	4c 89 c0             	mov    %r8,%rax
    1160:	c3                   	ret
    1161:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
    1168:	49 89 f0             	mov    %rsi,%r8             # %r8 = %rsi    # %rsi is count
    116b:	4c 89 c0             	mov    %r8,%rax             # %rax = %r8
    116e:	c3                   	ret



gcc -O1 -g -o 4.4 4.4.c
objdump -D 4.4

0000000000001129 <rsum>:
    1129:       f3 0f 1e fa             endbr64
    112d:       48 89 f0                mov    %rsi,%rax
    1130:       48 85 f6                test   %rsi,%rsi
    1133:       7e 16                   jle    114b <rsum+0x22>
    1135:       53                      push   %rbx                 # stack winding
    1136:       48 89 fb                mov    %rdi,%rbx
    1139:       48 83 ee 01             sub    $0x1,%rsi            # count - 1
    113d:       48 8d 7f 08             lea    0x8(%rdi),%rdi       # start + 1
    1141:       e8 e3 ff ff ff          call   1129 <rsum>
    1146:       48 03 03                add    (%rbx),%rax
    1149:       5b                      pop    %rbx
    114a:       c3                      ret
    114b:       c3                      ret



gcc -O0 -g -o 4.4 4.4.c
objdump -D 4.4

0000000000001129 <rsum>:
    1129:       f3 0f 1e fa             endbr64
    112d:       55                      push   %rbp
    112e:       48 89 e5                mov    %rsp,%rbp
    1131:       53                      push   %rbx
    1132:       48 83 ec 18             sub    $0x18,%rsp
    1136:       48 89 7d e8             mov    %rdi,-0x18(%rbp)
    113a:       48 89 75 e0             mov    %rsi,-0x20(%rbp)
    113e:       48 83 7d e0 00          cmpq   $0x0,-0x20(%rbp)
    1143:       7f 06                   jg     114b <rsum+0x22>
    1145:       48 8b 45 e0             mov    -0x20(%rbp),%rax
    1149:       eb 25                   jmp    1170 <rsum+0x47>
    114b:       48 8b 45 e8             mov    -0x18(%rbp),%rax
    114f:       48 8b 18                mov    (%rax),%rbx
    1152:       48 8b 45 e0             mov    -0x20(%rbp),%rax
    1156:       48 8d 50 ff             lea    -0x1(%rax),%rdx
    115a:       48 8b 45 e8             mov    -0x18(%rbp),%rax
    115e:       48 83 c0 08             add    $0x8,%rax
    1162:       48 89 d6                mov    %rdx,%rsi
    1165:       48 89 c7                mov    %rax,%rdi
    1168:       e8 bc ff ff ff          call   1129 <rsum>
    116d:       48 01 d8                add    %rbx,%rax
    1170:       48 8b 5d f8             mov    -0x8(%rbp),%rbx
    1174:       c9                      leave
    1175:       c3                      ret

*/
long rsum(long *start, long count)
{
    if (count <= 0)
        return count;
    return *start + rsum(start + 1, count - 1);
}

#include <stdio.h>
// int main(int argc, char *argv[])
int main(void)
{
    long sum;
    sum = rsum(array, 3);
    printf("sum=%ld\n", sum);
}

/*
0x00007fffffffd920
0x00007fffffffd888

gef➤  hexdump qword array --size 5
0x0000555555558020│+0x0000   <array+0000> 0x0000000000000001
0x0000555555558028│+0x0008   <array+0008> 0x0000000000000002
0x0000555555558030│+0x0010   <array+0010> 0x0000000000000003
0x0000555555558038│+0x0018   <array+0018> 0x0000000000000004
0x0000555555558040│+0x0020   <completed+0000> 0x0000000000000000

diff_reg_stk add 0x00007fffffffd888 --size 21

redirect to:gef_20230312095910.txt!
* reg             :cc/csapp/4.4.c@133|cc/csapp/4.4.c@135|cc/csapp/4.4.c@124|cc/csapp/4.4.c@125|cc/csapp/4.4.c@125|cc/csapp/4.4.c@124|cc/csapp/4.4.c@124|cc/csapp/4.4.c@127|cc/csapp/4.4.c@124|* reg             :cc/csapp/4.4.c@125|cc/csapp/4.4.c@124|cc/csapp/4.4.c@124|cc/csapp/4.4.c@127|cc/csapp/4.4.c@124|cc/csapp/4.4.c@125|cc/csapp/4.4.c@125|cc/csapp/4.4.c@124|cc/csapp/4.4.c@124|cc/csapp/4.4.c@127|cc/csapp/4.4.c@124|* reg             :cc/csapp/4.4.c@125|cc/csapp/4.4.c@125|cc/csapp/4.4.c@128|cc/csapp/4.4.c@127|cc/csapp/4.4.c@128|cc/csapp/4.4.c@128|cc/csapp/4.4.c@127|cc/csapp/4.4.c@128|cc/csapp/4.4.c@128|cc/csapp/4.4.c@127|cc/csapp/4.4.c@128|cc/csapp/4.4.c@128|cc/csapp/4.4.c@135|/bits/stdio2.h@112 * reg             :
$rax              :0x000055555555516c|                  |                  |                  |0x0000000000000003|                  |                  |                  |                  |$rax              :0x0000000000000002|                  |                  |                  |                  |                  |0x0000000000000001|                  |                  |                  |0x0000000000000001|$rax              :                  |0x0000000000000000|                  |                  |0x0000000000000003|                  |                  |0x0000000000000005|                  |                  |0x0000000000000006|                  |                  |0x0000000000000000|$rax              :
$rbx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |0x0000555555558020| start            |$rbx              :                  |                  |                  |0x0000555555558028|                  |                  |                  |                  |                  |0x0000555555558030|                  |$rbx              : start - 1        |                  |                  |0x0000555555558030| &3               |0x0000555555558028| &2               |                  |0x0000555555558020|                  |                  |0x0000000000000000|                  |                  |$rbx              :
$rcx              :0x0000555555557dc0|                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :
$rdx              :0x00007fffffffd9f8|                  |                  |                  |                  |                  |                  |                  |                  |$rdx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rdx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000006|$rdx              :
$rsp              :0x00007fffffffd8d8|0x00007fffffffd8d0|0x00007fffffffd8c8|                  |                  |                  |0x00007fffffffd8c0|                  |0x00007fffffffd8b8|$rsp              :                  |                  |0x00007fffffffd8b0|                  |0x00007fffffffd8a8|                  |                  |                  |0x00007fffffffd8a0|                  |0x00007fffffffd898|$rsp              :                  |                  |                  |0x00007fffffffd8a0|                  |0x00007fffffffd8a8|0x00007fffffffd8b0|                  |0x00007fffffffd8b8|0x00007fffffffd8c0|                  |0x00007fffffffd8c8|0x00007fffffffd8d0|                  |$rsp              :
$rbp              :0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |$rbp              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbp              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbp              :
$rsi              :0x00007fffffffd9e8|0x0000000000000003| count            |                  |                  |                  |                  |0x0000000000000002|                  |$rsi              :                  |                  |                  |0x0000000000000001|                  |                  |                  |                  |                  |0x0000000000000000|                  |$rsi              : count            |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555556004|$rsi              :
$rdi              :0x0000000000000001|0x0000555555558020| array            |                  |                  |                  |                  |0x0000555555558028|                  |$rdi              :                  |                  |                  |0x0000555555558030|                  |                  |                  |                  |                  |0x0000555555558038|                  |$rdi              : start            |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000001|$rdi              :
$rip              :0x000055555555516c|0x0000555555555180|0x0000555555555149|0x000055555555514d|0x0000555555555153|0x0000555555555155|0x0000555555555156|0x0000555555555161|0x0000555555555149|$rip              :0x0000555555555153|0x0000555555555155|0x0000555555555156|0x0000555555555161|0x0000555555555149|0x000055555555514d|0x0000555555555153|0x0000555555555155|0x0000555555555156|0x0000555555555161|0x0000555555555149|$rip              :0x000055555555514d|0x0000555555555153|0x000055555555516b|0x0000555555555166|0x0000555555555169|0x000055555555516a|0x0000555555555166|0x0000555555555169|0x000055555555516a|0x0000555555555166|0x0000555555555169|0x000055555555516a|0x0000555555555185|0x0000555555555199|$rip              :
$r8               :0x00007ffff7f9ef10|                  |                  |                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r8               :
$r9               :0x00007ffff7fc9040|                  |                  |                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r9               :
$r10              :0x00007ffff7fc3908|                  |                  |                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r10              :
$r11              :0x00007ffff7fde680|                  |                  |                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r11              :
$r12              :0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r12              :
$r13              :0x000055555555516c|                  |                  |                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r13              :
$r14              :0x0000555555557dc0|                  |                  |                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r14              :
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r15              :
$eflags           :0x0000000000000246|0x0000000000000202|                  |                  |0x0000000000000206|                  |                  |0x0000000000000202|                  |$eflags           :                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000246|                  |$eflags           :                  |                  |                  |                  |0x0000000000000206|                  |                  |                  |                  |                  |                  |                  |                  |                  |$eflags           :
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$cs               :
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ss               :
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ds               :
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$es               :
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$fs               :
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$gs               :
* stk              endbr64 for main   call rsum          endbr64 for rsum   mov rax, rsi       jle rsum+34        push rbx           mov rbx, rdi       call rsum          endbr64 for rsum   * stk              jle rsum+34        push rbx           mov rbx, rdi       call rsum          endbr64 for rsum   mov rax, rsi       jle rsum+34        push rbx           mov rbx, rdi       call rsum          endbr64 for rsum   * stk              mov rax, rsi       jle rsum+34        ret                add rax, (rbx)     pop rbx            ret                add rax, (rbx)     pop rbx            ret                add rax, (rbx)     pop rbx            ret                mov rdx, rax       call printf        * stk             :
0x00007fffffffd888:0x00007fffffffdda9|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd888:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd888:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd888:
0x00007fffffffd890:0x00007ffff7fc1000|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd890:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd890:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd890:
0x00007fffffffd898:0x0000010101000000|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd898:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555555166+0x00007fffffffd898:                  +                  +0x0000555555555166+ rsp to rip       |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd898:
0x00007fffffffd8a0:0x0000000000000002|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a0:                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555558028+                  +0x0000555555558028|0x00007fffffffd8a0:                  |                  |                  |0x0000555555558028+                  +                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a0:
0x00007fffffffd8a8:0x00000000178bfbff|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a8:                  |                  |                  |                  |0x0000555555555166+                  +                  +                  +                  |                  |0x0000555555555166|0x00007fffffffd8a8:                  |                  |                  |0x0000555555555166|                  |0x0000555555555166+                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8a8:
0x00007fffffffd8b0:0x00007fffffffddb9|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8b0:                  |                  |0x0000555555558020+                  +                  |                  |                  |                  |                  |                  |0x0000555555558020|0x00007fffffffd8b0:                  |                  |                  |0x0000555555558020|                  |                  |                  +0x0000555555558020+                  |                  |                  |                  |                  |                  |0x00007fffffffd8b0:
0x00007fffffffd8b8:0x0000000000000064|                  |                  |                  |                  |                  |                  |                  |0x0000555555555166+0x00007fffffffd8b8:                  +                  +                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555555166|0x00007fffffffd8b8:                  |                  |                  |0x0000555555555166|                  |                  |                  |                  |0x0000555555555166+                  |                  |                  |                  |                  |0x00007fffffffd8b8:
0x00007fffffffd8c0:0x0000000000001000|                  |                  |                  |                  |                  |0x0000000000000000+                  +                  |0x00007fffffffd8c0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000000|0x00007fffffffd8c0:                  |                  |                  |0x0000000000000000|                  |                  |                  |                  |                  |                  +                  +                  |                  |                  |0x00007fffffffd8c0:
0x00007fffffffd8c8:0x0000555555555060|                  |0x0000555555555185+ return address   +                  +                  +                  |                  |                  |0x00007fffffffd8c8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555555185|0x00007fffffffd8c8:                  |                  |                  |0x0000555555555185|                  |                  |                  |                  |                  |                  |                  |0x0000555555555185+                  |                  |0x00007fffffffd8c8:
0x00007fffffffd8d0:0x0000000000000000|                  +                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8d0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8d0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  +                  +0x00007fffffffd8d0:
0x00007fffffffd8d8:0x00007ffff7dadd90+                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8d8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8d8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8d8:
0x00007fffffffd8e0:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e0:
0x00007fffffffd8e8:0x000055555555516c|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8e8:
0x00007fffffffd8f0:0x00000001ffffd9d0|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f0:
0x00007fffffffd8f8:0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd8f8:
0x00007fffffffd900:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd900:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd900:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd900:
0x00007fffffffd908:0x9b399f6f6345854a|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd908:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd908:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd908:
0x00007fffffffd910:0x00007fffffffd9e8|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd910:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd910:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd910:
0x00007fffffffd918:0x000055555555516c|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd918:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd918:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd918:
0x00007fffffffd920:0x0000555555557dc0|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd920:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd920:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd920:
0x00007fffffffd928:0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd928:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd928:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd928:


Dump of assembler code for function main:
=> 0x000055555555516c <+0>:     endbr64
   0x0000555555555170 <+4>:     sub    rsp,0x8
   0x0000555555555174 <+8>:     mov    esi,0x3
   0x0000555555555179 <+13>:    lea    rdi,[rip+0x2ea0]        # 0x555555558020 <array>
   0x0000555555555180 <+20>:    call   0x555555555149 <rsum>
   0x0000555555555185 <+25>:    mov    rdx,rax
   0x0000555555555188 <+28>:    lea    rsi,[rip+0xe75]        # 0x555555556004
   0x000055555555518f <+35>:    mov    edi,0x1
   0x0000555555555194 <+40>:    mov    eax,0x0
   0x0000555555555199 <+45>:    call   0x555555555050 <__printf_chk@plt>
   0x000055555555519e <+50>:    mov    eax,0x0
   0x00005555555551a3 <+55>:    add    rsp,0x8
   0x00005555555551a7 <+59>:    ret
End of assembler dump.

Dump of assembler code for function rsum:
   0x0000555555555149 <+0>:     endbr64
   0x000055555555514d <+4>:     mov    rax,rsi
   0x0000555555555150 <+7>:     test   rsi,rsi
   0x0000555555555153 <+10>:    jle    0x55555555516b <rsum+34>
   0x0000555555555155 <+12>:    push   rbx
   0x0000555555555156 <+13>:    mov    rbx,rdi
   0x0000555555555159 <+16>:    sub    rsi,0x1
   0x000055555555515d <+20>:    lea    rdi,[rdi+0x8]
   0x0000555555555161 <+24>:    call   0x555555555149 <rsum>
   0x0000555555555166 <+29>:    add    rax,QWORD PTR [rbx]
   0x0000555555555169 <+32>:    pop    rbx
   0x000055555555516a <+33>:    ret
   0x000055555555516b <+34>:    ret
End of assembler dump.



  long rsum(long *start, long count)
  start in %rdi, count in %rsi
rsum:
    movl    $0, %eax
    testq   %rsi, %rsi      # count
    jle     .L9
    pushq   %rbx            # *start
    movq    (%rdi), %rbx
    subq    $1, %rsi        # count - 1
    addq    $8, %rdi        # start++
    call    rsum
    addq    %rbx, %rax      # sum += *start
    popq    %rbx
.L9:
    rep; ret

y84
rsum:
    irmovq  $0, %rax
    andq    %rsi, %rsi      # count
    jle     .L9
    pushq   %rbx            # *start
    mrmovq  (%rdi), %rbx

    # subq    $1, %rsi        # count - 1
    # addq    $8, %rdi        # start++
    irmovq  $1, %r8
    subq    %r8, %rsi
    irmovq  $8, %r8
    addq    %r8, % 

    call    rsum

    addq    %rbx, %rax      # sum += *start

    andq    %rbx, %rbx      # can void
    jg     .Positive
    xorq    %r8, %r8
    subq    %rbx, %r8
    rrmovq  %r8, %rbx
.Positive:
    addq    %rbx, %rax      # sum += *start

    xorq    %r8, %r8
    subq    %rbx, %r8
    jl     .Negative        # if 0 - %rbx < 0
    rrmovq  %r8, %rbx
.Negative:
    addq    %rbx, %rax      # sum += *start

    xorq    %r8, %r8
    subq    %rbx, %r8
    andq    %rbx, %rbx      # can void
    cmovl   %r8, %rbx
    addq    %rbx, %rax      # sum += *start

    xorq    %r8, %r8
    subq    %rbx, %r8       # if 0 - %rbx > 0
    cmovg   %r8, %rbx
    addq    %rbx, %rax      # sum += *start

    popq    %rbx
.L9:
    rep; ret
*/
