#!/usr/bin/env python3
# coding=utf-8

print('hello python world!')

# Importing libraries
import matplotlib.pyplot as plt
import numpy as np


# Using Numpy to create an array X
k = np.arange(1, 300, 1)

latency = 300 + 20*k # (300/k+20)*k
throughput = 1000/(300/k+20)

# Plotting both the curves simultaneously
plt.plot(k, latency, color='r', label='latency')
plt.plot(k, throughput, color='g', label='throughput')

# Naming the x-axis, y-axis and the whole graph
plt.xlabel("k")
plt.ylabel("ps/GIPS")
plt.title("Pipeline Latency and Throughput")



rst = throughput/latency
plt.plot(k, rst, color='b', label='throughput/latency')

max_rst = np.max(rst)
max_k = -1
for mk in range(1,300):
    if max_rst == (1000/(300/mk+20))/(300 + 20*mk):
        max_k = mk
        break

# (1000/(300/max_k+20))/(300 + 20*max_k)=max_rst
# 1000/((300/max_k+20)*(300 + 20*max_k))
# 1000/( 300*300/max_k + 300*20 + 20*300 + 20*20*max_k )

# # Creating a numpy array
# X = np.array([1])
# Y = np.array([5])
# # Plotting point using sactter method
# plt.scatter(X,Y, color='b', label='max throughput/latency')
plt.scatter(np.array([max_k]), np.array([max_rst]), color='b', label='max throughput/latency:%d,%f' % (max_k, max_rst))

# Adding legend, which helps us recognize the curve according to it's color
plt.legend()

# To load the display window
plt.show()
