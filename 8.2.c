#include <sys/types.h>
#include <unistd.h>
// pid_t fork(void);
#include <stdio.h>  // printf
#include <errno.h>  // errno
#include <string.h> // strerror
#include <stdlib.h> // exit

int main()
{
    pid_t pid;
    int x = 1;

    if ((pid = fork()) < 0)
    {
        fprintf(stderr, "fork error: %s\n", strerror(errno));
        exit(0);
    }

    if (pid == 0)
    { /* Child */
        printf("child:x=%d\t@%p\n", ++x, &x);
        exit(0);
    }

    /* Parent */
    printf("parent:x=%d\t@%p\n", --x, &x);
    exit(0);
}
