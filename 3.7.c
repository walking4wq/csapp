#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef enum
{
    arithmetic,
    xor,
    shift,
    endian, // Little/Big-Endian
    logical,

    unexpected,
} model_t;

static char *model_name[] = {"arithmetic", "xor", "shift", "endian", "logical", "unexpected"};

void fn(model_t model)
{
    int32_t a = 0xff, b = 0xab, c = 0x13, d = 0x11;
    int64_t e = 0x40000000;
    //            4 3 2 1
    //            00000000 // 32bit
    printf("<<< model:%s! >>>\n", model_name[model]);
    printf("a@%p=0x%.2x:%d\nb@%p=0x%.2x:%d\nc@%p=0x%.2x:%d\nd@%p=0x%.2x:%d\ne@%p=0x%.2lx:%ld\n", &a, a, a, &b, b, b, &c, c, c, &d, d, d, &e, e, e);
    switch (model)
    {
    case arithmetic:
        asm volatile(
            "movq %0, %%rax\n"
            "movl $0x1, %%ecx\n"
            "movq $0x3, %%rdx\n"

            "addl %%ecx, (%%rax)\n"  // a += 1
            "subl %%edx, 4(%%rax)\n" // b -= 0x3

            // "movl $16, (%%rax, %%rdx, 4)\n" // d = 0x10
            // "imull $16, (%%rax, %%rdx, 4)\n" // d = 0x11*16 // operand size mismatch for `imul'

            "movl (%%rax, %%rdx, 4), %%ebx\n" // %ebx = d = 0x11
            "imull $16, %%ebx\n"
            "movl %%ebx, (%%rax, %%rdx, 4)\n"

            "incl 8(%%rax)\n"     // c++
            "decl %%ecx\n"        // 1--
            "subq %%rdx, %%rax\n" // &a - 3

            :
            : "r"(&a)
            : "%rax", "%ecx", "%rdx", "%ebx");
        break;
    case xor:
        asm volatile(
            "movl %2, %%eax\n"
            "movl $0, %%eax\n" //    b8 00 00 00 00          mov $0x0,%eax
            "movl %%eax, %0\n"

            "movl %2, %%edx\n"
            "xorl %%edx, %%edx\n" // 31 d2                   xor %edx,%edx
            "movl %%edx, %1\n"
            : "=r"(b), "=r"(c)
            : "r"(a)
            : "%eax", "%edx");
        break;
    case shift:
        asm volatile(
            "movq %0, %%rax\n"
            "shll $2, (%%rax)\n"
            :
            : "r"(&d)
            : "%rax");
        break;
    case endian:
        asm volatile(
            "movl $0x40000000, %%eax\n" // 0x40000000 = 1073741824
            "movl $2, %%edx\n"
            // "imull $2\n" // operand type mismatch for `imul'
            "imull %%edx\n"

            "movq %2, %%rbx\n"       // https://blog.csdn.net/waitingbb123/article/details/80504093
            "movl %%eax, (%%rbx)\n"  // low-order 32 bits
            "movl %%edx, 4(%%rbx)\n" // high-order 32 bits

            "movl $-2, %%eax\n"
            "cltd\n"
            "movl %%eax, %0\n"
            "movl %%edx, %1\n"
            : "=r"(b), "=r"(c)
            : "r"(&e)
            : "%eax", "%edx", "%rbx");
        break;
    default:
        fprintf(stderr, "<<< unsupported model:%s! >>>\n", model_name[model]);
        break;
    }
    printf("*** after asm ***\n");
    printf("a@%p=0x%.2x:%d\nb@%p=0x%.2x:%d\nc@%p=0x%.2x:%d\nd@%p=0x%.2x:%d\ne@%p=0x%.2lx:%ld\n", &a, a, a, &b, b, b, &c, c, c, &d, d, d, &e, e, e);
}

int main()
{
    printf("Hello, World!\n");

    fn(arithmetic);
    fn(xor);
    fn(shift);
    fn(endian);

    int32_t d = 0x11;
    int32_t e = 3;
    e *= d;
    printf("e=%d\n", e);
    int f = e;
    f = 0;
    printf("f=%d\n", f);
    f = e;
    f ^= f;
    printf("f=%d\n", f);
}
