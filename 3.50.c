#include <stdio.h>

float float_mov(float v1, float *src, float *dst)
{
    float v2 = *src;
    *dst = v1;
    return v2;
}

double fcvt(int i, float *fp, double *dp, long *lp)
{
    float f = *fp;
    double d = *dp;
    long l = *lp;
    *lp = (long)d;
    *fp = (float)i;
    *dp = (double)l;
    return (double)f;
}

/*
3.55
0000000000001244 <cel2fahr>:
    1244:       f3 0f 1e fa             endbr64
    1248:       55                      push   %rbp
    1249:       48 89 e5                mov    %rsp,%rbp
    124c:       f2 0f 11 45 f8          movsd  %xmm0,-0x8(%rbp)
    1251:       f2 0f 10 4d f8          movsd  -0x8(%rbp),%xmm1
    1256:       f2 0f 10 05 02 0e 00    movsd  0xe02(%rip),%xmm0        # 2060 <_IO_stdin_used+0x60> # 0x 125e + 0xe02
    125d:       00
    125e:       f2 0f 59 c8             mulsd  %xmm0,%xmm1
    1262:       f2 0f 10 05 fe 0d 00    movsd  0xdfe(%rip),%xmm0        # 2068 <_IO_stdin_used+0x68> # 0x 126a + 0xdfe
    1269:       00
    126a:       f2 0f 59 c1             mulsd  %xmm1,%xmm0
    126e:       66 48 0f 7e c0          movq   %xmm0,%rax
    1273:       66 48 0f 6e c0          movq   %rax,%xmm0
    1278:       5d                      pop    %rbp
    1279:       c3                      ret

0000000000002000 <_IO_stdin_used>:
    2000:       01 00                   add    %eax,(%rax)
    ...
    2060:       cd cc                   int    $0xcc
    2062:       cc                      int3
    2063:       cc                      int3
    2064:       cc                      int3
    2065:       cc                      int3
    2066:       fc                      cld
    2067:       3f                      (bad)
    2068:       00 00                   add    %al,(%rax)
    206a:       00 00                   add    %al,(%rax)
    206c:       00 00                   add    %al,(%rax)
    206e:       40                      rex
    206f:       40 cd cc                rex int $0xcc
    2072:       cc                      int3

0x 3f fc cc cc cc cc cc cd = 1.8
0x 40 40 00 00 00 00 00 00 = 32.0

*/
double cel2fahr(double temp)
{
    return 1.8 * temp * 32.0;
}

#define EXPR(x) x = 0.0 // pxor   %xmm0,%xmm0
double simplefun(double x)
{
    return EXPR(x);
}

/*
andpd  0xd74(%rip),%xmm0        # 20a0 <_IO_stdin_used+0xa0>

    20a0:       ff                      (bad)
    20a1:       ff                      (bad)
    20a2:       ff                      (bad)
    20a3:       ff                      (bad)
    20a4:       ff                      (bad)
    20a5:       ff                      (bad)
    20a6:       ff                      (bad)
    20a7:       7f 00                   jg     20a9 <_IO_stdin_used+0xa9>

andpd  7f ff ff ff ff ff ff ff,%xmm0
*/
#include <math.h>
#define EXPR2(x) fabs(x)
double simplefun2(double x)
{
    return EXPR2(x);
}
/*
xorpd  0xd64(%rip),%xmm0        # 20b0 <_IO_stdin_used+0xb0>

    20a7:       7f 00                   jg     20a9 <_IO_stdin_used+0xa9>
        ...
    20b5:       00 00                   add    %al,(%rax)
    20b7:       80 00 00                addb   $0x0,(%rax)
    20ba:       00 00                   add    %al,(%rax)

xorpd  0x80 00 00 00 00 00 00 00,%xmm0
*/
#define EXPR3(x) -x
double simplefun3(double x)
{
    return EXPR3(x);
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    float v1 = 0.1, src = 0.2, dst = 0.3, v2;
    printf("v1:%f,src:%f,dst:%f,v2:%f\n", v1, src, dst, v2);
    v2 = float_mov(v1, &src, &dst);
    printf("v1:%f,src:%f,dst:%f,v2:%f\n", v1, src, dst, v2);

    int i = 1;
    float fp = 0.2;
    double dp = 0.3;
    long lp = 4;
    double rst;
    printf("i:%d,fp=%f,dp=%f,lp=%ld,rst=%f\n", i, fp, dp, lp, rst);
    rst = fcvt(i, &fp, &dp, &lp);
    printf("i:%d,fp=%f,dp=%f,lp=%ld,rst=%f\n", i, fp, dp, lp, rst);
}
