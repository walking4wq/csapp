#include <stdio.h>

// double aprod(double a[], long n)
// {
//     long i;
//     double x, y, z;
//     double r = 1;
//     for (i = 0; i < n - 2; i += 3)
//     {
//         x = a[i];
//         y = a[i + 1];
//         z = a[i + 2];
//         r = r * x * y * z; /* Product computation */
//     }
//     for (; i < n; i++)
//         r *= a[i];
//     return r;
// }

#define APROD(suffix, product_computation)   \
    double aprod##suffix(double a[], long n) \
    {                                        \
        long i;                              \
        double x, y, z;                      \
        double r = 1;                        \
        for (i = 0; i < n - 2; i += 3)       \
        {                                    \
            x = a[i];                        \
            y = a[i + 1];                    \
            z = a[i + 2];                    \
            r = product_computation;         \
        }                                    \
        for (; i < n; i++)                   \
            r *= a[i];                       \
        return r;                            \
    }

// r = ((r * x) * y) * z; /* A1 */
// r = (r * (x * y)) * z; /* A2 */
// r = r * ((x * y) * z); /* A3 */
// r = r * (x * (y * z)); /* A4 */
// r = (r * x) * (y * z); /* A5 */
APROD(0, r *x *y *z)
APROD(1, ((r * x) * y) * z)
APROD(2, (r * (x * y)) * z)
APROD(3, r *((x * y) * z))
APROD(4, r *(x *(y *z)))
APROD(5, (r * x) * (y * z))

// https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
#define xstr(s) str(s)
#define str(s) #s

#include <stdint.h>

#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

void test_avx();

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }

    const int LEN = 64, TIMES = 2048;
    double arr[LEN], rst, rst2;

    for (int i = 0; i < LEN; i++)
        arr[i] = i + 1;

    rst = aprod0(arr, LEN);

    printf("aprod0:%f\n", rst);
//@formatter:off
// rst2 = aprod1(arr, LEN); if (rst != rst2) { printf("err aprod1:\t%f\n!=\t\t%f\n", rst2, rst); }
// rst2 = aprod2(arr, LEN); if (rst != rst2) { printf("err aprod2:\t%f\n!=\t\t%f\n", rst2, rst); }
// rst2 = aprod3(arr, LEN); if (rst != rst2) { printf("err aprod3:\t%f\n!=\t\t%f\n", rst2, rst); }
// rst2 = aprod4(arr, LEN); if (rst != rst2) { printf("err aprod4:\t%f\n!=\t\t%f\n", rst2, rst); }
// rst2 = aprod5(arr, LEN); if (rst != rst2) { printf("err aprod5:\t%f\n!=\t\t%f\n", rst2, rst); }
//@formatter:no
#define CHECK_RESULT(idx)                                             \
    rst2 = aprod##idx(arr, LEN);                                      \
    if (rst != rst2)                                                  \
    {                                                                 \
        printf("err aprod" xstr(idx) ":\t%f\n!=\t\t%f\n", rst2, rst); \
    }
    CHECK_RESULT(0)
    CHECK_RESULT(1)
    CHECK_RESULT(2)
    CHECK_RESULT(3)
    CHECK_RESULT(4)
    CHECK_RESULT(5)

    uint64_t tsc0, tsc1;

#define TEST_CASE(idx)               \
    rst = 0;                         \
    tsc0 = perf_counter();           \
    for (int i = 0; i < TIMES; i++)  \
    {                                \
        rst += aprod##idx(arr, LEN); \
    }                                \
    tsc1 = perf_counter();           \
    printf("aprod" xstr(idx) ":%lf,%lu, result:%f\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0, rst);

    TEST_CASE(0)
    TEST_CASE(1)
    TEST_CASE(2)
    TEST_CASE(3)
    TEST_CASE(4)
    TEST_CASE(5)

    test_avx();
    return 0;
}

#ifndef DT
#define DT 0
#endif

#if DT == 0
#define data_t int64_t
#else
#define data_t double
#endif

#ifndef data_t
#define data_t int64_t
#endif

#if IDENT
#define OP *
#else
#define IDENT 0
#define OP +
#endif

#ifndef TIMES
#define TIMES 2000
#endif

#ifndef LENGTH // 10*8
#define LENGTH 80
#endif

/* 2 x 2 loop unrolling like combine6 */
void combine2x2(data_t *data, long length, data_t *dest)
{
    long i;
    // long length = vec_length(v);
    long limit = length - 1;
    // data_t *data = get_vec_start(v);
    data_t acc0 = IDENT;
    data_t acc1 = IDENT;

    /* Combine 2 elements at a time */
    for (i = 0; i < limit; i += 2)
    {
        acc0 = acc0 OP data[i];
        acc1 = acc1 OP data[i + 1];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0 OP acc1;
}
/* 8 x 8 loop unrolling */
void combine8x8(data_t *data, long length, data_t *dest)
{
    long i;
    // long length = vec_length(v);
    long limit = length - 7;
    // data_t *data = get_vec_start(v);
    data_t acc0 = IDENT;
    data_t acc1 = IDENT;
    data_t acc2 = IDENT;
    data_t acc3 = IDENT;
    data_t acc4 = IDENT;
    data_t acc5 = IDENT;
    data_t acc6 = IDENT;
    data_t acc7 = IDENT;

    /* Combine 2 elements at a time */
    for (i = 0; i < limit; i += 8)
    {
        acc0 = acc0 OP data[i];
        acc1 = acc1 OP data[i + 1];
        acc2 = acc2 OP data[i + 2];
        acc3 = acc3 OP data[i + 3];
        acc4 = acc4 OP data[i + 4];
        acc5 = acc5 OP data[i + 5];
        acc6 = acc6 OP data[i + 6];
        acc7 = acc7 OP data[i + 7];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0 OP acc1 OP acc2 OP acc3 OP acc4 OP acc5 OP acc6 OP acc7;
}
/* 10 x 10 loop unrolling */
void combine10x10(data_t *data, long length, data_t *dest)
{
    long i;
    // long length = vec_length(v);
    long limit = length - 9;
    // data_t *data = get_vec_start(v);
    data_t acc0 = IDENT;
    data_t acc1 = IDENT;
    data_t acc2 = IDENT;
    data_t acc3 = IDENT;
    data_t acc4 = IDENT;
    data_t acc5 = IDENT;
    data_t acc6 = IDENT;
    data_t acc7 = IDENT;
    data_t acc8 = IDENT;
    data_t acc9 = IDENT;

    /* Combine 10 elements at a time */
    for (i = 0; i < limit; i += 10)
    {
        acc0 = acc0 OP data[i];
        acc1 = acc1 OP data[i + 1];
        acc2 = acc2 OP data[i + 2];
        acc3 = acc3 OP data[i + 3];
        acc4 = acc4 OP data[i + 4];
        acc5 = acc5 OP data[i + 5];
        acc6 = acc6 OP data[i + 6];
        acc7 = acc7 OP data[i + 7];
        acc8 = acc8 OP data[i + 8];
        acc9 = acc9 OP data[i + 9];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0 OP acc1 OP acc2 OP acc3 OP acc4 OP acc5 OP acc6 OP acc7 OP acc8 OP acc9;
}

/* 20 x 20 loop unrolling */
void combine20x20(data_t *data, long length, data_t *dest)
{
    long i;
    // long length = vec_length(v);
    long limit = length - 19;
    // data_t *data = get_vec_start(v);
    data_t acc0 = IDENT;
    data_t acc1 = IDENT;
    data_t acc2 = IDENT;
    data_t acc3 = IDENT;
    data_t acc4 = IDENT;
    data_t acc5 = IDENT;
    data_t acc6 = IDENT;
    data_t acc7 = IDENT;
    data_t acc8 = IDENT;
    data_t acc9 = IDENT;
    data_t acc10 = IDENT;
    data_t acc11 = IDENT;
    data_t acc12 = IDENT;
    data_t acc13 = IDENT;
    data_t acc14 = IDENT;
    data_t acc15 = IDENT;
    data_t acc16 = IDENT;
    data_t acc17 = IDENT;
    data_t acc18 = IDENT;
    data_t acc19 = IDENT;

    /* Combine 20 elements at a time */
    for (i = 0; i < limit; i += 20)
    {
        acc0 = acc0 OP data[i];
        acc1 = acc1 OP data[i + 1];
        acc2 = acc2 OP data[i + 2];
        acc3 = acc3 OP data[i + 3];
        acc4 = acc4 OP data[i + 4];
        acc5 = acc5 OP data[i + 5];
        acc6 = acc6 OP data[i + 6];
        acc7 = acc7 OP data[i + 7];
        acc8 = acc8 OP data[i + 8];
        acc9 = acc9 OP data[i + 9];
        acc10 = acc10 OP data[i + 10];
        acc11 = acc11 OP data[i + 11];
        acc12 = acc12 OP data[i + 12];
        acc13 = acc13 OP data[i + 13];
        acc14 = acc14 OP data[i + 14];
        acc15 = acc15 OP data[i + 15];
        acc16 = acc16 OP data[i + 16];
        acc17 = acc17 OP data[i + 17];
        acc18 = acc18 OP data[i + 18];
        acc19 = acc19 OP data[i + 19];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0 OP acc1 OP acc2 OP acc3 OP acc4 OP acc5 OP acc6 OP acc7 OP acc8 OP acc9
        OP acc10 OP acc11 OP acc12 OP acc13 OP acc14 OP acc15 OP acc16 OP acc17 OP acc18 OP acc19;
}

/*
https://zhuanlan.zhihu.com/p/94649418
https://zhuanlan.zhihu.com/p/381547657

https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html

https://www.cnblogs.com/yutongqing/p/6823385.html
$ gcc -c -Q -march=native --help=target
-mavx2                                [enabled]
...
-mavx512f                             [disabled]
*/
#include <immintrin.h>

#include <limits.h>

void combine_avx(data_t *data, long length, data_t *dest)
{
    long i;
    data_t acc0 = IDENT;
    /*
    // long length = vec_length(v);
    long limit = length - 7;
    // data_t *data = get_vec_start(v);
        data_t acc8[] = {IDENT, IDENT, IDENT, IDENT, IDENT, IDENT, IDENT, IDENT};
    #if (DT == 1)                           // data_t is double
        __m512d acc = _mm512_load_pd(acc8); // AVX512F // -mavx512f // Illegal instruction
        for (i = 0; i < limit; i += 8)
        {
            __m512d acc_ = _mm512_load_pd(&data[i]);
    #if (IDENT == 0)
            acc = _mm512_add_pd(acc, acc_);
    #else
            acc = _mm512_mul_pd(acc, acc_);
    #endif
        }
        _mm512_store_pd(acc8, acc);
    #else // data_t is int64_t
        __m512i acc = _mm512_load_epi64(acc8);
        for (i = 0; i < limit; i += 8)
        {
            __m512i acc_ = _mm512_load_epi64(&data[i]);
    #if (IDENT == 0)
            acc = _mm512_add_epi64(acc, acc_);
    #else
            // acc = _mm512_mul_epi64(acc, acc_);
            acc = _mm512_mullo_epi64(acc, acc_);
    #endif
        }
        _mm512_store_epi64(acc8, acc);
    #endif
    acc0 = acc8[0] OP acc8[1] OP acc8[2] OP acc8[3] OP acc8[4] OP acc8[5] OP acc8[6] OP acc8[7];
    */
    long limit = length - 3;
    data_t acc4[] = {IDENT, IDENT, IDENT, IDENT};
#if (DT == 1)                           // data_t is double
    __m256d acc = _mm256_load_pd(acc4); // -mavx2
    for (i = 0; i < limit; i += 4)
    {
        __m256d acc_ = _mm256_load_pd(&data[i]);
#if (IDENT == 0)
        acc = _mm256_add_pd(acc, acc_);
#else
        acc = _mm256_mul_pd(acc, acc_);
#endif
    }
    _mm256_store_pd(acc4, acc);
#else // data_t is int64_t
    // __m256i acc = _mm256_load_epi64(acc4);
    __m256i mask = _mm256_set1_epi64x(ULLONG_MAX);
    // __m256i acc = _mm256_maskload_epi64(acc4, mask);
    __m256i acc = _mm256_set_epi64x(IDENT, IDENT, IDENT, IDENT);
    for (i = 0; i < limit; i += 4)
    {
        // __m256i acc_ = _mm256_load_epi64(&data[i]);
        // __m256i acc_ = _mm256_maskload_epi64(&data[i], mask);
        __m256i acc_ = _mm256_set_epi64x(data[i + 3], data[i + 2], data[i + 1], data[i]);
#if (IDENT == 0)
        acc = _mm256_add_epi64(acc, acc_);
#else
        acc = _mm256_mul_epi32(acc, acc_); // no _mm256_mul_epi64
#endif
    }
    // _mm256_store_epi64(acc4, acc);
    _mm256_maskstore_epi64(acc4, mask, acc);
#endif

    acc0 = acc4[0] OP acc4[1] OP acc4[2] OP acc4[3];
    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0;
}
/*
CFLAGS="-g -O2 -mavx2"
gcc $CFLAGS 5.8.c -o 5.8
./5.8
gcc $CFLAGS 5.8.c -o 5.8 -DIDENT=1
./5.8
gcc $CFLAGS 5.8.c -o 5.8 _DDT=1 -Ddata_t=double
./5.8
gcc $CFLAGS 5.8.c -o 5.8 _DDT=1 -Ddata_t=double -DIDENT=1
./5.8
*/
void test_avx()
{
    printf("DT:" xstr(DT) ",\tdata_t:" xstr(data_t) ",\tOP:" xstr(OP) ",\tIDENT:" xstr(IDENT) ",\tTIMES:" xstr(TIMES) ",\tLENGTH:" xstr(LENGTH) "\n");

    data_t data[LENGTH];
    for (int i = 0; i < LENGTH; i++)
    {
        data[i] = i + 1;
    }
    data_t dest;
    combine2x2(data, LENGTH, &dest);
    printf("combine2x2 rst:%ld/%lf\n", (int64_t)dest, (double)dest);
    combine8x8(data, LENGTH, &dest);
    printf("combine8x8 rst:%ld/%lf\n", (int64_t)dest, (double)dest);
    combine10x10(data, LENGTH, &dest);
    printf("combine10x10 rst:%ld/%lf\n", (int64_t)dest, (double)dest);
    combine_avx(data, LENGTH, &dest);
    printf("combine_avx rst:%ld/%lf\n", (int64_t)dest, (double)dest);
    combine20x20(data, LENGTH, &dest);
    printf("combine20x20 rst:%ld/%lf\n", (int64_t)dest, (double)dest);

    uint64_t tsc0, tsc1;
    data_t rst[TIMES];

    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        combine2x2(data, LENGTH, &rst[i]);
    }
    tsc1 = perf_counter();
#if (data_t != double)
    for (int i = 0; i < TIMES; i++)
    {
        if (rst[i] != dest)
        {
            printf("rst[%d]:%ld/%lf!=dest:%ld/%lf\n", i, (int64_t)rst[i], (double)rst[i], (int64_t)dest, (double)dest);
        }
    }
#endif
    printf("combine2x2 cycle:%lf,%lu\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);

    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        combine8x8(data, LENGTH, &rst[i]);
    }
    tsc1 = perf_counter();
#if (data_t != double)
    for (int i = 0; i < TIMES; i++)
    {
        if (rst[i] != dest)
        {
            printf("rst[%d]:%ld/%lf!=dest:%ld/%lf\n", i, (int64_t)rst[i], (double)rst[i], (int64_t)dest, (double)dest);
        }
    }
#endif
    printf("combine8x8 cycle:%lf,%lu\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);

    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        combine10x10(data, LENGTH, &rst[i]);
    }
    tsc1 = perf_counter();
#if (data_t != double)
    for (int i = 0; i < TIMES; i++)
    {
        if (rst[i] != dest)
        {
            printf("rst[%d]:%ld/%lf!=dest:%ld/%lf\n", i, (int64_t)rst[i], (double)rst[i], (int64_t)dest, (double)dest);
        }
    }
#endif
    printf("combine10x10 cycle:%lf,%lu\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);

    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        combine20x20(data, LENGTH, &rst[i]);
    }
    tsc1 = perf_counter();
#if (data_t != double)
    for (int i = 0; i < TIMES; i++)
    {
        if (rst[i] != dest)
        {
            printf("rst[%d]:%ld/%lf!=dest:%ld/%lf\n", i, (int64_t)rst[i], (double)rst[i], (int64_t)dest, (double)dest);
        }
    }
#endif
    printf("combine20x20 cycle:%lf,%lu\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);

    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        combine_avx(data, LENGTH, &rst[i]);
    }
    tsc1 = perf_counter();
#if (data_t != double)
    for (int i = 0; i < TIMES; i++)
    {
        if (rst[i] != dest)
        {
            printf("rst[%d]:%ld/%lf!=dest:%ld/%lf\n", i, (int64_t)rst[i], (double)rst[i], (int64_t)dest, (double)dest);
        }
    }
#endif
    printf("combine_avx cycle:%lf,%lu\n", (double)(tsc1 - tsc0) / TIMES, tsc1 - tsc0);
}
