int main()
{
    write(1, "hello, world\n", 13);
    _exit(0);
}

/*

─────────────────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
   0x7ffff7e98a11 <read+145>       mov    DWORD PTR fs:[rdx], eax
   0x7ffff7e98a14 <read+148>       mov    rax, 0xffffffffffffffff
   0x7ffff7e98a1b <read+155>       jmp    0x7ffff7e989d4 <__GI___libc_read+84>
   0x7ffff7e98a1d                  nop    DWORD PTR [rax]
 → 0x7ffff7e98a20 <write+0>        endbr64
   0x7ffff7e98a24 <write+4>        mov    eax, DWORD PTR fs:0x18
   0x7ffff7e98a2c <write+12>       test   eax, eax
   0x7ffff7e98a2e <write+14>       jne    0x7ffff7e98a40 <__GI___libc_write+32>
   0x7ffff7e98a30 <write+16>       mov    eax, 0x1
   0x7ffff7e98a35 <write+21>       syscall                                                                  <
   0x7ffff7e98a37 <write+23>       cmp    rax, 0xfffffffffffff000
   0x7ffff7e98a3d <write+29>       ja     0x7ffff7e98a90 <__GI___libc_write+112>
─────────────────────────────────────────────────────────────────────────────────────────────────────────── threads ────
[#0] Id 1, Name: "8.1.3", stopped 0x7ffff7e98a20 in __GI___libc_write (), reason: SINGLE STEP
───────────────────────────────────────────────────────────────────────────────────────────────────────────── trace ────
[#0] 0x7ffff7e98a20 → __GI___libc_write(fd=0x1, buf=0x555555556004, nbytes=0xd)
[#1] 0x5555555550a2 → main()
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

─────────────────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
   0x7ffff7e6ec61 <vfork+49>       mov    DWORD PTR fs:[rcx], eax
   0x7ffff7e6ec64 <vfork+52>       or     rax, 0xffffffffffffffff
   0x7ffff7e6ec68 <vfork+56>       ret
   0x7ffff7e6ec69                  nop    DWORD PTR [rax+0x0]
 → 0x7ffff7e6ec70 <_exit+0>        endbr64
   0x7ffff7e6ec74 <_exit+4>        mov    r8, QWORD PTR [rip+0x12e195]        # 0x7ffff7f9ce10
   0x7ffff7e6ec7b <_exit+11>       mov    esi, 0xe7
   0x7ffff7e6ec80 <_exit+16>       mov    edx, 0x3c
   0x7ffff7e6ec85 <_exit+21>       jmp    0x7ffff7e6ec9d <__GI__exit+45>
   0x7ffff7e6ec87 <_exit+23>       nop    WORD PTR [rax+rax*1+0x0]
   0x7ffff7e6ec90 <_exit+32>       mov    eax, edx
   0x7ffff7e6ec92 <_exit+34>       syscall                                                                  <
   0x7ffff7e6eca1 <_exit+49>       cmp    rax, 0xfffffffffffff000
   0x7ffff7e6eca7 <_exit+55>       jbe    0x7ffff7e6ec90 <__GI__exit+32>
   0x7ffff7e6eca9 <_exit+57>       neg    eax
   0x7ffff7e6ecab <_exit+59>       mov    DWORD PTR fs:[r8], eax
   0x7ffff7e6ecaf <_exit+63>       jmp    0x7ffff7e6ec90 <__GI__exit+32>
   0x7ffff7e6ecb1 <_exit+65>       nop    DWORD PTR [rax+0x0]
─────────────────────────────────────────────────────────────────────────────────────────────────────────── threads ────
[#0] Id 1, Name: "8.1.3", stopped 0x7ffff7e6ec70 in __GI__exit (), reason: SINGLE STEP
───────────────────────────────────────────────────────────────────────────────────────────────────────────── trace ────
[#0] 0x7ffff7e6ec70 → __GI__exit(status=0x0)
[#1] 0x5555555550a9 → main()
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

*/
