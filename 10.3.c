#include "csapp.h"

int main(int argc, char **argv)
{
    struct stat stat;
    char *type, *readok;

    Stat(argv[1], &stat);
    if (S_ISREG(stat.st_mode))
        /* Determine file type */
        type = "regular";
    else if (S_ISDIR(stat.st_mode))
        type = "directory";
    else
        type = "other";
    if ((stat.st_mode & S_IRUSR)) /* Check read access */
        readok = "yes";
    else
        readok = "no";

    printf("type: %s, read: %s\n", type, readok);
    // __dev_t st_dev;		/* Device.  */
    // __ino64_t st_ino;		/* File serial number.  */
    // __nlink_t st_nlink;		/* Link count.  */
    printf("dev:%ld,ino:%ld,nlink:%ld\n", stat.st_dev, stat.st_ino, stat.st_nlink);
    // __uid_t st_uid;		/* User ID of the file's owner.	*/
    // __gid_t st_gid;		/* Group ID of the file's group.*/
    printf("id4usr:%d,id4grp:%d\n", stat.st_uid, stat.st_gid);
    // __off64_t st_size;			/* Size of file, in bytes.  */
    // __blksize_t st_blksize;	/* Optimal block size for I/O.  */
    // __blkcnt64_t st_blocks;	/* Nr. 512-byte blocks allocated.  */
    printf("bytes:%ld,siz4blk:%ld,cnt4blk:%ld\n", stat.st_size, stat.st_blksize, stat.st_blocks);
    // struct timespec st_atim;		/* Time of last access.  */
    // struct timespec st_mtim;		/* Time of last modification.  */
    // struct timespec st_ctim;		/* Time of last status change.  */
    printf("atim:%ld/%ld,mtim:%ld/%ld,ctim:%ld/%ld\n",
           stat.st_atim.tv_sec, stat.st_atim.tv_nsec,
           stat.st_mtim.tv_sec, stat.st_mtim.tv_nsec,
           stat.st_ctim.tv_sec, stat.st_ctim.tv_nsec);

    if (S_ISDIR(stat.st_mode))
    {
        DIR *streamp;
        struct dirent *dep;

        streamp = Opendir(argv[1]);

        errno = 0;
        while ((dep = readdir(streamp)) != NULL)
        {
            printf("Found file: %s\n", dep->d_name);
        }
        if (errno != 0)
            unix_error("readdir error");
        Closedir(streamp);
    }

    int fd;
    char c;

    fd = Open(argv[1], O_RDONLY, 0);
    if (Fork() == 0)
    {
        Read(fd, &c, 1);
        printf("child c = %c\n", c);
        exit(0);
    }
    Wait(NULL);
    Read(fd, &c, 1);
    printf("parent c = %c\n", c);

    int fd2;
    fd2 = Open(argv[1], O_RDONLY, 0);
    Read(fd2, &c, 1);
    printf("parent reopen c = %c\n", c);

    exit(0);
}
