#include <stdint.h>
#include <stdio.h>

// https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html#Stringizing
#define STR_EXPANDED(s) STR(s)
#define STR(s) #s

typedef int32_t data_t;
#define COMP <
data_t a[] = {1, 1, 2};
data_t b[] = {1, 2, 1};
int len = sizeof(a) / sizeof(data_t), t;

int comp(data_t a, data_t b)
{
    return a COMP b;
}

int asm4comp(data_t a, data_t b)
{
    int t;
    asm volatile(
        "movl   %1, %%edx\n"    // a
        "movl   %2, %%eax\n"    // b
        "cmpl   %%eax, %%edx\n" // t = a - b
        "setl   %%al\n"         // SF^OF // t=a+b => t<0 ^ (a<0 == b<0)&&(t<0 != a<0)
        // Assume initially that %dh = CD, %eax = 98765432
        // movb   %dh,%al  // %eax = 987654CD
        // movsbl %dh,%eax // %eax = FFFFFFCD
        // movzbl %dh,%eax // %eax = 000000CD
        "movzbl %%al, %%eax\n"
        "movl   %%eax, %0\n"
        : "=r"(t)
        : "r"(a), "r"(b)
        : "%eax", "%ebx", "%ecx");

    // t = a + b
    // SF^OF // t<0 ^ (a < 0 == b < 0)&&(t < 0 != a < 0)
    // a = 1, b = 2 // 1+-2 = -1
    // true ^ (false == true) && (true != false) = true^false = 1
    //  a = 2, b = 1 // 2+-1 = 1
    // false ^ (false == true) && (false != false) = false^false = 0
    return t;
}

int main()
{
    printf("Hello, World!\n");

    // data_t a[] = {1, 1, 2};
    // data_t b[] = {1, 2, 1};
    // int len = sizeof(a) / sizeof(data_t), t;

    for (int i = 0; i < len; i++)
    {
        printf("a=%d,b=%d,t=a%sb\n", a[i], b[i], STR_EXPANDED(COMP));
        t = comp(a[i], b[i]);
        printf("*** after func ***\n");
        printf("a=%d,b=%d,t=a%sb=%d\n", a[i], b[i], STR_EXPANDED(COMP), t);
        t = asm4comp(a[i], b[i]);
        printf("*** after asm ***\n");
        printf("a=%d,b=%d,t=a%sb=%d\n\n", a[i], b[i], STR_EXPANDED(COMP), t);
    }
}

int absdiff(int x, int y)
{
    if (x < y)
        return y - x;
    else
        return x - y;
}

int gotodiff(int x, int y)
{
    int result;
    if (x >= y)
        goto x_ge_y;
    result = y - x;
    goto done;
x_ge_y:
    result = x - y;
done:
    return result;
}

/**
 * high performance: '-march=i686' // no effective
 */
int cmovdiff(int x, int y)
{
    int tval = y - x;
    int rval = x - y;
    int test = x < y;
    // Line below requires single instruction:
    if (test)
        rval = tval;
    return rval;
}

// -O0
/*
absdiff:
.LFB3:
    .cfi_startproc
    endbr64
    pushq	%rbp
    .cfi_def_cfa_offset 16
    .cfi_offset 6, -16
    movq	%rsp, %rbp
    .cfi_def_cfa_register 6
    movl	%edi, -4(%rbp)
    movl	%esi, -8(%rbp)
    movl	-4(%rbp), %eax
    cmpl	-8(%rbp), %eax
    jge	.L10
    movl	-8(%rbp), %eax
    subl	-4(%rbp), %eax
    jmp	.L11
.L10:
    movl	-4(%rbp), %eax
    subl	-8(%rbp), %eax
.L11:
    popq	%rbp
    .cfi_def_cfa 7, 8
    ret
    .cfi_endproc
.LFE3:
    .size	absdiff, .-absdiff
    .globl	gotodiff
    .type	gotodiff, @function
...
cmovdiff:
.LFB5:
    .cfi_startproc
    endbr64
    pushq	%rbp
    .cfi_def_cfa_offset 16
    .cfi_offset 6, -16
    movq	%rsp, %rbp
    .cfi_def_cfa_register 6
    movl	%edi, -20(%rbp)
    movl	%esi, -24(%rbp)
    movl	-24(%rbp), %eax
    subl	-20(%rbp), %eax
    movl	%eax, -8(%rbp)
    movl	-20(%rbp), %eax
    subl	-24(%rbp), %eax
    movl	%eax, -12(%rbp)
    movl	-20(%rbp), %eax
    cmpl	-24(%rbp), %eax
    setl	%al
    movzbl	%al, %eax
    movl	%eax, -4(%rbp)
    cmpl	$0, -4(%rbp)
    je	.L20
    movl	-8(%rbp), %eax
    movl	%eax, -12(%rbp)
.L20:
    movl	-12(%rbp), %eax
    popq	%rbp
    .cfi_def_cfa 7, 8
    ret
    .cfi_endproc
.LFE5:
    .size	cmovdiff, .-cmovdiff
    .globl	cread
    .type	cread, @function
*/
// -O2 is all:
/*
absdiff:
.LFB26:
    .cfi_startproc
    endbr64
    movl	%esi, %edx
    movl	%edi, %eax
    subl	%edi, %edx
    subl	%esi, %eax
    cmpl	%esi, %edi
    cmovl	%edx, %eax
    ret
    .cfi_endproc
.LFE26:
    .size	absdiff, .-absdiff
    .p2align 4
    .globl	gotodiff
    .type	gotodiff, @function
...
cmovdiff:
.LFB28:
    .cfi_startproc
    endbr64
    movl	%edi, %edx
    movl	%esi, %eax
    subl	%esi, %edx
    subl	%edi, %eax
    cmpl	%edi, %esi
    cmovle	%edx, %eax
    ret
    .cfi_endproc
.LFE28:
    .size	cmovdiff, .-cmovdiff
    .p2align 4
    .globl	cread
    .type	cread, @function
*/

int cread(int *xp)
{
    return xp ? *xp : 0;
}
/*
cread:
.LFB6:
    .cfi_startproc
    endbr64
    pushq	%rbp
    .cfi_def_cfa_offset 16
    .cfi_offset 6, -16
    movq	%rsp, %rbp
    .cfi_def_cfa_register 6
    movq	%rdi, -8(%rbp)
    cmpq	$0, -8(%rbp)
    je	.L23
    movq	-8(%rbp), %rax
    movl	(%rax), %eax
    jmp	.L25
.L23:
    movl	$0, %eax
.L25:
    popq	%rbp
    .cfi_def_cfa 7, 8
    ret
    .cfi_endproc
.LFE6:
    .size	cread, .-cread
    .ident	"GCC: (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0"
    .section	.note.GNU-stack,"",@progbits
    .section	.note.gnu.property,"a"
    .align 8
    .long	1f - 0f
    .long	4f - 1f
    .long	5



cread:
.LFB29:
    .cfi_startproc
    endbr64
    xorl	%eax, %eax
    testq	%rdi, %rdi
    je	.L19
    movl	(%rdi), %eax
.L19:
    ret
    .cfi_endproc
.LFE29:
    .size	cread, .-cread
    .globl	t
    .bss
    .align 4
    .type	t, @object
    .size	t, 4

*/

int lcount = 0;
int absdiff_se(int x, int y)
{
    return x < y ? (lcount++, y - x) : x - y;
}

int arith(int x)
{
    return x < 0 ? (x + 3) : (x / 4);
}

/*
arith:
.LFB31:
    .cfi_startproc
    endbr64
    movl	%edi, %eax
    leal	3(%rdi), %edx
    sarl	$2, %eax
    testl	%edi, %edi
    cmovs	%edx, %eax
    ret
*/

int arith_key(int x)
{
    return x / 4;
}
/*
arith_key:
.LFB32:
    .cfi_startproc
    endbr64
    testl	%edi, %edi
    leal	3(%rdi), %eax
    cmovns	%edi, %eax
    sarl	$2, %eax
    ret
*/