#include <stdint.h>
#include <stdio.h>

struct prob
{
    int *p; // 8 byte
    struct
    {
        int x; // 4 byte
        int y;
    } s;
    struct prob *next; // 8 byte
};
/*
on ia-32, not on x86-64
sp at %ebp+8
1   movl    8(%ebp), %eax   // &(sp->p)
2   movl    8(%eax), %edx   // sp->s.y
3   movl    %edx, 4(%eax)   // sp->s.x = sp->s.y
4   leal    4(%eax), %edx   // &(sp->s.x)
5   movl    %edx, (%eax)    // sp->p = &(sp->s.x)
6   movl    %eax, 12(%eax)  // sp->next = sp
*/
void sp_init(struct prob *sp)
{
    sp->s.x = sp->s.y;
    sp->p = &(sp->s.x);
    sp->next = sp;
}
/*
(gdb) disass /s sp_init
Dump of assembler code for function sp_init:
3.39.c:
25      {
   0x0000000000001189 <+0>:     endbr64
   0x000000000000118d <+4>:     push   %rbp
   0x000000000000118e <+5>:     mov    %rsp,%rbp
   0x0000000000001191 <+8>:     mov    %rdi,-0x8(%rbp)

26          sp->s.x = sp->s.y;
   0x0000000000001195 <+12>:    mov    -0x8(%rbp),%rax
   0x0000000000001199 <+16>:    mov    0xc(%rax),%edx
   0x000000000000119c <+19>:    mov    -0x8(%rbp),%rax
   0x00000000000011a0 <+23>:    mov    %edx,0x8(%rax)

27          sp->p = &(sp->s.x);
   0x00000000000011a3 <+26>:    mov    -0x8(%rbp),%rax
   0x00000000000011a7 <+30>:    lea    0x8(%rax),%rdx
   0x00000000000011ab <+34>:    mov    -0x8(%rbp),%rax
   0x00000000000011af <+38>:    mov    %rdx,(%rax)

28          sp->next = sp;
   0x00000000000011b2 <+41>:    mov    -0x8(%rbp),%rax
   0x00000000000011b6 <+45>:    mov    -0x8(%rbp),%rdx
   0x00000000000011ba <+49>:    mov    %rdx,0x10(%rax)

29      }
   0x00000000000011be <+53>:    nop
   0x00000000000011bf <+54>:    pop    %rbp
   0x00000000000011c0 <+55>:    ret
End of assembler dump.

(gdb) disass /s sp_init
Dump of assembler code for function sp_init:
3.39.c:
25      {
   0x0000000000001290 <+0>:     endbr64 

26          sp->s.x = sp->s.y;
   0x0000000000001294 <+4>:     mov    0xc(%rdi),%eax

28          sp->next = sp;
   0x0000000000001297 <+7>:     mov    %rdi,0x10(%rdi)

26          sp->s.x = sp->s.y;
   0x000000000000129b <+11>:    mov    %eax,0x8(%rdi)

27          sp->p = &(sp->s.x);
   0x000000000000129e <+14>:    lea    0x8(%rdi),%rax
   0x00000000000012a2 <+18>:    mov    %rax,(%rdi)

28          sp->next = sp;
   0x00000000000012a5 <+21>:    ret    
End of assembler dump.

*/
int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }

    int p = 1;
    struct prob pb = {&p, {2, 3}, 0};
    pb.next = &pb;

    printf("sizeof(struct prob)=%ld\np=%d@\t\t\t%p\nprob@\t\t\t%p\n.p=%p@\t%p\n.s@\t\t\t%p\n"
           ".s.x=%d=0x%.2x@\t\t%p\n.s.y=%d=0x%.2x@\t\t%p\n.next=%p@\t%p\n",
           sizeof(pb), p, &p, &pb, pb.p, &pb.p, &pb.s,
           pb.s.x, pb.s.x, &pb.s.x,
           pb.s.y, pb.s.y, &pb.s.y, pb.next, &pb.next);
}
