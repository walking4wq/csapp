#include <stdio.h>  // printf
#include <stdlib.h> // exit
#include <signal.h> // SIGINT
#include <unistd.h> // sleep

#include "csapp.h"

/* SIGINT handler */
void handler(int sig)
{
    // printf("Caught signal:%d\n", sig);
    // http://www.cs.cmu.edu/afs/cs/academic/class/15213-f05/code/
    // rio_writen(STDOUT_FILENO, "Caught signal:%d\n", 1);
    // http://csapp.cs.cmu.edu/3e/ics3/code/include/csapp.h
    Sio_puts("Caught SIGINT:"); /* Safe output */
    Sio_putl(sig);              /* Safe output */
    Sio_puts("!\n");            /* Safe output */
    // _exit(0);                     /* Safe exit */
    return; /* Catch the signal and return */
}

unsigned int snooze(unsigned int secs)
{
    unsigned int rc = sleep(secs);

    printf("Slept for %d of %d secs.\n", secs - rc, secs);
    return rc;
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <secs>\n", argv[0]);
        exit(0);
    }

    // if (signal(SIGINT, SIG_IGN) == SIG_ERR)
    // if (signal(SIGINT, SIG_DFL) == SIG_ERR)
    // if (signal(SIGHUP, handler) == SIG_ERR)
    if (signal(SIGINT, handler) == SIG_ERR) /* Install SIGINT */
        // unix_error("signal error\n");
        perror("signal error\n");
    if (signal(SIGQUIT, handler) == SIG_ERR) /* Install SIGQUIT */
        perror("signal error\n");
    /* handler
     */
    (void)snooze(atoi(argv[1]));
    exit(0);
}
