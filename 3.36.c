#include <stdint.h>
#include <stdio.h>

/*
1   int mat1[M][N];
2   int mat2[N][M];
3
4   int sum_element(int i, int j) {
5     return mat1[i][j] + mat2[j][i];
6   }

    i at %ebp+8, j at %ebp+12
1   movl    8(%ebp), %ecx           // i
2   movl    12(%ebp), %edx          // j
3   leal    0(,%ecx,8), %eax        // i*8
4   subl    %ecx, %eax              // i*8-i = i*7
5   addl    %edx, %eax              // i*7 + j
6   leal    (%edx,%edx,4), %edx     // j*5
7   addl    %ecx, %edx              // j*5 + i
8   movl    mat1(,%eax,4), %eax     // mat1 + 4*(i*7 + j)
9   addl    mat2(,%edx,4), %eax     // mat2 + 4*(j*5 + i)

#define M 5
#define N 7
*/

#define M 7
#define N 5
int mat1[M][N];
int mat2[N][M];

int sum_element(int i, int j)
{
    return mat1[i][j] + mat2[j][i];
}
/*
136         // x/8xw 0x4040 // # 0x4040 <mat1>
137         sum_element(1, 2);
   0x000055555555558e <+925>:   mov    $0x2,%esi                                        // j
   0x0000555555555593 <+930>:   mov    $0x1,%edi                                        // i
   0x0000555555555598 <+935>:   call   0x555555555189 <sum_element>
   0x000055555555559d <+940>:   mov    $0x0,%eax

(gdb) disass sum_element
Dump of assembler code for function sum_element:
   0x0000555555555189 <+0>:     endbr64
   0x000055555555518d <+4>:     push   %rbp
   0x000055555555518e <+5>:     mov    %rsp,%rbp
   0x0000555555555191 <+8>:     mov    %edi,-0x4(%rbp)                                  // 1 // i
   0x0000555555555194 <+11>:    mov    %esi,-0x8(%rbp)                                  // 2 // j
=> 0x0000555555555197 <+14>:    mov    -0x8(%rbp),%eax
   0x000055555555519a <+17>:    movslq %eax,%rcx                                        // j
   0x000055555555519d <+20>:    mov    -0x4(%rbp),%eax                                  // i
   0x00005555555551a0 <+23>:    movslq %eax,%rdx                                        // i
   0x00005555555551a3 <+26>:    mov    %rdx,%rax                                        // i
   0x00005555555551a6 <+29>:    shl    $0x2,%rax                                        // i<<2 // i*4
   0x00005555555551aa <+33>:    add    %rdx,%rax                                        // i*4 + i // i*5
   0x00005555555551ad <+36>:    add    %rcx,%rax                                        // i*5 + j
   0x00005555555551b0 <+39>:    lea    0x0(,%rax,4),%rdx                                // 4*(i*5 + j)
   0x00005555555551b8 <+47>:    lea    0x2e81(%rip),%rax        # 0x555555558040 <mat1> // 0x00005555555551bf + 0x2e81 = 0x555555558040
   0x00005555555551bf <+54>:    mov    (%rdx,%rax,1),%ecx                               // mat1 + 4*(i*5 + j)
   0x00005555555551c2 <+57>:    mov    -0x4(%rbp),%eax                                  // i
   0x00005555555551c5 <+60>:    movslq %eax,%rsi                                        // i
   0x00005555555551c8 <+63>:    mov    -0x8(%rbp),%eax                                  // j
   0x00005555555551cb <+66>:    movslq %eax,%rdx                                        // j
   0x00005555555551ce <+69>:    mov    %rdx,%rax                                        // j
   0x00005555555551d1 <+72>:    shl    $0x3,%rax                                        // j<<3 // j*8
   0x00005555555551d5 <+76>:    sub    %rdx,%rax                                        // j*8 - j // j*7
   0x00005555555551d8 <+79>:    add    %rsi,%rax                                        // j*7 + i
   0x00005555555551db <+82>:    lea    0x0(,%rax,4),%rdx                                // 4*(j*7 + i)
   0x00005555555551e3 <+90>:    lea    0x2ef6(%rip),%rax        # 0x5555555580e0 <mat2>
   0x00005555555551ea <+97>:    mov    (%rdx,%rax,1),%eax                               // mat2 + 4*(j*7 + i)
   0x00005555555551ed <+100>:   add    %ecx,%eax
   0x00005555555551ef <+102>:   pop    %rbp
   0x00005555555551f0 <+103>:   ret
End of assembler dump.
(gdb) x/60xw 0x555555558040
0x555555558040 <mat1>:  0x00000000      0x00000001      0x00000002      0x00000003
0x555555558050 <mat1+16>:       0x00000004      0x00000010      0x00000011      0x00000012
0x555555558060 <mat1+32>:       0x00000013      0x00000014      0x00000020      0x00000021
0x555555558070 <mat1+48>:       0x00000022      0x00000023      0x00000024      0x00000030
0x555555558080 <mat1+64>:       0x00000031      0x00000032      0x00000033      0x00000034
0x555555558090 <mat1+80>:       0x00000040      0x00000041      0x00000042      0x00000043
0x5555555580a0 <mat1+96>:       0x00000044      0x00000050      0x00000051      0x00000052
0x5555555580b0 <mat1+112>:      0x00000053      0x00000054      0x00000060      0x00000061
0x5555555580c0 <mat1+128>:      0x00000062      0x00000063      0x00000064      0x00000000
0x5555555580d0: 0x00000000      0x00000000      0x00000000      0x00000000
0x5555555580e0 <mat2>:  0x00000000      0x00000001      0x00000002      0x00000003
0x5555555580f0 <mat2+16>:       0x00000004      0x00000005      0x00000006      0x00000010
0x555555558100 <mat2+32>:       0x00000011      0x00000012      0x00000013      0x00000014
0x555555558110 <mat2+48>:       0x00000015      0x00000016      0x00000020      0x00000021
0x555555558120 <mat2+64>:       0x00000022      0x00000023      0x00000024      0x00000025

*/

typedef int fix_matrix[N][N];
/* Compute i,k of fixed matrix product */
int fix_prod_ele(fix_matrix A, fix_matrix B, int i, int k)
{
    int j;
    int result = 0;
    for (j = 0; j < N; j++)
        result += A[i][j] * B[j][k];
    return result;
}
/* Compute i,k of fixed matrix product */
int fix_prod_ele_opt(fix_matrix A, fix_matrix B, int i, int k)
{
    int *Arow = &A[i][0];
    int *Bptr = &B[0][k];
    int result = 0;
    int j;
    for (j = 0; j != N; j++)
    {
        result += Arow[j] * *Bptr;
        Bptr += N;
    }
    return result;
}

int var_ele(int n, int A[n][n], int i, int j)
{
    return A[i][j];
}
/*
231         var_ele(N, mat3, 1, 2);
   0x00005555555558aa <+1352>:  lea    -0x70(%rbp),%rax
   0x00005555555558ae <+1356>:  mov    $0x2,%ecx            // j
   0x00005555555558b3 <+1361>:  mov    $0x1,%edx            // i
   0x00005555555558b8 <+1366>:  mov    %rax,%rsi            // mat3
   0x00005555555558bb <+1369>:  mov    $0x5,%edi            // n
=> 0x00005555555558c0 <+1374>:  call   0x55555555530e <var_ele>
   0x00005555555558c5 <+1379>:  mov    $0x0,%eax

(gdb) disassemble /s
Dump of assembler code for function var_ele:
3.36.c:
122     {
   0x000055555555530e <+0>:     endbr64 
   0x0000555555555312 <+4>:     push   %rbp
   0x0000555555555313 <+5>:     mov    %rsp,%rbp
   0x0000555555555316 <+8>:     mov    %edi,-0x14(%rbp)     // n
   0x0000555555555319 <+11>:    mov    %rsi,-0x20(%rbp)     // mat3
   0x000055555555531d <+15>:    mov    %edx,-0x18(%rbp)     // i
   0x0000555555555320 <+18>:    mov    %ecx,-0x24(%rbp)     // j

121     int var_ele(int n, int A[n][n], int i, int j)
=> 0x0000555555555323 <+21>:    mov    -0x14(%rbp),%eax
   0x0000555555555326 <+24>:    movslq %eax,%rdx            // n
   0x0000555555555329 <+27>:    sub    $0x1,%rdx            // n-1
   0x000055555555532d <+31>:    mov    %rdx,-0x8(%rbp)      // 
   0x0000555555555331 <+35>:    movslq %eax,%rdx            // n
   0x0000555555555334 <+38>:    mov    %rdx,%r8
   0x0000555555555337 <+41>:    mov    $0x0,%r9d

123         return A[i][j];
   0x000055555555533d <+47>:    mov    -0x18(%rbp),%edx     // i
   0x0000555555555340 <+50>:    movslq %edx,%rdx            // i
   0x0000555555555343 <+53>:    cltq                        // $rax = n
   0x0000555555555345 <+55>:    imul   %rdx,%rax            // n*i
   0x0000555555555349 <+59>:    lea    0x0(,%rax,4),%rdx    // n*i*4
   0x0000555555555351 <+67>:    mov    -0x20(%rbp),%rax     // mat3
   0x0000555555555355 <+71>:    add    %rax,%rdx            // mat3 + n*i*4
   0x0000555555555358 <+74>:    mov    -0x24(%rbp),%eax     // j
   0x000055555555535b <+77>:    cltq                        // $rax = j
   0x000055555555535d <+79>:    mov    (%rdx,%rax,4),%eax   // mat3+n*i*4 +j*4

124     }
   0x0000555555555360 <+82>:    pop    %rbp
   0x0000555555555361 <+83>:    ret    
End of assembler dump.

*/
int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }

    int16_t S[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int len = sizeof(S) / sizeof(int16_t);
    printf("S=\t\t%p\n", S);
    for (int i = 0; i < len; i++)
    {
        printf("S[%d]=%hd\t\t%p\n", i, S[i], &S[i]);
    }
    int16_t *c_addr, *asm_addr;
    int16_t c_val, asm_val;
    // %edx = S
    // %ecx = i
    // ptr to %eax
    // val to %ax
    c_addr = S + 1;
    asm_addr = 0;
    asm volatile(
        "movq %1, %%rdx\n"
        "leaq 2(%%rdx), %%rax\n" // S+1
        "movq %%rax, %0\n"
        : "=r"(asm_addr)
        : "r"(S)
        : "%rdx", "%rax");
    printf("\nS+1=\t\t%p\nasm=\t\t%p\n", c_addr, asm_addr);

    c_val = S[3];
    asm_val = 0;
    asm volatile(
        "movq %1, %%rdx\n"
        "movw 6(%%rdx), %%ax\n" // S[3]
        "movw %%ax, %0\n"
        : "=r"(asm_val)
        : "r"(S)
        : "%rdx", "%rax");
    printf("\nS[3]=\t\t%hd\nasm=\t\t%hd\n", c_val, asm_val);

    printf("\nS=\t\t%p\n", S);
    for (int i = 0; i < len; i++)
    {
        asm volatile(
            "movq %1, %%rdx\n"
            "movl %2, %%ecx\n"
            "leaq (%%rdx, %%rcx, 2), %%rax\n" // &S[i]
            "movq %%rax, %0\n"
            : "=r"(asm_addr)
            : "r"(S), "r"(i)
            : "%rdx", "%rcx", "%rax");
        printf("S[%d]=%hd\t\t%p\nasm_addr:\t%p\n", i, S[i], &S[i], asm_addr);
    }
    int i = 2;
    c_val = S[4 * i + 1];
    asm_val = 0;
    asm volatile(
        "movq %1, %%rdx\n"
        "movl %2, %%ecx\n"
        "movw 2(%%rdx, %%rcx, 8), %%ax\n" // S[4*i+1]
        "movw %%ax, %0\n"
        : "=r"(asm_val)
        : "r"(S), "r"(i)
        : "%rdx", "%rcx", "%rax");
    printf("\nS[4*i+1]=S[4*%d+1]=\t%hd\nasm=\t\t\t%hd\n", i, c_val, asm_val);

    i = 8;
    c_addr = S + i - 5;
    asm_addr = 0;
    asm volatile(
        "movq %1, %%rdx\n"
        "movl %2, %%ecx\n"
        "leaq -10(%%rdx, %%rcx, 2), %%rax\n" // S+i-5
        "movq %%rax, %0\n"
        : "=r"(asm_addr)
        : "r"(S), "r"(i)
        : "%rdx", "%rcx", "%rax");
    printf("\nS+i-5=S+%d-5=\t\t%p\nasm=\t\t\t%p\n", i, c_addr, asm_addr);

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            mat1[i][j] = i * 16 + j;
        }
    }
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            mat2[i][j] = (i << 4) + j;
        }
    }
    sum_element(1, 2);

    fix_matrix mat3;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            mat3[i][j] = (i << 4) + j;
        }
    }
    var_ele(N, mat3, 1, 2);
}
