#include <stdint.h>
#include <stdio.h>

/*
                         Stack "bottom"
    ^                   +-----+-----+-----+-----+ 4byte/32bit
    |                   |                       |\
Increasing              |...                    | Earlier frames
  address               |_______________________|/
    |                   |...                    |\
                +4+4n	|Argument n             |
                        |...                    | Caller's frame
                +8      |Argument 1             |
                +4      |_ Return address ______|/
Frame pointer: %eb      |Saved %ebp             |\
                -4      +-----+-----+-----+-----+ |
                        |                       | |
                        |Saved registers,       | |
                        |local variables,       | |
                        |and temporaries        | \
                        |                       | Current frame
                        +-----+-----+-----+-----+ /
                        |                       | |
                        |Argument               | |
                        |build area             | |
                        |                       |/
Stack pointer: %esp     +-----+-----+-----+-----+
                         Stack "top"

// Push double word
pushl S; // R[%esp] ← R[%esp] − 4;
         // M[R[%esp]] ← S
// Pop double word
popl D; // D ← M[R[%esp]];
        // R[%esp] ← R[%esp] + 4

// enter // new frame
pushl %ebp       // push down %esp 4 byte
                 // save %ebp point to new %esp(%esp-4)
mov   %esp, %ebp // %ebp point to new %esp(%esp-4)

// leave // like stack unwinding
movl %ebp, %esp // %esp point to %ebp                      // up %esp to %ebp
popl %ebp       // %ebp point to Saved caller's frame %ebp // stack unwinding
                // %esp += 4                               // pop 4 byte

// ret
movl (%esp), %eip
subl $4, %esp

// call and ret
  +-----+------------+    +-----+------------+                   +-----+------------+
  |%eip | 0x080483dc |    |%eip | 0x08048394 | // <sum>          |%eip | 0x080483e1 |
+-|%esp | 0xff9bc960 |  +-|%esp | 0xff9bc95c | // %esp-=4      +-|%esp | 0xff9bc960 |
| +-----+------------+  | +-----+------------+                 | +-----+------------+
|  +------------+       |  +------------+                      |  +------------+
|  |            |       |  |            |                      |  |            |
|  |            |       |  |            |    ...               |  |            |
|  |            |       |  |            |                      |  |            |
+->|            |       |  |____________|                      |  |            |
   +------------+       +->| 0x080483e1 | // Return address:5  +->+------------+
    before call            +------------+                          after ret
                            after call
// Beginning of function sum
1 0x08048394 <sum>:
2 0x08048394: 55				push	%ebp
...
// Return from function sum
3 0x080483a4: c3				ret
...
// Call to sum from main
4 0x080483dc: e8 b3 ff ff ff	call	0x08048394 <sum> // 0x080483e1+0xffffffb3=0x108048394
5 0x080483e1: 83 c4 14			add		$0x14, %esp
  ^ Return address



// 3.30
a:     call next
b: next:
b:     popl %eax
c:

        |before call       |after call |after popl %eax
--------+------------------+-----------+------------------+
  %eip  |a                 |b          |c                 |
M[%esp] |earlier stack top |b          |earlier stack top |
  %eax  |                  |           |b                 |

*/

int swap_add(int *xp, int *yp)
{
    int x = *xp;
    int y = *yp;

    *xp = y;
    *yp = x;
    return x + y;
}

int caller()
{
    int arg1 = 1;
    int arg2 = 3;
    int sum = swap_add(&arg1, &arg2);
    int diff = arg1 - arg2;
    return sum * diff;
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    int result = caller();
    printf("caller return:%d\n", result);
}

/*
waq@walking:~/walking/shared4vm/walking/cc/csapp$ gdb 3.30
GNU gdb (Ubuntu 12.0.90-0ubuntu1) 12.0.90
Copyright (C) 2022 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from 3.30...
(gdb) b 3.30.c:112
Breakpoint 1 at 0x1295: file /home/waq/walking/shared4vm/walking/cc/csapp/3.30.c, line 112.
(gdb) r
Starting program: /home/waq/walking/shared4vm/walking/cc/csapp/3.30 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
Hello, World!
0       >[/home/waq/walking/shared4vm/walking/cc/csapp/3.30]

Breakpoint 1, main (argc=1, argv=0x7fffffffdb58) at /home/waq/walking/shared4vm/walking/cc/csapp/3.30.c:112
warning: Source file is more recent than executable.
112         int result = caller();
(gdb) disass /m swap_add
Dump of assembler code for function swap_add:
87      {
   0x0000555555555189 <+0>:     endbr64 
   0x000055555555518d <+4>:     push   %rbp
   0x000055555555518e <+5>:     mov    %rsp,%rbp
   0x0000555555555191 <+8>:     mov    %rdi,-0x18(%rbp)
   0x0000555555555195 <+12>:    mov    %rsi,-0x20(%rbp)

88          int x = *xp;
   0x0000555555555199 <+16>:    mov    -0x18(%rbp),%rax
   0x000055555555519d <+20>:    mov    (%rax),%eax
   0x000055555555519f <+22>:    mov    %eax,-0x8(%rbp)

89          int y = *yp;
   0x00005555555551a2 <+25>:    mov    -0x20(%rbp),%rax
   0x00005555555551a6 <+29>:    mov    (%rax),%eax
   0x00005555555551a8 <+31>:    mov    %eax,-0x4(%rbp)

90
91          *xp = y;
   0x00005555555551ab <+34>:    mov    -0x18(%rbp),%rax
   0x00005555555551af <+38>:    mov    -0x4(%rbp),%edx
   0x00005555555551b2 <+41>:    mov    %edx,(%rax)

92          *yp = x;
   0x00005555555551b4 <+43>:    mov    -0x20(%rbp),%rax
   0x00005555555551b8 <+47>:    mov    -0x8(%rbp),%edx
   0x00005555555551bb <+50>:    mov    %edx,(%rax)

93          return x + y;
   0x00005555555551bd <+52>:    mov    -0x8(%rbp),%edx
   0x00005555555551c0 <+55>:    mov    -0x4(%rbp),%eax
   0x00005555555551c3 <+58>:    add    %edx,%eax

94      }
   0x00005555555551c5 <+60>:    pop    %rbp
   0x00005555555551c6 <+61>:    ret    

End of assembler dump.
(gdb) disass /m caller
Dump of assembler code for function caller:                                     // $rbp           |$rsp           |x/1xg $rsp         |$rip
97      {                                                                       // 0x7fffffffda40 |0x7fffffffda18 |0x000055555555529f |0x00005555555551c7
   0x00005555555551c7 <+0>:     endbr64                                         // 0x7fffffffda40 |0x7fffffffda18 |0x000055555555529f |0x00005555555551cb
   0x00005555555551cb <+4>:     push   %rbp                                     // 0x7fffffffda40 |0x7fffffffda10 |0x00007fffffffda40 |0x00005555555551cc
   0x00005555555551cc <+5>:     mov    %rsp,%rbp                                // 0x7fffffffda10 |0x7fffffffda10 |0x00007fffffffda40 |0x00005555555551cf
   0x00005555555551cf <+8>:     sub    $0x20,%rsp                               // 0x7fffffffda10 |0x7fffffffd9f0 |0x0000000000000000 |0x00005555555551d3
   0x00005555555551d3 <+12>:    mov    %fs:0x28,%rax                            // $fs = 0x0, $rax=0x0 -> 0xbf0588e5c6440f00
   0x00005555555551dc <+21>:    mov    %rax,-0x8(%rbp)
   0x00005555555551e0 <+25>:    xor    %eax,%eax

98          int arg1 = 1;
   0x00005555555551e2 <+27>:    movl   $0x1,-0x18(%rbp)

99          int arg2 = 3;
   0x00005555555551e9 <+34>:    movl   $0x3,-0x14(%rbp)

100         int sum = swap_add(&arg1, &arg2);
   0x00005555555551f0 <+41>:    lea    -0x14(%rbp),%rdx
   0x00005555555551f4 <+45>:    lea    -0x18(%rbp),%rax
   0x00005555555551f8 <+49>:    mov    %rdx,%rsi
   0x00005555555551fb <+52>:    mov    %rax,%rdi
   0x00005555555551fe <+55>:    call   0x555555555189 <swap_add>
   0x0000555555555203 <+60>:    mov    %eax,-0x10(%rbp)

101         int diff = arg1 - arg2;
   0x0000555555555206 <+63>:    mov    -0x18(%rbp),%eax
   0x0000555555555209 <+66>:    mov    -0x14(%rbp),%edx
   0x000055555555520c <+69>:    sub    %edx,%eax
   0x000055555555520e <+71>:    mov    %eax,-0xc(%rbp)

102         return sum * diff;
   0x0000555555555211 <+74>:    mov    -0x10(%rbp),%eax
   0x0000555555555214 <+77>:    imul   -0xc(%rbp),%eax

103     }
   0x0000555555555218 <+81>:    mov    -0x8(%rbp),%rdx
   0x000055555555521c <+85>:    sub    %fs:0x28,%rdx
   0x0000555555555225 <+94>:    je     0x55555555522c <caller+101>
   0x0000555555555227 <+96>:    call   0x555555555080 <__stack_chk_fail@plt>
   0x000055555555522c <+101>:   leave  
   0x000055555555522d <+102>:   ret    

End of assembler dump.
(gdb) disass /m main
Dump of assembler code for function main:
106     {
   0x000055555555522e <+0>:     endbr64 
   0x0000555555555232 <+4>:     push   %rbp
   0x0000555555555233 <+5>:     mov    %rsp,%rbp
   0x0000555555555236 <+8>:     sub    $0x20,%rsp
   0x000055555555523a <+12>:    mov    %edi,-0x14(%rbp)
   0x000055555555523d <+15>:    mov    %rsi,-0x20(%rbp)

107         printf("Hello, World!\n");
   0x0000555555555241 <+19>:    lea    0xdbc(%rip),%rax        # 0x555555556004
   0x0000555555555248 <+26>:    mov    %rax,%rdi
   0x000055555555524b <+29>:    call   0x555555555070 <puts@plt>

108         for (int i = 0; i < argc; i++)
   0x0000555555555250 <+34>:    movl   $0x0,-0x8(%rbp)
   0x0000555555555257 <+41>:    jmp    0x55555555528d <main+95>
   0x0000555555555289 <+91>:    addl   $0x1,-0x8(%rbp)
   0x000055555555528d <+95>:    mov    -0x8(%rbp),%eax
   0x0000555555555290 <+98>:    cmp    -0x14(%rbp),%eax
   0x0000555555555293 <+101>:   jl     0x555555555259 <main+43>

109         {
110             printf("%d\t>[%s]\n", i, argv[i]);
   0x0000555555555259 <+43>:    mov    -0x8(%rbp),%eax
   0x000055555555525c <+46>:    cltq   
   0x000055555555525e <+48>:    lea    0x0(,%rax,8),%rdx
   0x0000555555555266 <+56>:    mov    -0x20(%rbp),%rax
   0x000055555555526a <+60>:    add    %rdx,%rax
   0x000055555555526d <+63>:    mov    (%rax),%rdx
   0x0000555555555270 <+66>:    mov    -0x8(%rbp),%eax
   0x0000555555555273 <+69>:    mov    %eax,%esi
   0x0000555555555275 <+71>:    lea    0xd96(%rip),%rax        # 0x555555556012
   0x000055555555527c <+78>:    mov    %rax,%rdi
   0x000055555555527f <+81>:    mov    $0x0,%eax
   0x0000555555555284 <+86>:    call   0x555555555090 <printf@plt>

111         }                                                                   // $rbp           |$rsp           |x/1xg $rsp         |$rip
112         int result = caller();                                              // 0x7fffffffda40 |0x7fffffffda20 |0x00007fffffffdb58 |0x0000555555555295
=> 0x0000555555555295 <+103>:   mov    $0x0,%eax                                // 0x7fffffffda40 |0x7fffffffda20 |0x00007fffffffdb58 |0x000055555555529a
   0x000055555555529a <+108>:   call   0x5555555551c7 <caller>                  // 0x7fffffffda40 |0x7fffffffda18 |0x000055555555529f |0x00005555555551c7
   0x000055555555529f <+113>:   mov    %eax,-0x4(%rbp)

113         printf("caller return:%d\n", result);
   0x00005555555552a2 <+116>:   mov    -0x4(%rbp),%eax
   0x00005555555552a5 <+119>:   mov    %eax,%esi
   0x00005555555552a7 <+121>:   lea    0xd6e(%rip),%rax        # 0x55555555601c
   0x00005555555552ae <+128>:   mov    %rax,%rdi
   0x00005555555552b1 <+131>:   mov    $0x0,%eax
   0x00005555555552b6 <+136>:   call   0x555555555090 <printf@plt>
   0x00005555555552bb <+141>:   mov    $0x0,%eax

114     }
   0x00005555555552c0 <+146>:   leave  
   0x00005555555552c1 <+147>:   ret    

End of assembler dump.
(gdb) 

(gdb) i r
r8              0x0
r9              0x7fffffff
r10             0x0
r11             0x246
r12             0x7fffffffdb58
r13             0x55555555522e
r14             0x555555557db0
r15             0x7ffff7ffd040

cs              0x33
ss              0x2b
ds              0x0
es              0x0
fs              0x0
gs              0x0
eflags          0x246                                                                                          0x206                                                    0x246                                                                                                                                                0x202

rax             0x1                0x0                                                                                            0x5760000fffc8d900                    0x0                0x7fffffffd9f8     rax                                                                                                            0x4
rbx             0x0                                                                                                                                                                                           rbx
rcx             0x1                                                                                                                                                                                           rcx
rdx             0x0                                                                                                                                                                        0x7fffffffd9fc     rdx                                                                                                            0x1
rsi             0x5555555592a0                                                                                                                                                             0x7fffffffd9fc     rsi
rdi             0x7fffffffd4c0                                                                                                                                                             0x7fffffffd9f8     rdi
rbp             0x7fffffffda40                                                              0x7fffffffda10                                                                                                    rbp                                                                      0x7fffffffd9e0                                           0x7fffffffda10
rsp             0x7fffffffda20                        0x7fffffffda18     0x7fffffffda10                        0x7fffffffd9f0                                                                                 rsp             0x7fffffffd9e8                        0x7fffffffd9e0                                                                                 0x7fffffffd9f0
rip             0x555555555295     0x55555555529a     0x5555555551c7     0x5555555551cc     0x5555555551cf     0x5555555551d3     0x5555555551dc     0x5555555551e0     0x5555555551e2     0x5555555551fe     rip             0x555555555189     0x55555555518d     0x55555555518e     0x555555555191     0x555555555195     0x5555555551c5     0x5555555551c6     0x555555555203     0x555555555206

before exec:    mov %0x0,%eax     |call <caller>     |endbr64/push %rbp |mov %rsp,%rbp     |sub $0x20,%rsp    |mov %fs:0x28,%rax |mov %rax,         |xor %eax,%eax     |movl $0x1,    |...|call <swap_add>   |before exec:   |endbr64           |push %rbp         |mov %rsp,%rbp     |mov %rdi,         |mov %rsi,     |...|pop %rbp          |ret               |mov %eax,         |mov -0x18(%rbp)   |
               _______________________________________________________________________________________________________________________-0x8(%rbp)_____________________________-0x18(%rbp)_____________________                 _____________________________________________________________-0x18(%rbp)________-0x20(%rbp)_________________________________________________-0x10(%rbp)_____,%eax_________
0x7fffffffda40 |0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x7fffffffda40 |                  |0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |
0x7fffffffda38 |0x0000555500000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x7fffffffda38 |                  |0x0000555500000001|                  |                  |                  |                  |                  |                  |                  |
0x7fffffffda30 |0x0000000000001000|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x7fffffffda30 |                  |0x0000000000001000|                  |                  |                  |                  |                  |                  |                  |
0x7fffffffda28 |0x0000000100000064|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x7fffffffda28 |                  |0x0000000100000064|                  |                  |                  |                  |                  |                  |                  |
0x7fffffffda20 |0x00007fffffffdb58|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x7fffffffda20 |                  |0x00007fffffffdb58|                  |                  |                  |                  |                  |                  |                  |
0x7fffffffda18 +------------------+------------------|0x000055555555529f|_Return address___|__________________|__________________|__________________|__________________|__________________|__________________|0x7fffffffda18 |_ret addr@main____|0x000055555555529f|_ret addr@main____|_ret addr@main____|__________________|__________________|__________________|__________________|__________________|
0x7fffffffda10                                       +------------------|    0x7fffffffda40| %rbp for main    |                  |                  |                  |                  |                  |0x7fffffffda10 | %rbp for main    |    0x7fffffffda40| %rbp for main    | %rbp for main    |                  |                  |                  |                  |                  |
0x7fffffffda08                                                          +------------------+------------------|0x000055555555522e|                  |0x5760000fffc8d900| <random_value>   |                  |0x7fffffffda08 |                  |0x5760000fffc8d900| <random_value>   |                  |                  |                  |                  |                  |                  |
0x7fffffffda00                                                                                                |0x00007fffffffdb58|                  |                  |                  |                  |0x7fffffffda00 |                  |0x00007fffffffdb58|                  |                  |                  |                  |                  |                  |0x00007fff00000004|0x7fffffffda00: 0x04    0x00    0x00    0x00    0xff    0x7f    0x00    0x00
0x7fffffffd9f8                                                                                                |0x00007fffffffda40|                  |                  |                  |0x0000000300000001|0x7fffffffd9f8 |                  |0x0000000300000001|                  |                  |                  |0x0000000100000003|                  |                  |                  |
0x7fffffffd9f0                                                                                                |0x0000000000000000|                  |                  |                  |                  |0x7fffffffd9f0 |                  |0x0000000000000000|                  |                  |                  |                  |__________________|__________________|__________________|
0x7fffffffd9e8                                                                                                +------------------+------------------+------------------+------------------+------------------+0x7fffffffd9e8 |0x0000555555555203|  = 0x555555555203|_ret addr@caller__|_ret addr@caller__|__________________|__________________|                  |                  |                  |
0x7fffffffd9e0                                                                                                                                                                                                0x7fffffffd9e0 +------------------+------------------|    0x7fffffffda10| %rbp for caller  |                  |                  |                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9d8                                       +------------------+0x00007ffff7e0502a+------------------+0x0000000300000001+                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9d0                                                          |0x0000555555556004|                  |                  |                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9c8                                                          |0x00007ffff7f9e780|    0x7fffffffd9f8|    0x7fffffffd9f8|                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9c0                                                          |0x000000000000000d|                  |    0x7fffffffd9fc|                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9b8                                                          |0x00007ffff7e10f43|                  |                  |                  |                  |                  |
                                                                                                                                                                                                              0x7fffffffd9b0                                                          |0x00007ffff7ffd040|                  |                  |                  |                  |                  |

(gdb) disass /m
(gdb) i r
(gdb) x/13xg $rsp
(gdb) x/13xg $rsp-8
(gdb) x/13xg 0x7fffffffd9b0



Dump of assembler code for function main:                             
3.30.c:                                                               
112     {                                                             
   0x000055555555522e <+0>:     endbr64         
   0x0000555555555232 <+4>:     push   %rbp           
   0x0000555555555233 <+5>:     mov    %rsp,%rbp                                                                                            
   0x0000555555555236 <+8>:     sub    $0x20,%rsp                     
   0x000055555555523a <+12>:    mov    %edi,-0x14(%rbp)           
   0x000055555555523d <+15>:    mov    %rsi,-0x20(%rbp)                                                                                     
                                                                      
113         printf("Hello, World!\n");                                
=> 0x0000555555555241 <+19>:    lea    0xdbc(%rip),%rax        # 0x555555556004                                                              
   0x0000555555555248 <+26>:    mov    %rax,%rdi    
   0x000055555555524b <+29>:    call   0x555555555070 <puts@plt>      
                                                                                                                                             
114         for (int i = 0; i < argc; i++)            
   0x0000555555555250 <+34>:    movl   $0x0,-0x8(%rbp)
   0x0000555555555257 <+41>:    jmp    0x55555555528d <main+95>       
                                                                      
115         {                                                         
116             printf("%d\t>[%s]\n", i, argv[i]);                    
   0x0000555555555259 <+43>:    mov    -0x8(%rbp),%eax                
   0x000055555555525c <+46>:    cltq            
   0x000055555555525e <+48>:    lea    0x0(,%rax,8),%rdx                                                                                    
   0x0000555555555266 <+56>:    mov    -0x20(%rbp),%rax
   0x000055555555526a <+60>:    add    %rdx,%rax       
   0x000055555555526d <+63>:    mov    (%rax),%rdx                
   0x0000555555555270 <+66>:    mov    -0x8(%rbp),%eax
   0x0000555555555273 <+69>:    mov    %eax,%esi                      
   0x0000555555555275 <+71>:    lea    0xd96(%rip),%rax        # 0x555555556012                                                              
   0x000055555555527c <+78>:    mov    %rax,%rdi      
   0x000055555555527f <+81>:    mov    $0x0,%eax       
   0x0000555555555284 <+86>:    call   0x555555555090 <printf@plt>    
                                   
114         for (int i = 0; i < argc; i++)                            
   0x0000555555555289 <+91>:    addl   $0x1,-0x8(%rbp)                
   0x000055555555528d <+95>:    mov    -0x8(%rbp),%eax
   0x0000555555555290 <+98>:    cmp    -0x14(%rbp),%eax               
   0x0000555555555293 <+101>:   jl     0x555555555259 <main+43>       
                                                                      
117         }                                                         
118         int result = caller();                                    
   0x0000555555555295 <+103>:   mov    $0x0,%eax  
   0x000055555555529a <+108>:   call   0x5555555551c7 <caller>                                                                              
   0x000055555555529f <+113>:   mov    %eax,-0x4(%rbp)                
                                                                      
119         printf("caller return:%d\n", result);                 
   0x00005555555552a2 <+116>:   mov    -0x4(%rbp),%eax
   0x00005555555552a5 <+119>:   mov    %eax,%esi
   0x00005555555552a7 <+121>:   lea    0xd6e(%rip),%rax        # 0x55555555601c                                                              
   0x00005555555552ae <+128>:   mov    %rax,%rdi      
   0x00005555555552b1 <+131>:   mov    $0x0,%eax      
   0x00005555555552b6 <+136>:   call   0x555555555090 <printf@plt>
   0x00005555555552bb <+141>:   mov    $0x0,%eax
                                                                      
120     }                                                             
   0x00005555555552c0 <+146>:   leave  
   0x00005555555552c1 <+147>:   ret                                   
End of assembler dump.
(gdb) disassemble /s caller                                           
Dump of assembler code for function caller:
3.30.c:                                                               
103     {                                                             
   0x00005555555551c7 <+0>:     endbr64 
   0x00005555555551cb <+4>:     push   %rbp
   0x00005555555551cc <+5>:     mov    %rsp,%rbp
   0x00005555555551cf <+8>:     sub    $0x20,%rsp
   0x00005555555551d3 <+12>:    mov    %fs:0x28,%rax
   0x00005555555551dc <+21>:    mov    %rax,-0x8(%rbp)
   0x00005555555551e0 <+25>:    xor    %eax,%eax

104         int arg1 = 1;                                             
   0x00005555555551e2 <+27>:    movl   $0x1,-0x18(%rbp)

105         int arg2 = 3;                                             
   0x00005555555551e9 <+34>:    movl   $0x3,-0x14(%rbp)

106         int sum = swap_add(&arg1, &arg2);
   0x00005555555551f0 <+41>:    lea    -0x14(%rbp),%rdx
   0x00005555555551f4 <+45>:    lea    -0x18(%rbp),%rax
   0x00005555555551f8 <+49>:    mov    %rdx,%rsi
   0x00005555555551fb <+52>:    mov    %rax,%rdi
   0x00005555555551fe <+55>:    call   0x555555555189 <swap_add>
   0x0000555555555203 <+60>:    mov    %eax,-0x10(%rbp)

107         int diff = arg1 - arg2;                                   
   0x0000555555555206 <+63>:    mov    -0x18(%rbp),%eax
   0x0000555555555209 <+66>:    mov    -0x14(%rbp),%edx
   0x000055555555520c <+69>:    sub    %edx,%eax
   0x000055555555520e <+71>:    mov    %eax,-0xc(%rbp)

108         return sum * diff;                                        
   0x0000555555555211 <+74>:    mov    -0x10(%rbp),%eax
   0x0000555555555214 <+77>:    imul   -0xc(%rbp),%eax

109     }                                                             
   0x0000555555555218 <+81>:    mov    -0x8(%rbp),%rdx
   0x000055555555521c <+85>:    sub    %fs:0x28,%rdx
   0x0000555555555225 <+94>:    je     0x55555555522c <caller+101>
   0x0000555555555227 <+96>:    call   0x555555555080 <__stack_chk_fail@plt>                                                                 
   0x000055555555522c <+101>:   leave  
   0x000055555555522d <+102>:   ret    
End of assembler dump.                                                
(gdb) disassemble /s swap_add                                         
Dump of assembler code for function swap_add:
3.30.c:                                                               
93      {                                                             
   0x0000555555555189 <+0>:     endbr64 
   0x000055555555518d <+4>:     push   %rbp
   0x000055555555518e <+5>:     mov    %rsp,%rbp
   0x0000555555555191 <+8>:     mov    %rdi,-0x18(%rbp)
   0x0000555555555195 <+12>:    mov    %rsi,-0x20(%rbp)

94          int x = *xp;                                              
   0x0000555555555199 <+16>:    mov    -0x18(%rbp),%rax
   0x000055555555519d <+20>:    mov    (%rax),%eax
   0x000055555555519f <+22>:    mov    %eax,-0x8(%rbp)

95          int y = *yp;                                              
   0x00005555555551a2 <+25>:    mov    -0x20(%rbp),%rax
   0x00005555555551a6 <+29>:    mov    (%rax),%eax
   0x00005555555551a8 <+31>:    mov    %eax,-0x4(%rbp)

96                                                                    
97          *xp = y;                                                  
   0x00005555555551ab <+34>:    mov    -0x18(%rbp),%rax
   0x00005555555551af <+38>:    mov    -0x4(%rbp),%edx
   0x00005555555551b2 <+41>:    mov    %edx,(%rax)

98          *yp = x;                                                  
   0x00005555555551b4 <+43>:    mov    -0x20(%rbp),%rax
   0x00005555555551b8 <+47>:    mov    -0x8(%rbp),%edx
   0x00005555555551bb <+50>:    mov    %edx,(%rax)

99          return x + y;                                             
   0x00005555555551bd <+52>:    mov    -0x8(%rbp),%edx
   0x00005555555551c0 <+55>:    mov    -0x4(%rbp),%eax
   0x00005555555551c3 <+58>:    add    %edx,%eax

100     }                                                             
   0x00005555555551c5 <+60>:    pop    %rbp
   0x00005555555551c6 <+61>:    ret    
End of assembler dump.                                                
(gdb)                                                                 
[0] 0:[tmux]*Z 1:sh-                                                                                                                                                                                                                                             "walking" 18:44 24-Feb-23

diffrs add 0x00007fffffffdaa0 --size 28

redirect to:gef_20230224184156.txt!
* reg             :c/csapp/3.30.c@113|c/csapp/3.30.c@118|c/csapp/3.30.c@103|c/csapp/3.30.c@103|c/csapp/3.30.c@103|c/csapp/3.30.c@103|c/csapp/3.30.c@103|c/csapp/3.30.c@103|c/csapp/3.30.c@106|cc/csapp/3.30.c@93|* reg             :cc/csapp/3.30.c@93|cc/csapp/3.30.c@93|cc/csapp/3.30.c@93|c/csapp/3.30.c@100|c/csapp/3.30.c@100|c/csapp/3.30.c@106|c/csapp/3.30.c@107|c/csapp/3.30.c@109|* reg             :c/csapp/3.30.c@109|c/csapp/3.30.c@109|c/csapp/3.30.c@118|c/csapp/3.30.c@119|c/csapp/3.30.c@120|c/csapp/3.30.c@120|art_call_main.h@58|art_call_main.h@74
$rax              :0x000055555555522e|0x0000000000000000|                  |                  |                  |                  |                  |0xb1e553690508b000|0x00007fffffffdac8|                  |$rax              :                  |                  |                  |0x0000000000000004|                  |                  |                  |0x0000000000000008|$rax              :                  |                  |                  |                  |0x0000000000000000|                  |                  |                  |
$rbx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbx              :                  |                  |                  |                  |                  |                  |                  |                  |$rbx              :                  |                  |                  |                  |                  |                  |                  |                  |
$rcx              :0x0000555555557db0|0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :                  |                  |                  |                  |                  |                  |                  |                  |
$rdx              :0x00007fffffffdc38|0x0000000000000000|                  |                  |                  |                  |                  |                  |0x00007fffffffdacc|                  |$rdx              :                  |                  |                  |0x0000000000000001|                  |                  |                  |0xb1e553690508b000|$rdx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
$rsp              :0x00007fffffffdaf0|                  |0x00007fffffffdae8|                  |0x00007fffffffdae0|                  |0x00007fffffffdac0|                  |                  |0x00007fffffffdab8|$rsp              :                  |0x00007fffffffdab0|                  |                  |0x00007fffffffdab8|0x00007fffffffdac0|                  |                  |$rsp              :                  |0x00007fffffffdae8|0x00007fffffffdaf0|                  |                  |0x00007fffffffdb18|0x00007fffffffdb20|                  |
$rbp              :0x00007fffffffdb10|                  |                  |                  |                  |0x00007fffffffdae0|                  |                  |                  |                  |$rbp              :                  |                  |0x00007fffffffdab0|                  |0x00007fffffffdae0|                  |                  |                  |$rbp              :                  |0x00007fffffffdb10|                  |                  |                  |0x0000000000000001|                  |                  |
$rsi              :0x00007fffffffdc28|0x00005555555592a0|                  |                  |                  |                  |                  |                  |0x00007fffffffdacc|                  |$rsi              :                  |                  |                  |                  |                  |                  |                  |                  |$rsi              :                  |                  |                  |                  |0x00005555555592a0|                  |                  |                  |
$rdi              :0x0000000000000001|0x00007fffffffd590|                  |                  |                  |                  |                  |                  |0x00007fffffffdac8|                  |$rdi              :                  |                  |                  |                  |                  |                  |                  |                  |$rdi              :                  |                  |                  |                  |0x00007fffffffd590|                  |                  |0x0000000000000000|
$rip              :0x0000555555555241|0x000055555555529a|0x00005555555551c7|0x00005555555551cb|0x00005555555551cc|0x00005555555551cf|0x00005555555551d3|0x00005555555551e0|0x00005555555551fe|0x0000555555555189|$rip              :0x000055555555518d|0x000055555555518e|0x0000555555555191|0x00005555555551c5|0x00005555555551c6|0x0000555555555203|0x0000555555555206|0x000055555555521c|$rip              :0x000055555555522c|0x000055555555522d|0x000055555555529f|0x00005555555552a2|0x00005555555552c0|0x00005555555552c1|0x00007ffff7dadd90|0x00007ffff7dadd92|
$r8               :0x00007ffff7f9ef10|0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |                  |                  |                  |                  |                  |
$r9               :0x00007ffff7fc9040|0x000000007fffffff|                  |                  |                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |0x00007fffffffd9c7|                  |                  |                  |
$r10              :0x00007ffff7fc3908|0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |
$r11              :0x00007ffff7fde680|0x0000000000000246|                  |                  |                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |
$r12              :0x00007fffffffdc28|                  |                  |                  |                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |
$r13              :0x000055555555522e|                  |                  |                  |                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |
$r14              :0x0000555555557db0|                  |                  |                  |                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |
$eflags           :0x0000000000000206|0x0000000000000246|                  |                  |                  |                  |0x0000000000000206|                  |0x0000000000000246|                  |$eflags           :                  |                  |                  |0x0000000000000202|                  |                  |                  |                  |$eflags           :0x0000000000000246|                  |                  |                  |0x0000000000000206|                  |                  |                  |
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |
* stk                                 call               endbr64                                                                                                                                                                                                                                                                                                                                               leave              ret                                                                                                                                  
0x00007fffffffdaa0:0x0000000000000001|0x0000555555556004|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa0:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa0:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdaa8:0x0000000000000001|0x00007ffff7e0502a|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa8:                  |                  |                  |0x0000000300000001|                  |                  |                  |                  |0x00007fffffffdaa8:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdab0:0x0000555555554040|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdab0:                  |0x00007fffffffdae0+                  +                  +                  |                  |                  |                  |0x00007fffffffdab0:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdab8:0x00007ffff7fe285c|                  |                  |                  |                  |                  |                  |                  |                  |0x0000555555555203+0x00007fffffffdab8:                  +                  |                  |                  |0x0000555555555203+                  |                  |                  |0x00007fffffffdab8:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdac0:0x0000000000000d30|0x0000000000000000|                  |                  |                  |                  |                  +                  +                  +                  |0x00007fffffffdac0:                  |                  |                  |                  |                  |                  +                  +                  +0x00007fffffffdac0:                  +                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdac8:0x00007fffffffdfa9|0x00007fffffffdb10|                  |                  |                  |                  |                  |                  |0x0000000300000001|                  |0x00007fffffffdac8:                  |                  |                  |0x0000000100000003|                  |                  |                  |                  |0x00007fffffffdac8:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdad0:0x00007ffff7fc1000|0x00007fffffffdc28|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdad0:                  |                  |                  |                  |                  |                  |0x00007fff00000004|0x0000000200000004|0x00007fffffffdad0:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdad8:0x0000010101000000|0x000055555555522e|                  |                  |                  |                  |                  |0xb1e553690508b000|                  |                  |0x00007fffffffdad8:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdad8:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdae0:0x0000000000000002|0x0000555555557db0|                  |                  |0x00007fffffffdb10+ Earlier frames   + $rbp of call     =                  =                  =                  =0x00007fffffffdae0:                  =                  =                  |                  |                  =                  =                  =                  =0x00007fffffffdae0:0x00007fffffffdb10=                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdae8:0x00000000178bfbff|0x0000555555555289|0x000055555555529f+ Return address   + next $rip of call|                  |                  |                  |                  |                  |0x00007fffffffdae8:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdae8:0x000055555555529f|                  +                  |                  |0x00005555555552bb|                  |                  |                  |
0x00007fffffffdaf0:0x00007fffffffdc28+                  +                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf0:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf0:                  |                  |                  +                  +                  +                  |                  |                  |
0x00007fffffffdaf8:0x0000000100000064|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf8:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf8:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb00:0x0000000000001000|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb00:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb00:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb08:0x00005555555550a0|0x0000555500000001|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb08:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb08:                  |                  |                  |0x0000000800000001|                  |                  |                  |                  |
0x00007fffffffdb10:0x0000000000000001=                  =                  =                  =                  =                  |                  |                  |                  |                  |0x00007fffffffdb10:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb10:                  |                  =                  =                  =                  =                  |                  |                  |
0x00007fffffffdb18:0x00007ffff7dadd90|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb18:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb18:                  |                  |                  |                  |                  |                  +                  |                  |
0x00007fffffffdb20:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb20:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb20:                  |                  |                  |                  |                  |                  |                  +                  +
0x00007fffffffdb28:0x000055555555522e|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb28:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb28:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb30:0x00000001ffffdc10|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb30:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb30:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb38:0x00007fffffffdc28|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb38:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb38:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb40:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb40:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb40:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb48:0xe5e5dbc3ce269303|0x1fc0f33320e4379d|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb48:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb48:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb50:0x00007fffffffdc28|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb50:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb50:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb58:0x000055555555522e|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb58:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb58:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb60:0x0000555555557db0|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb60:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb60:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb68:0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb68:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb68:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb70:0x1a1a243c78649303|0xe03f0ccc96a6379d|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb70:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb70:                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb78:0x1a1a347674ac9303|0xe03f1c869a6e379d|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb78:                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb78:                  |                  |                  |                  |                  |                  |                  |                  |



*/
