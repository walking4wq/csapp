#include "csapp.h"

int main(int argc, char **argv)
{
    struct in_addr inaddr; /* Address in network byte order */
    uint16_t addr;
    /* Address in host byte order */
    char buf[MAXBUF];
    /* Buffer for dotted-decimal string */

    if (argc != 3)
    {
        fprintf(stderr, "usage: %s <n <hex number>|p <network byte order>>\n", argv[0]);
        exit(0);
    }

    if (argv[1][0] == 'n')
    {
        sscanf(argv[2], "%hx", &addr);
        inaddr.s_addr = htons(addr);

        if (!inet_ntop(AF_INET, &inaddr, buf, MAXBUF))
            unix_error("inet_ntop");
        printf("%s\n", buf);
    }
    else
    {
        int rc;
        rc = inet_pton(AF_INET, argv[2], &inaddr);
        if (rc == 0)
            app_error("inet_pton error: invalid network byte order");
        else if (rc < 0)
            unix_error("inet_pton error");

        printf("0x%x\n", ntohl(inaddr.s_addr));
    }

    exit(0);
}
