#include <stdio.h>
#include <stdint.h>

#include <time.h>
// #include <unistd.h>

void swap(long *xp, long *yp)
{
    *xp = *xp + *yp; /* x+y */
    *yp = *xp - *yp; /* x+y - y = x */
    *xp = *xp - *yp; /* x+y - x = y*/
}

/*
# 5.2

import matplotlib.pyplot as plt
import numpy as np

n = np.arange(1, 10, 1)

v1 = 60+35*n
v2 = 136+4*n
v3 = 157+1.25*n

plt.plot(n, v1, color='r', label='v1')
plt.plot(n, v2, color='g', label='v2')
plt.plot(n, v3, color='b', label='v3')

plt.xlabel("element")
plt.ylabel("cycle")
plt.title("CPE")

plt.legend()
plt.show()

*/
// #define DEF_TMP_VAR4GET_CPU_CYCLE uint64_t reth1, retl0;
#define GET_CPU_CYCLE(reth1, retl0, tsc)              \
    __asm__ __volatile__("rdtsc"                      \
                         : "=d"(reth1), "=a"(retl0)); \
    tsc = ((reth1 << 32) | (retl0))

#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

/* Compute prefix sum of vector a */
void psum1(float a[], float p[], long n)
{
    long i;
    p[0] = a[0];
    for (i = 1; i < n; i++)
        p[i] = p[i - 1] + a[i];
}
void psum2(float a[], float p[], long n)
{
    long i;
    p[0] = a[0];
    for (i = 1; i < n - 1; i += 2)
    {
        float mid_val = p[i - 1] + a[i];
        p[i] = mid_val;
        p[i + 1] = mid_val + a[i + 1];
    }
    /* For even n, finish remaining element */
    if (i < n)
        p[i] = p[i - 1] + a[i];
}

void test_psum(long n, uint64_t *tsc1, uint64_t *tsc2)
{
    if (n < 1)
    {
        n = 1;
    }
    float a[n], p[n];
    for (long i = 0; i < n; i++)
    {
        a[i] = i * 0.001 * i;
    }

    // warmed up
    psum1(a, p, n);
    psum1(a, p, n);
    psum1(a, p, n);
    psum2(a, p, n);
    psum2(a, p, n);
    psum2(a, p, n);
    uint64_t tsc4b, tsc4e;

    tsc4b = perf_counter();
    psum1(a, p, n);
    psum1(a, p, n);
    psum1(a, p, n);
    tsc4e = perf_counter();
    *tsc1 = tsc4e - tsc4b;

    tsc4b = perf_counter();
    psum2(a, p, n);
    psum2(a, p, n);
    psum2(a, p, n);
    tsc4e = perf_counter();
    *tsc2 = tsc4e - tsc4b;
}
int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }

    clock_t begin, end;
    double cost;

    long a = 999;
    printf("before swap(&a, &a) a=%ld\n", a);

    begin = clock();

    swap(&a, &a);

    end = clock();
    end -= begin;
    cost = (double)end / CLOCKS_PER_SEC;
    printf("constant CLOCKS_PER_SEC is:%ld, clock is:%ld, time cost is:%lf sec\n", CLOCKS_PER_SEC, end, cost);

    printf("after swap(&a, &a) a=%ld\n", a);

    a = 999;
    long b = 0;
    printf("before swap(&a, &b) a=%ld, b=%ld\n", a, b);

    begin = clock();

    swap(&a, &b);

    end = clock();
    end -= begin;
    cost = (double)end / CLOCKS_PER_SEC;
    printf("constant CLOCKS_PER_SEC is:%ld, clock is:%ld, time cost is:%lf sec\n", CLOCKS_PER_SEC, end, cost);

    printf("after swap(&a, &b) a=%ld, b=%ld\n", a, b);

    // https://www.cnblogs.com/tzj-kernel/p/16196166.html
    uint64_t tsc0;
    uint64_t tsc1, tsc2;

#ifndef GET_CPU_CYCLE_TMP_VAR
#define GET_CPU_CYCLE_TMP_VAR
    uint64_t reth1;
    uint64_t retl0;
#endif

    uint64_t get_cpu_cycle()
    {
        __asm__ __volatile__("rdtsc"
                             : "=d"(reth1), "=a"(retl0));
        return ((reth1 << 32) | (retl0));
    }

    tsc0 = get_cpu_cycle();
    // unsigned long long number = 100000;
    // while (number--)
    // {
    __asm__ __volatile__("nop");
    // }
    tsc1 = get_cpu_cycle();
    GET_CPU_CYCLE(reth1, retl0, tsc2);

    // printf("%llx-%llx\n", reth1, retl0);
    // printf("%llu-%llu=%llu\n", tsc0, tsc1, tsc1 - tsc0); // -Wformat=0
    printf("%lu-%lu=%lu, %lu-%lu=%lu\n", tsc0, tsc1, tsc1 - tsc0, tsc2, tsc1, tsc2 - tsc1);
    printf("sizeof(unsigned long long)=%ld, sizeof(uint64_t)=%ld\n", sizeof(unsigned long long), sizeof(uint64_t));

    tsc0 = perf_counter();
    __asm__ __volatile__("nop");
    tsc1 = perf_counter();
    printf("%lu-%lu=%lu\n", tsc0, tsc1, tsc1 - tsc0);

    const int times = 2000;
    uint64_t tsc4psum1[times];
    uint64_t tsc4psum2[times];
    for (int i = 1; i < times + 1; i++)
    {
        test_psum(i, &tsc1, &tsc2);
        // printf("test_psum(%d,%lu,%lu)\n", i, tsc1, tsc2);
        tsc4psum1[i - 1] = tsc1;
        tsc4psum2[i - 1] = tsc2;
    }
    printf("n = np.arange(1, %d, 1)\n", times + 1);
    printf("psum1 = [");
    for (int i = 0; i < times; i++)
    {
        if (i != 0)
            printf(",");
        printf("%ld", tsc4psum1[i]);
    }
    printf("]\n");
    printf("psum2 = [");
    for (int i = 0; i < times; i++)
    {
        if (i != 0)
            printf(",");
        printf("%ld", tsc4psum2[i]);
    }
    printf("]\n");

    return 0;
}

/*
# Figure 5.2

import matplotlib.pyplot as plt
import numpy as np

n = np.arange(1, 2001, 1)
psum1 = []
psum2 = []



plt.plot(n, psum1, 'r--', n, psum2, 'bs')

# plt.plot(n, psum1, color='r', label='psum1')
# plt.plot(n, psum2, color='g', label='psum2')

plt.xlabel("elements")
plt.ylabel("cycles")
plt.title("Performance of prefix-sum functions")

plt.legend()
plt.show()

*/
