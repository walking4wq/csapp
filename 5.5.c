#include <stdio.h>
#include <stdint.h>

#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

double poly(double a[], double x, long degree)
{
    long i;
    double result = a[0];
    double xpwr = x; /* Equals x^i at start of loop */
    for (i = 1; i <= degree; i++)
    {
        result += a[i] * xpwr;
        xpwr = x * xpwr;
    }
    return result;
}

/* Apply Horner’s method */
double polyh(double a[], double x, long degree)
{
    long i;
    double result = a[degree];
    for (i = degree - 1; i >= 0; i--)
        result = a[i] + x * result;
    return result;
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }

    const int LEN = 64, TIMES = 1024;

    double arr[LEN];
    const double PI = 3.1415926;
    double rst;

    for (int i = 0; i < LEN; i++)
    {
        arr[i] = i * PI;
    }

    rst = 0;
    for (int i = 0; i < TIMES; i++)
    {
        rst += poly(arr, PI, LEN);
        rst += polyh(arr, PI, LEN);
    }

    uint64_t tsc0, tsc1;

    rst = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        rst += poly(arr, PI + i, LEN);
    }
    tsc1 = perf_counter();
    printf("poly used:%lu, CPE:%f, rst:%f\n", (tsc1 - tsc0), (double)(tsc1 - tsc0) / TIMES / LEN, rst);

    rst = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
    {
        rst += polyh(arr, PI + i, LEN);
    }
    tsc1 = perf_counter();
    printf("polyh used:%lu, CPE:%f, rst:%f\n", (tsc1 - tsc0), (double)(tsc1 - tsc0) / TIMES / LEN, rst);

    return 0;
}
/*
waq@walking:~/walking/shared4vm/walking/cc/csapp$ gcc -g -O1 5.5.c -o 5.5
waq@walking:~/walking/shared4vm/walking/cc/csapp$ ./5.5
Hello, World!
0,>[./5.5]
poly used:396768, CPE:6.054199, rst:16652178690060499985421426799069182608246460170433485898221219496835538358076790758124937463044063932456285959488930868415247532218577467575916963869675318391556836751424685240569359823282372608.000000
polyh used:586816, CPE:8.954102, rst:16652178690060497959290777931396839584609807977483705130425207659708145782072235245110869390335008073224328465785906313631123210294363999609534602828769406337418734156715853667726682381139050496.000000

waq@walking:~/walking/shared4vm/walking/cc/csapp$ gcc -g -O2 5.5.c -o 5.5
waq@walking:~/walking/shared4vm/walking/cc/csapp$ ./5.5
Hello, World!
0,>[./5.5]
poly used:267168, CPE:4.076660, rst:16652178690060499985421426799069182608246460170433485898221219496835538358076790758124937463044063932456285959488930868415247532218577467575916963869675318391556836751424685240569359823282372608.000000
polyh used:444128, CPE:6.776855, rst:16652178690060497959290777931396839584609807977483705130425207659708145782072235245110869390335008073224328465785906313631123210294363999609534602828769406337418734156715853667726682381139050496.000000


0000000000001189 <poly>:
    1189:       f3 0f 1e fa             endbr64                             # %xmm0=x, %rdi=a, %rsi=degree
    118d:       66 0f 28 d8             movapd %xmm0,%xmm3                  # %xmm3=x
    1191:       f2 0f 10 07             movsd  (%rdi),%xmm0                 # %xmm0=a[0]
    1195:       48 85 f6                test   %rsi,%rsi                    # if %rsi|degree == 0 then ret
    1198:       7e 26                   jle    11c0 <poly+0x37>
    119a:       48 8d 47 08             lea    0x8(%rdi),%rax               # %rax=a
    119e:       48 8d 54 f7 08          lea    0x8(%rdi,%rsi,8),%rdx        # %rdx=a[degree]
    11a3:       66 0f 28 cb             movapd %xmm3,%xmm1                  # xpwr|%xmm1=x|%xmm3
                                                                            #  loop:
    11a7:       66 0f 28 d1             movapd %xmm1,%xmm2                  # %xmm2 = xpwr|%xmm1
    11ab:       f2 0f 59 10             mulsd  (%rax),%xmm2                 # %xmm2 = a[i]*xpwr|%xmm1
    11af:       f2 0f 58 c2             addsd  %xmm2,%xmm0                  # result|%xmm0 += %xmm2(a[i]*xpwr)
    11b3:       f2 0f 59 cb             mulsd  %xmm3,%xmm1                  # xpwr|%xmm1 *= x|%xmm3
    11b7:       48 83 c0 08             add    $0x8,%rax
    11bb:       48 39 d0                cmp    %rdx,%rax
    11be:       75 e7                   jne    11a7 <poly+0x1e>
    11c0:       c3                      ret

00000000000011c1 <polyh>:
    11c1:       f3 0f 1e fa             endbr64                             # %xmm0=x, %rdi=a, %rsi=degree
    11c5:       66 0f 28 c8             movapd %xmm0,%xmm1                  # %xmm1=x
    11c9:       f2 0f 10 04 f7          movsd  (%rdi,%rsi,8),%xmm0          # result|%xmm0 = a[degree]
    11ce:       48 83 ee 01             sub    $0x1,%rsi                    # i|%rsi--
    11d2:       78 13                   js     11e7 <polyh+0x26>
                                                                            #  loop:
    11d4:       f2 0f 59 c1             mulsd  %xmm1,%xmm0                  # result|%xmm0 *= x|%xmm1
    11d8:       f2 0f 58 04 f7          addsd  (%rdi,%rsi,8),%xmm0          # result|%xmm0 += a[i]
    11dd:       48 83 ee 01             sub    $0x1,%rsi
    11e1:       48 83 fe ff             cmp    $0xffffffffffffffff,%rsi
    11e5:       75 ed                   jne    11d4 <polyh+0x13>
    11e7:       c3                      ret

00000000000011e8 <main>:
    11e8:       f3 0f 1e fa             endbr64
    11ec:       55                      push   %rbp
    11ed:       48 89 e5                mov    %rsp,%rbp
    11f0:       41 56                   push   %r14
    11f2:       41 55                   push   %r13
    11f4:       41 54                   push   %r12
    11f6:       53                      push   %rbx
    11f7:       48 83 ec 20             sub    $0x20,%rsp
    11fb:       41 89 fc                mov    %edi,%r12d
    11fe:       49 89 f5                mov    %rsi,%r13
    1201:       64 48 8b 04 25 28 00    mov    %fs:0x28,%rax
    1208:       00 00
    120a:       48 89 45 d8             mov    %rax,-0x28(%rbp)
    120e:       31 c0                   xor    %eax,%eax
    1210:       48 8d 3d ed 0d 00 00    lea    0xded(%rip),%rdi        # 2004 <_IO_stdin_used+0x4>
    1217:       e8 54 fe ff ff          call   1070 <puts@plt>
    121c:       45 85 e4                test   %r12d,%r12d
    121f:       7e 31                   jle    1252 <main+0x6a>
    1221:       45 89 e4                mov    %r12d,%r12d
    1224:       bb 00 00 00 00          mov    $0x0,%ebx
    1229:       4c 8d 35 e2 0d 00 00    lea    0xde2(%rip),%r14        # 2012 <_IO_stdin_used+0x12>
    1230:       49 8b 4c dd 00          mov    0x0(%r13,%rbx,8),%rcx
    1235:       89 da                   mov    %ebx,%edx
    1237:       4c 89 f6                mov    %r14,%rsi
    123a:       bf 01 00 00 00          mov    $0x1,%edi
    123f:       b8 00 00 00 00          mov    $0x0,%eax
    1244:       e8 47 fe ff ff          call   1090 <__printf_chk@plt>
    1249:       48 83 c3 01             add    $0x1,%rbx
    124d:       49 39 dc                cmp    %rbx,%r12
    1250:       75 de                   jne    1230 <main+0x48>
    1252:       48 89 e0                mov    %rsp,%rax
    1255:       48 39 c4                cmp    %rax,%rsp
    1258:       74 12                   je     126c <main+0x84>
    125a:       48 81 ec 00 10 00 00    sub    $0x1000,%rsp
    1261:       48 83 8c 24 f8 0f 00    orq    $0x0,0xff8(%rsp)
    1268:       00 00
    126a:       eb e9                   jmp    1255 <main+0x6d>
    126c:       48 81 ec 00 02 00 00    sub    $0x200,%rsp
    1273:       48 83 8c 24 f8 01 00    orq    $0x0,0x1f8(%rsp)
    127a:       00 00
    127c:       49 89 e4                mov    %rsp,%r12
    127f:       b8 00 00 00 00          mov    $0x0,%eax
    1284:       f2 0f 10 0d d4 0d 00    movsd  0xdd4(%rip),%xmm1        # 2060 <_IO_stdin_used+0x60>
    128b:       00
    128c:       66 0f ef c0             pxor   %xmm0,%xmm0
    1290:       f2 0f 2a c0             cvtsi2sd %eax,%xmm0
    1294:       f2 0f 59 c1             mulsd  %xmm1,%xmm0
    1298:       f2 41 0f 11 04 c4       movsd  %xmm0,(%r12,%rax,8)
    129e:       48 83 c0 01             add    $0x1,%rax
    12a2:       48 83 f8 40             cmp    $0x40,%rax
    12a6:       75 e4                   jne    128c <main+0xa4>
    12a8:       b8 00 04 00 00          mov    $0x400,%eax
    12ad:       83 e8 01                sub    $0x1,%eax
    12b0:       75 fb                   jne    12ad <main+0xc5>
    12b2:       0f 31                   rdtsc
    12b4:       48 c1 e2 20             shl    $0x20,%rdx
    12b8:       48 09 d0                or     %rdx,%rax
    12bb:       49 89 c5                mov    %rax,%r13
    12be:       bb 00 00 00 00          mov    $0x0,%ebx
    12c3:       66 0f ef e4             pxor   %xmm4,%xmm4
    12c7:       f2 0f 11 65 c8          movsd  %xmm4,-0x38(%rbp)
    12cc:       66 0f ef c0             pxor   %xmm0,%xmm0
    12d0:       f2 0f 2a c3             cvtsi2sd %ebx,%xmm0
    12d4:       f2 0f 58 05 84 0d 00    addsd  0xd84(%rip),%xmm0        # 2060 <_IO_stdin_used+0x60>
    12db:       00
    12dc:       be 40 00 00 00          mov    $0x40,%esi
    12e1:       4c 89 e7                mov    %r12,%rdi
    12e4:       e8 a0 fe ff ff          call   1189 <poly>
    12e9:       f2 0f 58 45 c8          addsd  -0x38(%rbp),%xmm0
    12ee:       f2 0f 11 45 c8          movsd  %xmm0,-0x38(%rbp)
    12f3:       83 c3 01                add    $0x1,%ebx
    12f6:       81 fb 00 04 00 00       cmp    $0x400,%ebx
    12fc:       75 ce                   jne    12cc <main+0xe4>
    12fe:       0f 31                   rdtsc
    1300:       48 c1 e2 20             shl    $0x20,%rdx
    1304:       48 09 d0                or     %rdx,%rax
    1307:       4c 29 e8                sub    %r13,%rax
    130a:       48 89 c2                mov    %rax,%rdx
    130d:       0f 88 e6 00 00 00       js     13f9 <main+0x211>
    1313:       66 0f ef c0             pxor   %xmm0,%xmm0
    1317:       f2 48 0f 2a c0          cvtsi2sd %rax,%xmm0
    131c:       f2 0f 59 05 44 0d 00    mulsd  0xd44(%rip),%xmm0        # 2068 <_IO_stdin_used+0x68>
    1323:       00
    1324:       f2 0f 59 05 44 0d 00    mulsd  0xd44(%rip),%xmm0        # 2070 <_IO_stdin_used+0x70>
    132b:       00
    132c:       f2 0f 10 4d c8          movsd  -0x38(%rbp),%xmm1
    1331:       48 8d 35 e8 0c 00 00    lea    0xce8(%rip),%rsi        # 2020 <_IO_stdin_used+0x20>
    1338:       bf 01 00 00 00          mov    $0x1,%edi
    133d:       b8 02 00 00 00          mov    $0x2,%eax
    1342:       e8 49 fd ff ff          call   1090 <__printf_chk@plt>
    1347:       0f 31                   rdtsc
    1349:       48 c1 e2 20             shl    $0x20,%rdx
    134d:       48 09 d0                or     %rdx,%rax
    1350:       49 89 c5                mov    %rax,%r13
    1353:       bb 00 00 00 00          mov    $0x0,%ebx
    1358:       66 0f ef ed             pxor   %xmm5,%xmm5
    135c:       f2 0f 11 6d c8          movsd  %xmm5,-0x38(%rbp)
    1361:       66 0f ef c0             pxor   %xmm0,%xmm0
    1365:       f2 0f 2a c3             cvtsi2sd %ebx,%xmm0
    1369:       f2 0f 58 05 ef 0c 00    addsd  0xcef(%rip),%xmm0        # 2060 <_IO_stdin_used+0x60>
    1370:       00
    1371:       be 40 00 00 00          mov    $0x40,%esi
    1376:       4c 89 e7                mov    %r12,%rdi
    1379:       e8 43 fe ff ff          call   11c1 <polyh>
    137e:       f2 0f 58 45 c8          addsd  -0x38(%rbp),%xmm0
    1383:       f2 0f 11 45 c8          movsd  %xmm0,-0x38(%rbp)
    1388:       83 c3 01                add    $0x1,%ebx
    138b:       81 fb 00 04 00 00       cmp    $0x400,%ebx
    1391:       75 ce                   jne    1361 <main+0x179>
    1393:       0f 31                   rdtsc
    1395:       48 c1 e2 20             shl    $0x20,%rdx
    1399:       48 09 d0                or     %rdx,%rax
    139c:       4c 29 e8                sub    %r13,%rax
    139f:       48 89 c2                mov    %rax,%rdx
    13a2:       78 73                   js     1417 <main+0x22f>
    13a4:       66 0f ef c0             pxor   %xmm0,%xmm0
    13a8:       f2 48 0f 2a c0          cvtsi2sd %rax,%xmm0
    13ad:       f2 0f 59 05 b3 0c 00    mulsd  0xcb3(%rip),%xmm0        # 2068 <_IO_stdin_used+0x68>
    13b4:       00
    13b5:       f2 0f 59 05 b3 0c 00    mulsd  0xcb3(%rip),%xmm0        # 2070 <_IO_stdin_used+0x70>
    13bc:       00
    13bd:       f2 0f 10 4d c8          movsd  -0x38(%rbp),%xmm1
    13c2:       48 8d 35 77 0c 00 00    lea    0xc77(%rip),%rsi        # 2040 <_IO_stdin_used+0x40>
    13c9:       bf 01 00 00 00          mov    $0x1,%edi
    13ce:       b8 02 00 00 00          mov    $0x2,%eax
    13d3:       e8 b8 fc ff ff          call   1090 <__printf_chk@plt>
    13d8:       48 8b 45 d8             mov    -0x28(%rbp),%rax
    13dc:       64 48 2b 04 25 28 00    sub    %fs:0x28,%rax
    13e3:       00 00
    13e5:       75 4e                   jne    1435 <main+0x24d>
    13e7:       b8 00 00 00 00          mov    $0x0,%eax
    13ec:       48 8d 65 e0             lea    -0x20(%rbp),%rsp
    13f0:       5b                      pop    %rbx
    13f1:       41 5c                   pop    %r12
    13f3:       41 5d                   pop    %r13
    13f5:       41 5e                   pop    %r14
    13f7:       5d                      pop    %rbp
    13f8:       c3                      ret
    13f9:       48 d1 e8                shr    %rax
    13fc:       48 89 d1                mov    %rdx,%rcx
    13ff:       83 e1 01                and    $0x1,%ecx
    1402:       48 09 c8                or     %rcx,%rax
    1405:       66 0f ef c0             pxor   %xmm0,%xmm0
    1409:       f2 48 0f 2a c0          cvtsi2sd %rax,%xmm0
    140e:       f2 0f 58 c0             addsd  %xmm0,%xmm0
    1412:       e9 05 ff ff ff          jmp    131c <main+0x134>
    1417:       48 d1 e8                shr    %rax
    141a:       48 89 d1                mov    %rdx,%rcx
    141d:       83 e1 01                and    $0x1,%ecx
    1420:       48 09 c8                or     %rcx,%rax
    1423:       66 0f ef c0             pxor   %xmm0,%xmm0
    1427:       f2 48 0f 2a c0          cvtsi2sd %rax,%xmm0
    142c:       f2 0f 58 c0             addsd  %xmm0,%xmm0
    1430:       e9 78 ff ff ff          jmp    13ad <main+0x1c5>
    1435:       e8 46 fc ff ff          call   1080 <__stack_chk_fail@plt>


*/
