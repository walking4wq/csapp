#include <stdio.h> // printf
#include <sys/wait.h>
#include <stdlib.h> // EXIT_FAILURE
#include <unistd.h> // fork

#define MAXLINE 2 /* Max text line length */
// void command()
// {
//     char buf[MAXLINE];
//     if (!fgets(buf, MAXLINE, stdin))
//         exit(0);          // EOF
//     printf("<%s\n", buf); // Process the input command
// }

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    int status;
    pid_t pid;

    printf("Start\n");
    pid = fork();
    if (pid == -1)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    printf("%d\n", !pid);
    if (pid == 0)
    {
        printf("Child\n");
        if (argc > 1)
        {
            sleep(30);
        }
    }
    else if (argc <= 1)
    {
        char input4usr;
        printf("before waitpid, please see the `ps l|grep 8.4` STAT is Z\npress enter key to continue...");
        // while (1)
        // {
        //     input4usr = fgetc(stdin);
        //     printf(">%c\n", input4usr);
        // }
        while (fgetc(stdin) != '\n')
            ;
        // command();
        if ((waitpid(-1, &status, 0) > 0) &&
            (WIFEXITED(status) != 0))
        {
            printf("waitpid WEXITSTATUS:%d\n", WEXITSTATUS(status));
        }
        printf("after waitpid, please see the `ps l|grep 8.4` STAT is Z\npress enter key to continue...");
        while (fgetc(stdin) != '\n')
            ;
        // command();
    }
    else
    {
        printf("parent death first\n");
        printf("hold child to waiting for the parent death\npress enter key to stop parent...");
        while (fgetc(stdin) != '\n')
            ;
    }
    printf("Stop\n");
    exit(2);
}
