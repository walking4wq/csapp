#include <stdio.h>
#include <stdint.h>

#include <stdlib.h> // malloc ...

// https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
#define xstr(s) str(s)
#define str(s) #s

#ifndef data_t
#define data_t int64_t
#endif

/* Create abstract data type for vector */
typedef struct
{
    long len;
    data_t *data;
} vec_rec, *vec_ptr;

/* Create vector of specified length */
vec_ptr new_vec(long len)
{
    /* Allocate header structure */
    vec_ptr result = (vec_ptr)malloc(sizeof(vec_rec));
    data_t *data = NULL;
    if (!result)
        return NULL; /* Couldn’t allocate storage */
    result->len = len;
    /* Allocate array */
    if (len > 0)
    {
        data = (data_t *)calloc(len, sizeof(data_t));
        if (!data)
        {
            free((void *)result);
            return NULL; /* Couldn’t allocate storage */
        }
    }
    /* Data will either be NULL or allocated array */
    result->data = data;
    return result;
}

/*
 * Retrieve vector element and store at dest.
 * Return 0 (out of bounds) or 1 (successful)
 */
int get_vec_element(vec_ptr v, long index, data_t *dest)
{
    if (index < 0 || index >= v->len)
        return 0;
    *dest = v->data[index];
    return 1;
}

/* Return length of vector */
long vec_length(vec_ptr v)
{
    return v->len;
}

data_t *get_vec_start(vec_ptr v)
{
    return v->data;
}

#if IDENT
#define OP *
#else
#define IDENT 0
#define OP +
#endif

#ifndef TIMES
#define TIMES 2000
#endif

#ifndef LEN
#define LEN 16
#endif

#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

// LIKE 5.4.c //////////////////////////////////////////////////////////////////////////////////////////////////////////

/* Make sure dest updated on each iteration */
void combine1(vec_ptr v, data_t *dest)
{
    long i;
    long length = vec_length(v);
    data_t *data = get_vec_start(v);
    data_t acc = IDENT;
    /* Initialize in event length <= 0 */
    *dest = acc;
    for (i = 0; i < length; i++)
    {
        acc = acc OP data[i];
        *dest = acc;
    }
}
/* 2 x 1 loop unrolling */
void combine2(vec_ptr v, data_t *dest)
{
    long i;
    long length = vec_length(v);
    long limit = length - 1;
    data_t *data = get_vec_start(v);
    data_t acc = IDENT;

    /* Combine 5 elements at a time */
    for (i = 0; i < limit; i += 2)
    {
        acc = (acc OP data[i]) OP data[i + 1];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc = acc OP data[i];
    }
    *dest = acc;
}
/* unroll5 */
void combine5(vec_ptr v, data_t *dest)
{
    long i;
    long length = vec_length(v);
    long limit = length - 4;
    data_t *data = get_vec_start(v);
    data_t acc = IDENT;

    /* Combine 5 elements at a time */
    for (i = 0; i < limit; i += 5)
    {
        acc = acc OP data[i] OP data[i + 1];
        acc = acc OP data[i + 2] OP data[i + 3];
        acc = acc OP data[i + 4];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc = acc OP data[i];
    }
    *dest = acc;
}

/* 2 x 2 loop unrolling */
void combine6(vec_ptr v, data_t *dest)
{
    long i;
    long length = vec_length(v);
    long limit = length - 1;
    data_t *data = get_vec_start(v);
    data_t acc0 = IDENT;
    data_t acc1 = IDENT;

    /* Combine 2 elements at a time */
    for (i = 0; i < limit; i += 2)
    {
        acc0 = acc0 OP data[i];
        acc1 = acc1 OP data[i + 1];
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc0 = acc0 OP data[i];
    }
    *dest = acc0 OP acc1;
}

/* 2 x 1a loop unrolling */
void combine7(vec_ptr v, data_t *dest)
{
    long i;
    long length = vec_length(v);
    long limit = length - 1;
    data_t *data = get_vec_start(v);
    data_t acc = IDENT;

    /* Combine 2 elements at a time */
    for (i = 0; i < limit; i += 2)
    {
        acc = acc OP(data[i] OP data[i + 1]);
    }

    /* Finish any remaining elements */
    for (; i < length; i++)
    {
        acc = acc OP data[i];
    }
    *dest = acc;
}
/*
CFLAGS="-g -O2"
gcc $CFLAGS 5.7.c -o 5.7
./5.7
gcc $CFLAGS 5.7.c -o 5.7 -DIDENT=1
./5.7
gcc $CFLAGS 5.7.c -o 5.7 -Ddata_t=double
./5.7
gcc $CFLAGS 5.7.c -o 5.7 -Ddata_t=double -DIDENT=1
./5.7
*/
int main(int argc, char *argv[])
{
    // printf("Hello, World!\n");
    // for (int i = 0; i < argc; i++)
    // {
    //     printf("%d,>[%s]\n", i, argv[i]);
    // }

    printf("data_t:" xstr(data_t) ",\tOP:" xstr(OP) ",\tIDENT:" xstr(IDENT) ",\tTIMES:" xstr(TIMES) ",\tLEN:" xstr(LEN));

    vec_ptr vp = new_vec(LEN);

    long length = vec_length(vp);
    data_t *data = get_vec_start(vp);
    for (int i = 0; i < length; i++)
    {
        data[i] = i + 1;
    }

    data_t dest = 0;
    const int test_cases = 5;
    for (int i = 0; i < TIMES; i++)
    {
        combine1(vp, &dest);
        combine2(vp, &dest);
        combine5(vp, &dest);
        combine6(vp, &dest);
        combine7(vp, &dest);
    }

    uint64_t tsc0, tsc1;
    double cycles[test_cases];
    data_t dests[test_cases];
    data_t *ptr_dest;

#define TEST_CASE(suffix, idx)                   \
    dest = 0;                                    \
    tsc0 = perf_counter();                       \
    for (int i = 0; i < TIMES; i++)              \
    {                                            \
        combine##suffix(vp, &dest);              \
    }                                            \
    tsc1 = perf_counter();                       \
    cycles[idx] = (double)(tsc1 - tsc0) / TIMES; \
    dests[idx] = dest

    TEST_CASE(1, 0);
    TEST_CASE(2, 1);
    TEST_CASE(5, 2);
    TEST_CASE(6, 3);
    TEST_CASE(7, 4);

    printf(",\tcycles:");
    for (int i = 0; i < test_cases; i++)
    {
        printf("%lf\t", cycles[i]);
    }
    printf("dest:%ld/%lf\n", (int64_t)dests[0], (double)dests[0]);

    for (int i = 1; i < test_cases; i++)
    {
        if (dests[i - 1] != dests[i])
            printf("err:test_cases[%d]=%ld/%lf != test_cases[%d]=%ld/%lf\n",
                   i, (int64_t)dests[i - 1], (double)dests[i - 1], i + 1, (int64_t)dests[i], (double)dests[i]);
    }

    return 0;
}
