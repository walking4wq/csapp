#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <float.h>

#include "comm.h"

// #pragma pack(4) // default is 8 on x86-64
struct S3
{
    char c;
    int i[2];
    double v;
};
// #pragma pack()

union U3
{
    char c;
    int i[2];
    double v;
};

void print_struct(struct S3 *s3)
{
    if (s3)
    {
        printf("sizeof(*s3)=%ld@\t%p\n"
               "s3->c=%c@\t%p\n"
               "s3->i[0]=%d@\t%p\n"
               "s3->i[1]=%d@\t%p\n"
               "s3->v=%f@\t%p\n",
               sizeof(*s3), s3,
               s3->c, &(s3->c),
               s3->i[0], &(s3->i[0]),
               s3->i[1], &(s3->i[1]),
               s3->v, &(s3->v));
        char buff[sizeof(*s3) * 3 - 1] = {0x00};
        string_bytes(s3, sizeof(*s3), buff);
        printf("s3=[%s]\n", buff);
        //  0  1  2  3  4  5  6  7  8  9 10 11
        // 61 00 00 00 01 00 00 00 02 00 00 00
        // fe 7f 00 00 9a 99 99 99 99 99 d9 3f
        string_bytes(&s3->c, sizeof(s3->c), buff); // 0
        printf("s3.c=[%s]\n", buff);
        string_bytes(&s3->i, sizeof(s3->i), buff); // 4+8
        printf("s3.i=[%s]\n", buff);
        string_bytes(&s3->v, sizeof(s3->v), buff); // 12+4=8*2 // 16+8
        printf("s3.v=[%s]\n", buff);
    }
    else
    {
        printf("sizeof(struct S3)=%ld\n", sizeof(struct S3));
    }
}
void print_union(union U3 *u3)
{
    if (u3)
    {
        char buff[sizeof(*u3) * 3 - 1] = {0x00};
        string_bytes(u3, sizeof(*u3), buff);
        printf("sizeof(*u3)=%ld@\t%p\t[%s]\n", sizeof(*u3), u3, buff);
        string_bytes(&u3->c, sizeof(u3->c), buff);
        printf("u3->c=%c@\t%p\t[%s]\n", u3->c, &(u3->c), buff);
        string_bytes(&u3->i, sizeof(u3->i), buff);
        printf("u3->i@\t\t%p\t[%s]\n", &(u3->i), buff);
        printf("u3->i[0]=%d@\t%p\n", u3->i[0], &(u3->i[0]));
        printf("u3->i[1]=%d@\t%p\n", u3->i[1], &(u3->i[1]));
        string_bytes(&u3->v, sizeof(u3->v), buff);
        printf("u3->v=%f@\t%p\t[%s]\n", u3->v, &(u3->v), buff);
    }
    else
    {
        printf("sizeof(union U3)=%ld\n", sizeof(union U3));
    }
}
void print_offset()
{
    struct S3 s3 = {'W', {1, 2}, 0.4};
    union U3 u3;
    u3.i[0] = 'W'; // 0x61;
    u3.i[1] = 2;

    print_struct(0);
    print_struct(&s3);
    print_union(0);
    print_union(&u3);
}

typedef union
{
    struct
    {
        short v;
        short d;
        int s;
    } t1;
    struct
    {
        int a[2];
        char *p;
    } t2;
} u_type;

void print_u_type(u_type *ut)
{
    if (ut)
    {
        char buff[sizeof(*ut) * 3 - 1] = {0x00};
        string_bytes(ut, sizeof(*ut), buff);
        printf("sizeof(*ut)=%ld@\t%p\t[%s]\n", sizeof(*ut), ut, buff);
        // string_bytes(&ut->c, sizeof(ut->c), buff);
        // printf("ut->c=%c@\t%p\t[%s]\n", ut->c, &(ut->c), buff);
        // string_bytes(&ut->i, sizeof(ut->i), buff);
        // printf("ut->i@\t\t%p\t[%s]\n", &(ut->i), buff);
        // printf("ut->i[0]=%d@\t%p\n", ut->i[0], &(ut->i[0]));
        // printf("ut->i[1]=%d@\t%p\n", ut->i[1], &(ut->i[1]));
        // string_bytes(&ut->v, sizeof(ut->v), buff);
        // printf("ut->v=%f@\t%p\t[%s]\n", ut->v, &(ut->v), buff);
    }
    else
    {
        printf("sizeof(u_type)=%ld\n", sizeof(u_type));
    }
}

void get_up_t1_s(u_type *up, int *dest)
{
    // movl 4(%eax), %eax
    // movl %eax, (%rdx)
    // %ecx
    *dest = up->t1.s;
}
void get_up_t1_v(u_type *up, short *dest)
{
    // movw %rax, (%dx) // wrong!
    // movzwl (%rax), %eax
    // movw %ax, (rdx)
    *dest = up->t1.v;
}
void get_up_t1_d(u_type *up, short **dest)
{
    // leaq 2(%rax), (%rdx)
    *dest = &up->t1.d;
}
void get_up_t2_a_ptr(u_type *up, int **dest)
{
    // movq %rax, (%rdx)
    *dest = up->t2.a;
}
void get_up_t2_a(u_type *up, int *dest)
{
    // movl 4(%rax), %ecx // wrong!
    // leaq (%ecx, %rax), %rax
    // movl (%rax), %edx

    // movslq 4(%rax), %rcx
    // movl (%rax, %rcx, 4), %eax
    // movl %eax, (%rdx)
    *dest = up->t2.a[up->t1.s];
}
void get_up_t2_p(u_type *up, char *dest)
{
    // movl 8(%rax), %rax
    // movb (%rax), %edx // wrong!

    // movq 8(%rax), %rax
    // movzbl (%rax), %eax
    // movb %al, (%rdx)
    *dest = *up->t2.p;
}

typedef struct
{
    // double dbl;
    // char chr;
    long double dbl;
} sizeof4double;

struct P1
{
    int i;
    char c;
    int j;
    char d;
};
struct P2
{
    int i;
    char c;
    char d;
    int j;
};
struct P3
{
    short w[3];
    char c[3];
};
struct P4
{
    short w[3];
    char *c[3];
};
struct P5
{
    struct P1 a[2];
    struct P2 *p;
};

struct // 56
{
    char *a;     // 0
    short b;     // 8
    double c;    // 16
    char d;      // 24
    float e;     // 28
    char f;      // 32
    long long g; // 40
    void *h;     // 48
} foo;
struct // 40
{
    char d;
    char f;
    short b;
    float e; // 4
    char *a; // 8
    double c;
    long long g;
    void *h;
} foo2;

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    print_offset();

    print_u_type(0);
    u_type ut;
    ut.t1.v = 1;
    ut.t1.d = 2;
    ut.t1.s = 3;
    print_u_type(&ut);
    ut.t2.p = (char *)0xf123456789abcdef;
    print_u_type(&ut);
    ut.t2.a[0] = 4;
    ut.t2.a[1] = 5;
    ut.t2.p = (char *)&ut;
    print_u_type(&ut);

    printf("sizeof(long double)=%ld\n", sizeof(long double)); // on IA-32 is 10, and after align is 12 for 4 align
    sizeof4double sd = {1};                                   // {1, 'a'};
    char buff[sizeof(sizeof4double) * 3 - 1] = {0x00};
    string_bytes(&sd, sizeof(sizeof4double), buff);
    printf("sizeof(sizeof4double)=%ld@\t%p\t[%s]\n", sizeof(sizeof4double), &sd, buff);

    // int64_t siz = sizeof(struct P5);
    char buff2[sizeof(struct P5) * 3 - 1] = {0x00};
    int64_t siz = sizeof(struct P1);
    struct P1 p1 = {1, 'a', 2, 'b'};
    string_bytes(&p1, siz, buff2);
    printf("sizeof(P1)=%ld@\t%p\t[%s]\n", siz, &p1, buff2);

    siz = sizeof(struct P2);
    struct P2 p2 = {1, 'a', 'b', 2};
    string_bytes(&p2, siz, buff2);
    printf("sizeof(P2)=%ld@\t%p\t[%s]\n", siz, &p2, buff2);

    siz = sizeof(struct P3);
    struct P3 p3 = {{1, 2, 3}, {'a', 'b', 'c'}};
    string_bytes(&p3, siz, buff2);
    printf("sizeof(P3)=%ld@\t%p\t[%s]\n", siz, &p3, buff2);

    siz = sizeof(struct P4);
    struct P4 p4 = {{1, 2, 3}, {(char *)0x01, (char *)0x02, (char *)0x03}};
    string_bytes(&p4, siz, buff2);
    printf("sizeof(P4)=%ld@\t%p\t[%s]\n", siz, &p4, buff2);

    siz = sizeof(struct P5);
    struct P5 p5 = {{p1, {3, 'c', 4, 'd'}}, &p2};
    string_bytes(&p5, siz, buff2);
    printf("sizeof(P5)=%ld@\t%p\t[%s]\n", siz, &p5, buff2);

    char buff3[sizeof(foo) * 3 - 1] = {0x00};
    siz = sizeof(foo);
    memset(&foo, 0, siz);
    foo.a = (char *)0xf1f2f3f4f5f6f7f8;
    foo.b = 0xf1f2;
    foo.c = DBL_MAX; // (double)0xf1f2f3f4f5f6f7f8;
    foo.d = 0xf1;
    foo.e = FLT_MAX; // (float)0xffffffff;
    foo.f = 0xf1;
    foo.g = (long long)0xf1f2f3f4f5f6f7f8;
    foo.h = (void *)0xf1f2f3f4f5f6f7f8;
    string_bytes(&foo, siz, buff3);
    printf("sizeof(foo)=%ld@\t%p\t[%s]\n", siz, &foo, buff3);

    siz = sizeof(foo2);
    memset(&foo2, 0, siz);
    foo2.a = (char *)0xf1f2f3f4f5f6f7f8;
    foo2.b = 0xf1f2;
    foo2.c = DBL_MAX; // (double)0xf1f2f3f4f5f6f7f8;
    foo2.d = 0xf1;
    foo2.e = FLT_MAX; // (float)0xffffffff;
    foo2.f = 0xf1;
    foo2.g = (long long)0xf1f2f3f4f5f6f7f8;
    foo2.h = (void *)0xf1f2f3f4f5f6f7f8;
    string_bytes(&foo2, siz, buff3);
    printf("sizeof(foo)=%ld@\t%p\t[%s]\n", siz, &foo2, buff3);
}
