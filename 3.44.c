#include <stdio.h>
#include <string.h>

int intlen(int x);
// long int gfun(int x, int y);

int main(int argc, char *argv[])
{
   int local;
   // printf("local at %p\n", &local);
   printf("%p\n", &local);

   local = intlen(argc);
   printf("intlen(argc:%d)=%d\n", argc, local);

   // argc = gfun(1, argc);
   return 0;
}

/*
$ for i in {1..10000}; do ./3.44;done|sort|sed -n '1p;10000p'
0x7ffc002901e4
0x7fffffeccf34
*/

int len(char *s)
{
   return strlen(s);
}
void iptoa(char *s, int *p)
{
   int val = *p;
   sprintf(s, "%d", val);
}
int intlen(int x)
{
   int v;
   char buf[12];
   v = x;
   iptoa(buf, &v);
   return len(buf);
}

/*
1   subl    $36, %esp
2   movl    8(%ebp), %eax       # %eax = x
3   movl    %eax, -8(%ebp)      # v = %eax
4   leal    -8(%ebp), %eax      # %eax == &v
5   movl    %eax, 4(%esp)       # p = &v
6   leal    -20(%ebp), %ebx     # %ebx=&buff
7   movl    %ebx, (%esp)        # s = &buff
8   call    iptoa

0x00:+s
0x04:|p
0x08:|
0x0c:|
0x10:|buff[0-4]
0x14:|buff[5-8]
0x18:|buff[9-12]
0x1c:|v
0x20:|
0x24:=saved out %rbp
0x28:|return address
0x2c:|x



1   subl    $52, %esp
2   movl    %gs:20, %eax
3   movl    %eax, -8(%ebp)
4   xorl    %eax, %eax
5   movl    8(%ebp), %eax     # %eax = x
6   movl    %eax, -24(%ebp)   # v = %eax
7   leal    -24(%ebp), %eax   # %eax = &v
8   movl    %eax, 4(%esp)     # p = &v
9   leal    -20(%ebp), %ebx   # %ebx=&buff
10  movl    %ebx, (%esp)
11  call    iptoa

0x00:+s
0x04:|p
0x08:|
0x0c:|
0x10:|
0x14:|
0x18:|
0x1c:|v
0x20:|buff[0-4]
0x24:|buff[5-8]
0x28:|buff[9-12]
0x2c:|canary
0x30:|
0x34:=saved out %rbp
0x38:|return address
0x3c:|x



$ gcc -O0 -g -o 3.44 3.44.c

(gdb) disassemble /s intlen
Dump of assembler code for function intlen:
3.44.c:
warning: Source file is more recent than executable.
28      {
   0x0000000000001256 <+0>:     endbr64
   0x000000000000125a <+4>:     push   %rbp
   0x000000000000125b <+5>:     mov    %rsp,%rbp
   0x000000000000125e <+8>:     sub    $0x30,%rsp
   0x0000000000001262 <+12>:    mov    %edi,-0x24(%rbp)         # x
   0x0000000000001265 <+15>:    mov    %fs:0x28,%rax
   0x000000000000126e <+24>:    mov    %rax,-0x8(%rbp)
   0x0000000000001272 <+28>:    xor    %eax,%eax

29          int v;
30          char buf[12];
31          v = x;
   0x0000000000001274 <+30>:    mov    -0x24(%rbp),%eax         # x
   0x0000000000001277 <+33>:    mov    %eax,-0x18(%rbp)         # v

32          iptoa(buf, &v);
   0x000000000000127a <+36>:    lea    -0x18(%rbp),%rdx         # &v
   0x000000000000127e <+40>:    lea    -0x14(%rbp),%rax         # buf
   0x0000000000001282 <+44>:    mov    %rdx,%rsi                # &v
   0x0000000000001285 <+47>:    mov    %rax,%rdi                # buf
   0x0000000000001288 <+50>:    call   0x1218 <iptoa>

33          return len(buf);
   0x000000000000128d <+55>:    lea    -0x14(%rbp),%rax
   0x0000000000001291 <+59>:    mov    %rax,%rdi
   0x0000000000001294 <+62>:    call   0x11fa <len>

34      }
   0x0000000000001299 <+67>:    mov    -0x8(%rbp),%rdx
   0x000000000000129d <+71>:    sub    %fs:0x28,%rdx
   0x00000000000012a6 <+80>:    je     0x12ad <intlen+87>
   0x00000000000012a8 <+82>:    call   0x1090 <__stack_chk_fail@plt>
   0x00000000000012ad <+87>:    leave
   0x00000000000012ae <+88>:    ret
End of assembler dump.
(gdb) disassemble /s iptoa
Dump of assembler code for function iptoa:
3.44.c:
23      {
   0x0000000000001218 <+0>:     endbr64
   0x000000000000121c <+4>:     push   %rbp
   0x000000000000121d <+5>:     mov    %rsp,%rbp
   0x0000000000001220 <+8>:     sub    $0x20,%rsp
   0x0000000000001224 <+12>:    mov    %rdi,-0x18(%rbp)         # char *s
   0x0000000000001228 <+16>:    mov    %rsi,-0x20(%rbp)         # int *p

24          int val = *p;
   0x000000000000122c <+20>:    mov    -0x20(%rbp),%rax
   0x0000000000001230 <+24>:    mov    (%rax),%eax
   0x0000000000001232 <+26>:    mov    %eax,-0x4(%rbp)

25          sprintf(s, "%d", val);
   0x0000000000001235 <+29>:    mov    -0x4(%rbp),%edx
   0x0000000000001238 <+32>:    mov    -0x18(%rbp),%rax
   0x000000000000123c <+36>:    lea    0xdc5(%rip),%rcx        # 0x2008
   0x0000000000001243 <+43>:    mov    %rcx,%rsi
   0x0000000000001246 <+46>:    mov    %rax,%rdi
   0x0000000000001249 <+49>:    mov    $0x0,%eax
   0x000000000000124e <+54>:    call   0x10b0 <sprintf@plt>

26      }
   0x0000000000001253 <+59>:    nop
   0x0000000000001254 <+60>:    leave
   0x0000000000001255 <+61>:    ret
End of assembler dump.
(gdb)

0x00:+
0x08:|%edi:x,
0x10:|
0x18:|buff[0-4],v
0x20:|buff[4-12]
0x28:|canary
0x30:=saved out %rbp
0x38:|return address
0x40:|



0x00007fffffffdaf0
-0x50
,
0x00007fffffffdb28

0xaf0-0x050=0xaa0
0xb28-0xaa0=0x88

0x00007fffffffdaa0
0x88=17*8

diffrs add 0x00007fffffffdaa0 --size 17

* reg             :cc/csapp/3.44.c@12|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@33|cc/csapp/3.44.c@36|cc/csapp/3.44.c@37|cc/csapp/3.44.c@37|* reg             :cc/csapp/3.44.c@28|cc/csapp/3.44.c@28|cc/csapp/3.44.c@28|cc/csapp/3.44.c@28|cc/csapp/3.44.c@28|cc/csapp/3.44.c@29|cc/csapp/3.44.c@29|cc/csapp/3.44.c@29|cc/csapp/3.44.c@30|cc/csapp/3.44.c@30|cc/csapp/3.44.c@30|cc/csapp/3.44.c@30|cc/csapp/3.44.c@30|cc/csapp/3.44.c@31|cc/csapp/3.44.c@31|cc/csapp/3.44.c@31|cc/csapp/3.44.c@38|cc/csapp/3.44.c@38|cc/csapp/3.44.c@39|cc/csapp/3.44.c@39|cc/csapp/3.44.c@39|cc/csapp/3.44.c@39|cc/csapp/3.44.c@12|cc/csapp/3.44.c@13|cc/csapp/3.44.c@13|cc/csapp/3.44.c@14
$rax              :0x0000000000000001|                  |                  |                  |                  |                  |                  |0x822d4c3396d7a700|0x0000000000000001|                  |0x00007fffffffdacc|$rax              :                  |                  |                  |                  |                  |                  |0x00007fffffffdac8|0x0000000000000001|                  |                  |0x00007fffffffdacc|                  |0x0000000000000000|0x0000000000000001|                  |                  |                  |0x00007fffffffdacc|0x0000000000000001|                  |                  |                  |                  |                  |0x0000000000000000|0x0000000000000011|
$rbx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rbx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$rcx              :0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$rcx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x000055555555601c|                  |0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$rdx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdac8|$rdx              :                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000001|                  |                  |                  |0x0000000000000000|                  |                  |                  |                  |0x00007fffffffdacc|0x822d4c3396d7a700|0x0000000000000000|                  |                  |0x0000000000000001|                  |0x0000000000000000|
$rsp              :0x00007fffffffdaf0|0x00007fffffffdae8|                  |0x00007fffffffdae0|                  |0x00007fffffffdab0|                  |                  |                  |                  |                  |$rsp              :0x00007fffffffdaa8|                  |0x00007fffffffdaa0|                  |0x00007fffffffda80|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa8|0x00007fffffffdab0|                  |                  |                  |                  |0x00007fffffffdae8|0x00007fffffffdaf0|                  |                  |                  |
$rbp              :0x00007fffffffdb10|                  |                  |                  |0x00007fffffffdae0|                  |                  |                  |                  |                  |                  |$rbp              :                  |                  |                  |0x00007fffffffdaa0|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdae0|                  |                  |                  |                  |                  |0x00007fffffffdb10|                  |                  |                  |                  |
$rsi              :0x00005555555592a0|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdac8|$rsi              : &v               |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x000055555555601c|0x000055555555601e|                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000001|                  |0x00005555555592a0|
$rdi              :0x0000000000000001| argc             |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdacc|$rdi              : buff             |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd860|                  |                  |                  |0x00007fffffffdacc|                  |                  |                  |                  |                  |                  |0x0000555555556008|0x00007fffffffd590|
$rip              :0x00005555555551eb|0x0000555555555286|0x000055555555528a|0x000055555555528b|0x000055555555528e|0x0000555555555292|0x0000555555555295|0x00005555555552a2|0x00005555555552a7|0x00005555555552aa|0x00005555555552b8|$rip              :0x0000555555555248|0x000055555555524c|0x000055555555524d|0x0000555555555250|0x0000555555555254|0x000055555555525c|0x0000555555555260|0x0000555555555262|0x0000555555555265|0x0000555555555268|0x000055555555526c|0x0000555555555273|0x000055555555527e|0x0000555555555283|0x0000555555555284|0x0000555555555285|0x00005555555552bd|0x00005555555552c4|0x00005555555552c9|0x00005555555552cd|0x00005555555552dd|0x00005555555552de|0x00005555555551f0|0x00005555555551fb|0x000055555555520a|0x000055555555520f|
$r8               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r8               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r9               :0x00007fffffffd9bc|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r9               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd817|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffd9c7|
$r10              :0x0000000000000077|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r10              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r11              :0x0000000000000246|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r11              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000246|
$r12              :0x00007fffffffdc28|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r12              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r13              :0x00005555555551a9|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r13              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r14              :0x0000555555557da8|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r14              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$r15              :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$eflags           :0x0000000000000206|                  |                  |                  |                  |0x0000000000000202|                  |                  |0x0000000000000246|                  |                  |$eflags           :                  |                  |                  |                  |0x0000000000000202|                  |                  |                  |                  |                  |                  |                  |                  |0x0000000000000206|                  |                  |                  |                  |0x0000000000000202|                  |0x0000000000000246|                  |                  |                  |                  |0x0000000000000206|
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$cs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ss               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$ds               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$es               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$fs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |$gs               :                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
* stk              call intlen        endbr64            push %rbp          mov %rsp, %rbp     sub $0x30, %rsp   mov %edi,-0x24(%rbp)                                                                            call iptoa         * stk
0x00007fffffffdaa0:0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa0:                  |                  |0x00007fffffffdae0+ Saved intlen %rbp+                  =                  =                  =                  =                  =                  =                  =                  =                  =                  =                  =                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdaa8:0x0000000000000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaa8:0x00005555555552bd+ Return address   +                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  +                  |                  |0x00005555555552c9|                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdab0:0x0000555555554040|                  |                  |                  |                  |                  +                  +                  +                  +                  +                  +0x00007fffffffdab0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  +                  +                  +                  +                  +                  |                  |                  |                  |                  |
0x00007fffffffdab8:0x00007ffff7fe285c|                  |                  |                  |                  |                  |0x00000001f7fe285c| argc:x,          |                  |                  |                  |0x00007fffffffdab8: argc:x,          |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdac0:0x0000000000000d30|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdac0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdac8:0x00007fffffffdfa9|                  |                  |                  |                  |                  |                  |                  |                  |0x00007fff00000001| ,argc:v          |0x00007fffffffdac8: buff[0-3],argc:v |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000003100000001|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdad0:0x00007ffff7fc1000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdad0: buff[4, 11]      |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdad8:0x0000010101000000|                  |                  |                  |                  |                  |                  |0x822d4c3396d7a700| canary           |                  |                  |0x00007fffffffdad8: canary           |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdae0:0x0000000000000002|                  |                  |0x00007fffffffdb10+ Saved main %rbp  +                  =                  =                  =                  =                  =                  =0x00007fffffffdae0: Saved main %rbp  =                  =                  =                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  =                  =                  =                  =                  =                  =                  |                  |                  |                  |                  |
0x00007fffffffdae8:0x00005555555551e6|0x00005555555551f0+ Return address   +                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdae8: Return address   |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  +                  |                  |                  |0x000055555555520f|
0x00007fffffffdaf0:0x00007fffffffdc28+                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf0:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  +                  +                  +                  +
0x00007fffffffdaf8:0x0000000100000064|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdaf8:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb00:0x0000000000001000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb00:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x0000000100001000|                  |                  |
0x00007fffffffdb08:0x822d4c3396d7a700|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb08:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb10:0x0000000000000001=                  =                  =                  =                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb10:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  =                  =                  =                  =                  =
0x00007fffffffdb18:0x00007ffff7dadd90|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb18:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb20:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |0x00007fffffffdb20:                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |

*/

/*
3.2.3
gcc -O1 -S -m32 3.44.c
gcc -O1 -S -m64 3.44.c
*/
long int simple_l(long int *xp, long int y)
{
   long int t = *xp + y;
   *xp = t;
   return t;
}
long int gval1 = 567;
long int gval2 = 763;
long int call_simple_l()
{
   long int z = simple_l(&gval1, 12L);
   return z + gval2;
}

long int gfun(int x, int y)
{
   long int t1 = (long)x + y;
   /* 64-bit addition */
   long int t2 = (long)(x + y); /* 32-bit addition */
   return t1 | t2;
}

/*

Practice Problem 3.48
A C function arithprob with arguments a, b, c, and d has the following body:
return a*b + c*d;
It compiles to the following x86-64 code:

1  arithprob:
2     movslq   %ecx,%rcx            # %rcx = (long) d # int d;
3     imulq    %rdx, %rcx           # %rcx = ((long) d)*c # long c;
4     movsbl   %sil,%esi            # %esi = (int) b # char b;
5     imull    %edi, %esi           # %esi = a * (int) b # int a;
6     movslq   %esi,%rsi            # %rsi = (int) b
7     leaq     (%rcx,%rsi), %rax    # %rax = ((long) d)*c + (int) b
8     ret

The arguments and return value are all signed integers of various lengths.
Arguments a, b, c, and d are passed in the appropriate regions of registers %rdi,
%rsi, %rdx, and %rcx, respectively. Based on this assembly code, write a function
prototype describing the return and argument types for arithprob.

*/

int fact_while(int n)
{
   int result = 1;
   while (n > 1)
   {
      result *= n;
      n = n - 1;
   }
   return result;
}
