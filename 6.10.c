#include <stdio.h>

#include <stdint.h>
#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

// float dotprod(float x[8], float y[8])
float dotprod(float *x, float *y, int len)
{
    float sum = 0.0;
    int i;

    for (i = 0; i < len; i++)
        sum += x[i] * y[i];
    return sum;
}

#include <string.h> // memset

void test_cache()
{
    float x2stk[8];
    float *x2mem;
    x2mem--;
    printf("x2max:%p\n", x2mem);
    x2mem = (float *)malloc(sizeof(float) * 8);
    printf("x2stk:%p\nx2mem:%p\n", x2stk, x2mem);

    printf("sizeof(float):%ld\n", sizeof(float)); // 4
    // 2^12 = 4096; 4096 / 4 = 1024
    float x[1024 * 2];
    memset(x, 0, 1024 * 2);
    printf("x[0]   :%p\nx[1]   :%p\nx[8]   :%p\nx[12]  :%p\nx[1024]:%p\n", &x[0], &x[1], &x[8], &x[12], &x[1024]);

    uint64_t tsc0, tsc1;
    float sum;
    const int TIMES = 1024;

    sum = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
        sum += dotprod(x, x + 8, 8);
    tsc1 = perf_counter();
    printf("after dotprod(x, x + 8     , 8)=%f used cycle:%lf,\t%lu\n", sum, (double)(tsc1 - tsc0) / 8 / TIMES, tsc1 - tsc0);
    sum = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
        sum += dotprod(x, x + 12, 8);
    tsc1 = perf_counter();
    printf("after dotprod(x, x + 12    , 8)=%f used cycle:%lf,\t%lu\n", sum, (double)(tsc1 - tsc0) / 8 / TIMES, tsc1 - tsc0);
    sum = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
        sum += dotprod(x, x + 512, 8);
    tsc1 = perf_counter();
    printf("after dotprod(x, x + 512   , 8)=%f used cycle:%lf,\t%lu\n", sum, (double)(tsc1 - tsc0) / 8 / TIMES, tsc1 - tsc0);
    sum = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
        sum += dotprod(x, x + 1024, 8);
    tsc1 = perf_counter();
    printf("after dotprod(x, x + 1024  , 8)=%f used cycle:%lf,\t%lu\n", sum, (double)(tsc1 - tsc0) / 8 / TIMES, tsc1 - tsc0);
    sum = 0;
    tsc0 = perf_counter();
    for (int i = 0; i < TIMES; i++)
        sum += dotprod(x, x + 1024 + 8, 8);
    tsc1 = perf_counter();
    printf("after dotprod(x, x + 1024+8, 8)=%f used cycle:%lf,\t%lu\n", sum, (double)(tsc1 - tsc0) / 8 / TIMES, tsc1 - tsc0);
}

/*
$ cat /sys/devices/system/cpu/cpu0/cache/ ?

$ getconf -a | grep CACHE
$ lscpu
$ sudo dmidecode -t cache

https://blog.csdn.net/weixin_34420979/article/details/116759988

https://zhuanlan.zhihu.com/p/80672073

$ getconf -a | grep CACHE
LEVEL1_ICACHE_SIZE                 32768                # 32768 = 32*1024 = 32KB
LEVEL1_ICACHE_ASSOC
LEVEL1_ICACHE_LINESIZE             64
LEVEL1_DCACHE_SIZE                 32768                # 32768/64 = 512 cache line
LEVEL1_DCACHE_ASSOC                8
LEVEL1_DCACHE_LINESIZE             64
LEVEL2_CACHE_SIZE                  524288
LEVEL2_CACHE_ASSOC                 8
LEVEL2_CACHE_LINESIZE              64
LEVEL3_CACHE_SIZE                  16777216
LEVEL3_CACHE_ASSOC                 0
LEVEL3_CACHE_LINESIZE              64
LEVEL4_CACHE_SIZE
LEVEL4_CACHE_ASSOC
LEVEL4_CACHE_LINESIZE

icache is instruction cache
dcache is data cache

cache line:
+-------------------------------------------+
|  tag  |   data block(cache line) |  flag  |
+-------------------------------------------+
memory address:
+-------------------------------------------+
|  tag                | index    | offset   |
+-------------------------------------------+

etc.:
内存地址的二进制形式是(低位在前面)
0x1CAABBDD
 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 = 32bit
|     tag                         |     index       | offset    |
 0 0 0 1 1 1 0 0 1 0 1 0 1 0 1 0 1 0 1 1 1 0 1 1 1 1 0 1 1 1 0 1
0x     1       C       A       A       B       B       D       D

conflict miss by these address:
00011100101010100 011101111 011101
00011100101010110 011101111 011101
00011100101010111 011101111 011101


Parameter       Description
    Fundamental parameters
S = 2^s         Number of sets
E               Number of lines per set
B = 2^b         Block size (bytes)
m = log2(M)     Number of physical (main memory) address bits

    Derived quantities
M = 2^m         Maximum number of unique memory addresses
s = log2(S)     Number of set index bits
b = log2(B)     Number of block offset bits
t = m−(s+b)     Number of tag bits
C=B*E*S         Cache size (bytes), not including overhead such as the valid and tag bits

                :m      C       B       E       |S      t       s       b
----------------:-------------------------------|-------------------------------
demo            :32     32768   64      1       |512    17      9       6
LEVEL1_DCACHE   :64     32768   64      8       |64     52      6       6

linesize = 64 byte = 16 * 4byte float
assoc = 8
index = 8*16 float = 128 float = 512 byte
cache = 512*64 byte = 32768 byte = 128*64 float = 8192 float
*/
#include <unistd.h> // sysconf

#include <stdlib.h> // malloc ...

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }
    long l1_cache_line_size = sysconf(_SC_LEVEL1_DCACHE_LINESIZE);
    long l2_cache_line_size = sysconf(_SC_LEVEL2_CACHE_LINESIZE);
    long l3_cache_line_size = sysconf(_SC_LEVEL3_CACHE_LINESIZE);

    printf("L1 Cache Line Size is %ld bytes.\n", l1_cache_line_size);
    printf("L2 Cache Line Size is %ld bytes.\n", l2_cache_line_size);
    printf("L3 Cache Line Size is %ld bytes.\n", l3_cache_line_size);

    test_cache();
    return 0;
}
