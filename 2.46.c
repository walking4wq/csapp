#include <stdio.h>
#include <stdint.h>

/*
       12345678901234567890
  12345678901234567890123456789012345678901234567890
0.000110011001100110011001100110011001100110011001100
  248

*/
uint64_t *deci(int len)
{
    // printf("len[%d] > sizeof(uint64_t)[%ld]*8 will return 0.1\n", len, sizeof(uint64_t));
    static uint64_t rst[2];
    if (len > sizeof(uint64_t) * 8)
    {
        rst[0] = 1;
        rst[1] = 10;
        return rst;
    }
    // fraction = numerator / denominator
    uint64_t numerator = 3, denominator = 32;
    int prev_i = 5;
    for (int i = 6; i <= len; i++)
    {
        uint32_t j = (i - 5) % 4;
        if (j == 1 || j == 2)
            continue;
        // 3/2^5 + 1/2^8
        numerator = (numerator << (i - prev_i)) + 1;
        denominator = (uint64_t)1 << i;
        prev_i = i;
    }
    rst[0] = numerator;
    rst[1] = denominator;
    return rst;
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    for (int i = 0; i < 66; i++)
    {
        uint64_t *fraction = deci(i);
        uint64_t numerator = fraction[0];
        uint64_t denominator = fraction[1];
        double rst = (double)numerator / denominator;
        // x=25/256
        // 1/10 - x = (256 - 25*10)/256*10
        uint64_t nmt = denominator - numerator * 10, dnmt = denominator * 10;
        printf("deci(%d)=%ld/%ld=%f\t\t\t1/10-%ld/%ld=%ld/%ld=%f\n", i, numerator, denominator, rst,
               numerator, denominator, nmt, dnmt, (double)nmt / dnmt);
        // return rst;
    }
    // deci(23)=209715/2097152=0.100000                        1/10-209715/2097152=2/20971520=0.000000
    // deci(32)=429496729/4294967296=0.100000                  1/10-429496729/4294967296=6/42949672960=0.000000
    // 0.00011001100110011001100110011001
    //   1234567890123456789012345678901234567890
    //             1         2         3
    //   12345678901234567890123
    // 0.00011001100110011001101 < round-to-even // 2.51
    // 0.00011001100110011001100110011001100
    //                         0001100110100
    uint64_t *fraction = deci(23);
    uint64_t nmt = fraction[1] - fraction[0] * 10, dnmt = fraction[1] * 10;

    uint64_t numerator = 100 * 3600 * 10 * nmt;
    double less = numerator / (double)dnmt;

    uint64_t frac02even = (fraction[0] << 2) + 1, frac12even = (fraction[1] << 2);
    int64_t nmt2even = frac12even - frac02even * 10;
    int64_t dnmt2even = frac12even * 10;
    int64_t numerator2even = 100 * 3600 * 10 * nmt2even;
    double less2even = numerator2even / (double)dnmt2even;

    float ieeefpf = 0.1;
    ieeefpf = ieeefpf * 100 * 3600 * 10;

    printf("deci(23)=%ld/%ld,after 100 hour, compare with 1/10 less:%ld/%ld=%f, diff dist:%fmeters\n"
           "deci(23)=%ld/%ld,after 100 hour, compare with 1/10 less:%ld/%ld=%f, diff dist:%fmeters when round-to-event\n"
           "ieeefpf:%f\n",
           fraction[0], fraction[1], numerator, dnmt, less, 2000 * less,
           frac02even, frac12even, numerator2even, dnmt2even, less2even, 2000 * less2even,
           ieeefpf);
}
