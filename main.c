#include <stdio.h>
#include <malloc.h>

typedef unsigned char *byte_pointer;

void show_bytes(byte_pointer start, int len)
{
    int i;
    for (i = 0; i < len; i++)
        printf(" %.2x", start[i]);
    printf("\n");
}

void show_int(int x)
{
    show_bytes((byte_pointer)&x, sizeof(int));
}

void show_float(float x)
{
    show_bytes((byte_pointer)&x, sizeof(float));
}

void show_pointer(void *x)
{
    show_bytes((byte_pointer)&x, sizeof(void *));
}

void test_show_bytes(int val)
{
    int ival = val;
    float fval = (float)ival;
    int *pval = &ival;

    show_int(ival);
    show_float(fval);
    show_pointer(pval);
}

struct ngx_cycle_s
{
    void ****conf_ctx;
    //    ngx_pool_t               *pool;
    //
    //    ngx_log_t                *log;
    //    ngx_log_t                 new_log;
    //
    //    ngx_uint_t                log_use_stderr;  /* unsigned  log_use_stderr:1; */
    //
    //    ngx_connection_t        **files;
    //    ngx_connection_t         *free_connections;
    //    ngx_uint_t                free_connection_n;
    //
    //    ngx_module_t            **modules;
    //    ngx_uint_t                modules_n;
    //    ngx_uint_t                modules_used;    /* unsigned  modules_used:1; */
    //
    //    ngx_queue_t               reusable_connections_queue;
    //    ngx_uint_t                reusable_connections_n;
    //    time_t                    connections_reuse_time;
    //
    //    ngx_array_t               listening;
    //    ngx_array_t               paths;
    //
    //    ngx_array_t               config_dump;
    //    ngx_rbtree_t              config_dump_rbtree;
    //    ngx_rbtree_node_t         config_dump_sentinel;
    //
    //    ngx_list_t                open_files;
    //    ngx_list_t                shared_memory;
    //
    //    ngx_uint_t                connection_n;
    //    ngx_uint_t                files_n;
    //
    //    ngx_connection_t         *connections;
    //    ngx_event_t              *read_events;
    //    ngx_event_t              *write_events;
    //
    //    ngx_cycle_t              *old_cycle;
    //
    //    ngx_str_t                 conf_file;
    //    ngx_str_t                 conf_param;
    //    ngx_str_t                 conf_prefix;
    //    ngx_str_t                 prefix;
    //    ngx_str_t                 error_log;
    //    ngx_str_t                 lock_file;
    //    ngx_str_t                 hostname;
};
typedef struct ngx_cycle_s ngx_cycle_t;

int main()
{
    printf("Hello, World!\n");
    test_show_bytes(12345);

    ngx_cycle_t cycle;
    void *a[] = {0, 0, 0};
    void **b = a;
    void ***c = &b;
    cycle.conf_ctx = &c;

    void *ptr;
    int nbr;
    ptr = &nbr;
    cycle.conf_ctx[0][0] = ptr;

    printf("test compile complete\n");

    int A, B, C, D, E, F, G;
    A = 1;
    B = 2;
    C = 3;
    D = 4;
    E = 5;
    F = 6;
    G = 7;
    nbr = 7;
    int *vpa = &A;
    int *vpb = &B;
    int *vpc = &C;
    int *vpd = &D;
    int *vpe = &E;
    int *vpf = &F;
    int *vpg = &G;
    printf("A=%d, B=%d, C=%d, D=%d, E=%d, F=%d, G=%d\n", A, B, C, D, E, F, G);
    printf("&A=%p, &B=%p, &C=%p, &D=%p, &E=%p, &F=%p, &G=%p\n", &A, &B, &C, &D, &E, &F, &G);
    printf("vpa=%p, vpb=%p, vpc=%p, vpd=%p, vpe=%p, vpf=%p, vpg=%p\n", vpa, vpb, vpc, vpd, vpe, vpf, vpg);
    printf("*vpa=%d, *vpb=%d, *vpc=%d, *vpd=%d, *vpe=%d, *vpf=%d, *vpg=%d\n", *((int *)vpa), *((int *)vpb),
           *((int *)vpc), *((int *)vpd), *((int *)vpe), *((int *)vpf), *((int *)vpg));

    void *buff = malloc(sizeof(void *) * nbr);
    void ****vppp = buff; // {vpa, vpb, vpc, vpd, vpe, vpf, vpg};
    void **vp = (void **)vppp;
    vp[0] = vpa;
    vp[1] = vpb;
    vp[2] = vpc;
    vp[3] = vpd;
    vp[4] = vpe;
    vp[5] = vpf;
    vp[6] = vpg;
    int i;
    for (i = 0; i < nbr; ++i)
    {
        printf("%d>vp=%p\n", i, vppp[i]);
    }
    void ***p_b = vppp[1];
    printf("p_b=%p,*p_b=%d\n", p_b, *((int *)p_b));
    int *p_e;
    p_e = (*p_b)[2];
    // printf("p_e=%p,*p_e=%d\n", p_e, *((int *)p_e));

    printf("game over!\n");
    return 0;
}
