#include <stdio.h>
#include <stdint.h>

#include <string.h>
#include "comm.h"

// #include <stdlib.h>
#include <float.h> /* FLT_ROUNDS */
#include <fenv.h>  /* fegetround, FE_* */
// #include <cfenv>
#include <math.h> /* rint */

int bit(uint64_t code, int offset)
{
    if (offset < 0 || offset > sizeof(uint64_t) * 8)
        return 0;
    return (code >> offset) & 1;
}

/*
IEEE ﬂoating point format encoding

s[1],exp[k]=E,frac[n]=M

V=(-1)^s*M*2^E

e=exp is uint
f=frac is Binary Fractional
Bias=2^(k-1)-1


Normalized: exp != 0x00...0 && != 0xff...f
    E=e-Bias
    M=1+f

Denormalized: exp == 0x00...0
    E=1-Bias
    M=f

Special: exp == 0xff...f
    ∞

 */
double ieee_fpf_encoding(uint64_t code, int k, int n, int verbose)
{
    if (k < 1)
    {
        printf("k[%d] < 1\n", k);
        return 0;
    }
    if (n < 1)
    {
        printf("n[%d] < 1\n", n);
        return 0;
    }
    int len = 1 + k + n;
    if (len > sizeof(uint64_t) * 8)
    {
        printf("len[%d]=1+k[%d]+n[%d] > %ld*8\n", len, k, n, sizeof(uint64_t));
        return 0;
    }
    // 1,11,52 = 64
    // 1,     k=8,                   n=23
    //    3          2         1
    // 1|09876543|21098765432109876543210
    // s,     exp,                   frac
    int offset4s = k + n;        // 8+23 = 31
    int offset4k = offset4s - 1; // 31-1 = 30
    // int offset4n = n - 1;        // 23-1 = 22
    double rst = 1;
    if (bit(code, offset4s))
    {
        rst = -1;
    }
    if (verbose)
        printf("sign is:%f\n", rst);
    uint64_t oxff4kn = ((uint64_t)1 << offset4s) - 1;
    uint64_t oxff4k = oxff4kn >> n;
    uint64_t oxff4n = ((uint64_t)1 << n) - 1;
    uint64_t e = (code & oxff4kn) >> n;
    uint64_t code4f = code & oxff4n;
    uint64_t Bias = ((uint64_t)1 << (k - 1)) - 1;
    if (verbose)
    {
        printf("0x%.16lx is code\n", code);
        printf("0x%.16lx is oxff4kn\n", oxff4kn);
        printf("0x%.16lx is oxff4k\n", oxff4k);
        printf("0x%.16lx is oxff4n\n", oxff4n);
        printf("0x%.16lx is e\n", e);
        printf("0x%.16lx is code4f\n", code4f);
    }
    uint64_t nmt4f = 0, dnmt4f = 1;
    for (int i = n - 1; i >= 0; i--)
    {
        nmt4f <<= 1;
        if (bit(code4f, i))
        {
            nmt4f++;
        }
    }
    dnmt4f = (uint64_t)1 << n;
    double f = (double)nmt4f / dnmt4f;
    if (verbose)
    {
        printf("f[%f]=%ld/%ld\n", f, nmt4f, dnmt4f);
        printf("Bias[%ld]=(1 << (k[%d]-1)) - 1\n", Bias, k);
    }
    if (e == oxff4k) // Special
    {
        if (code4f == 0)
        {
            if (rst > 0)
                printf("+");
            else
                printf("-");
            printf("∞:0x%.16lx\n", code);
        }
        else
        {
            printf("NaN:0x%.16lx\n", code);
        }
    }
    else if (e == 0) // Denormalized
    {
        int64_t E = 1 - Bias;
        double M = f;
        if (E < 0)
        {
            rst *= M / ((uint64_t)1 << (0 - E));
        }
        else
        {
            rst *= M * ((uint64_t)1 << E);
        }
        printf("Denormalized: M[%f]*2^E[%ld]=%f:0x%.16lx\n", M, E, rst, code);
    }
    else // Normalized
    {
        int64_t E = e - Bias;
        double M = 1 + f;
        if (E < 0)
        {
            rst *= M / ((uint64_t)1 << (0 - E));
        }
        else
        {
            rst *= M * ((uint64_t)1 << E);
        }
        printf("Normalized: M[%f]*2^E[%ld]=%f:0x%.16lx\n", M, E, rst, code);
    }
    if (verbose)
    {
        printf("double rst:%f=\n", rst);
        print_bytes(&rst, sizeof(double), false);
        printf("\n");
    }
    return rst;
}

// https://blog.csdn.net/weixin_43708622/article/details/113617562
// gcc -g -O0 2.47.c -o 2.47 -lm
void test_rint()
{
    double a = 1.2;
    double b = -1.2;
    double c = -1.5;
    double d = -1.51;
    double e = 1.5;
    double f = 1.51;
    double g = 2.5;
    double h = 2.51;

    printf("FE_TONEAREST=%d\n", FE_TONEAREST);
    printf("FE_UPWARD=%d\n", FE_UPWARD);
    printf("FE_DOWNWARD=%d\n", FE_DOWNWARD);
    printf("FE_TOWARDZERO=%d\n", FE_TOWARDZERO);
    printf("\n");

    printf("current rouding mode:%d\n", fegetround());
    printf("FLT_ROUNDS=%d\n", FLT_ROUNDS);

    printf("rint(%f)=%f\n", a, rint(a));
    printf("rint(%f)=%f\n", b, rint(b));
    printf("rint(%f)=%f\n", c, rint(c));
    printf("rint(%f)=%f\n", e, rint(e));
    printf("rint(%f)=%f\n", f, rint(f));
    printf("rint(%f)=%f\n", g, rint(g));
    printf("rint(%f)=%f\n", h, rint(h));

    printf("1-after set rounding mode\n");
    fesetround(FE_TOWARDZERO);
    printf("current rouding mode:%d\n", fegetround());
    printf("FLT_ROUNDS=%d\n", FLT_ROUNDS);
    printf("rint(%f)=%f\n", a, rint(a));
    printf("rint(%f)=%f\n", b, rint(b));
    printf("rint(%f)=%f\n", c, rint(c));
    printf("rint(%f)=%f\n", e, rint(e));
    printf("rint(%f)=%f\n", f, rint(f));
    printf("rint(%f)=%f\n", g, rint(g));
    printf("rint(%f)=%f\n", h, rint(h));

    printf("2-after set rounding mode\n");
    fesetround(FE_UPWARD);
    printf("current rouding mode:%d\n", fegetround());
    printf("FLT_ROUNDS=%d\n", FLT_ROUNDS);
    printf("rint(%f)=%f\n", a, rint(a));
    printf("rint(%f)=%f\n", b, rint(b));
    printf("rint(%f)=%f\n", c, rint(c));
    printf("rint(%f)=%f\n", e, rint(e));
    printf("rint(%f)=%f\n", f, rint(f));
    printf("rint(%f)=%f\n", g, rint(g));
    printf("rint(%f)=%f\n", h, rint(h));

    printf("3-after set rounding mode\n");
    fesetround(FE_DOWNWARD);
    printf("current rouding mode:%d\n", fegetround());
    printf("FLT_ROUNDS=%d\n", FLT_ROUNDS);
    printf("rint(%f)=%f\n", a, rint(a));
    printf("rint(%f)=%f\n", b, rint(b));
    printf("rint(%f)=%f\n", c, rint(c));
    printf("rint(%f)=%f\n", e, rint(e));
    printf("rint(%f)=%f\n", f, rint(f));
    printf("rint(%f)=%f\n", g, rint(g));
    printf("rint(%f)=%f\n", h, rint(h));
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    uint64_t code;
    int k;
    int n;
    double rst;

    code = 1; // 0 0000 001
    k = 4;
    n = 3;
    rst = ieee_fpf_encoding(code, k, n, 0);
    for (uint64_t i = 0; i <= 0x78; i++) // 0 1111 000
    {
        rst = ieee_fpf_encoding(i, k, n, 0);
    }

    code = 0x3fe0000000000000; // 0x0008000000000000; // 0x000000000000e03f;
    k = 11;
    n = 52;
    rst = ieee_fpf_encoding(code, k, n, 1);

    double rst2;
    memcpy(&rst2, &code, sizeof(double));
    printf("memcpy 0x%.16lx to double is:%f\n", code, rst2);

    // 1,11,52 = 64
    // 1,     k=8,                   n=23
    //    3          2         1
    // 1|09876543|21098765432109876543210
    // s,     exp,                   frac
    double dbl = 0.5;
    printf("double:%f=\n", dbl);
    print_bytes(&dbl, sizeof(double), false); // 0x3fe0000000000000
    printf("\n");

    dbl = 0.1;
    printf("double:%f=\n", dbl);
    print_bytes(&dbl, sizeof(double), false); // 0x3fe0000000000000
    printf("\n");

    code = 0x3fb999999999999a; // 0x9a9999999999b93f;
    k = 11;
    n = 52;
    rst = ieee_fpf_encoding(code, k, n, 1);

    code = 0;
    k = 2;
    n = 2;
    rst = ieee_fpf_encoding(code, k, n, 0);
    for (uint64_t i = 0; i <= 0xf; i++) // 0 11 11
    {
        rst = ieee_fpf_encoding(i, k, n, 0);
    }

    float flt = 12345.0;
    printf("float:%f=\n", flt);
    print_bytes(&flt, sizeof(float), false);
    printf("\n");
    int32_t i32 = flt;
    print_bytes(&i32, sizeof(int32_t), false);
    printf("\n");

    code = 0x4640e400;
    k = 8;
    n = 23;
    rst = ieee_fpf_encoding(code, k, n, 1);

    code = 0x3ffccccccccccccd;
    k = 11;
    n = 52;
    rst = ieee_fpf_encoding(code, k, n, 1);

    dbl = 1.5;
    printf("double:%f=\n", dbl);
    print_bytes(&dbl, sizeof(double), false);
    printf("\n");
    code = dbl;
    printf("uint64_t:%ld=\n", code);
    print_bytes(&code, sizeof(code), false);
    printf("\n");

    test_rint();
}
