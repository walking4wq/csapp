#include <stdint.h>
// #include <stdbool.h>
#include <stdio.h>

typedef int64_t num_t;

void store_prod(num_t *dest, uint32_t x, num_t y)
{

    // movq	%rdi, -8(%rbp)  // dest
    // movl	%esi, -12(%rbp) // x
    // movq	%rdx, -24(%rbp) // y
    // movl	-12(%rbp), %eax // %eax = x
    // imulq	-24(%rbp), %rax // %rax = x*y
    // movq	%rax, %rdx
    // movq	-8(%rbp), %rax
    // movq	%rdx, (%rax) // dest = x*y
    *dest = x * y;
}

void asm4store_prod(num_t *dest, uint32_t x, num_t y)
{
    // dest at %ebp+8, x at %ebp+12, y at %ebp+16
    // 42 = 3 * 14 = 3*10+3*4 // x = 3, y = 14
    // asm volatile(
    //     "movl  12(%ebp), %eax\n"     // %eax = x = 3
    //     "movl  20(%ebp), %ecx\n"     // %ecx = y/10 = 1
    //     "imull %eax, %ecx\n"         // %ecx = x*(y/10) = 3
    //     "mull  16(%ebp)"             // 16(%ebp)*%eax-> %edx:%eax // y%10*3 = 4*3 // %edx = 0, %eax=12
    //     "leal  (%ecx, %edx), %edx\n" // 3+0 = %edx
    //     "movl  8(%ebp), %ecx\n"      // %ecx = dest
    //     "movl  %eax, (%ecx)\n"       // 12 -> 12
    //     "movl  %edx, 4(%ecx)\n"      // 3 -> 30 // dest = 30+12
    //     : : : "%eax", "%ecx", "%edx");

    asm volatile(
        "movl  %0, %%eax\n"             // %eax = x = 3
        "movq  %1, %%rbx\n"             // 64bit copy to %rbx
        "imull %%ebx\n"                 // (y/10)*%eax-> %edx:%eax // y/10*3 = 1*3 // %edx = 0, %eax=3
        "shrq  $4, %%rbx\n"             // %ebx = y%10 = 4
        "imull %%eax, %%ebx\n"          // %ebx = 3*(y%10) = 12 // mull -> Error: number of operands mismatch for `mul'
        "leal  (%%ebx, %%edx), %%edx\n" // 12+0 = %edx

        "movq  %2, %%rcx\n"    // %ecx = dest
        "movl  %%edx, %%ecx\n" // 12
        "shlq  $4, %%rcx\n"    // 12 -> 12
        "movl  %%eax, %%ecx\n" // 3 -> 30 // dest = 30+12
        :                      // "=r"(out)
        : "r"(x), "r"(y), "r"(dest)
        : "%eax", "%rbx", "%rcx", "%edx");
}

int main()
{
    printf("Hello, World!\n");
    num_t dest = 0;
    uint32_t x = 3;
    num_t y = 14;
    printf("dest@%p=0x%.2lx:%ld\nx@%p=0x%.2x:%d\ny@%p=0x%.2lx:%ld\n", &dest, dest, dest, &x, x, x, &y, y, y);
    store_prod(&dest, x, y);
    printf("*** after func ***\n");
    printf("dest@%p=0x%.2lx:%ld\nx@%p=0x%.2x:%d\ny@%p=0x%.2lx:%ld\n", &dest, dest, dest, &x, x, x, &y, y, y);

    dest = 0;
    x = 3;
    y = 14;
    printf("dest@%p=0x%.2lx:%ld\nx@%p=0x%.2x:%d\ny@%p=0x%.2lx:%ld\n", &dest, dest, dest, &x, x, x, &y, y, y);
    store_prod(&dest, x, y);
    printf("*** after asm ***\n");
    printf("dest@%p=0x%.2lx:%ld\nx@%p=0x%.2x:%d\ny@%p=0x%.2lx:%ld\n", &dest, dest, dest, &x, x, x, &y, y, y);
}
