#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "comm.h"

void init_regs()
{
    asm volatile(INIT_REGS
                 :
                 :
                 : REGS);
}
enum addressing_model
{
    absolute,
    indirect,

    unexpected,
};
static char *addressing_model_name[] = {"absolute", "indirect", "unexpected"};

void addressing(uint8_t model)
{
    int32_t a = 0xff, b = 0xab, c = 0x13, d = 0x11;

    int32_t *p_rax, *addr;
    int16_t immediate;
    int32_t memory;
#ifndef NO_DEBUG
    printf("<<< addressing model:%s! >>>\n", addressing_model_name[model]); // (copy_by_value ? "value" : "address"));
    printf("a@%p=0x%.2x:%d\nb@%p=0x%.2x:%d\nc@%p=0x%.2x:%d\nd@%p=0x%.2x:%d\n", &a, a, a, &b, b, b, &c, c, c, &d, d, d);
    printf("rax@%p=%p\nadr@%p=%p\nimm@%p=0x%.2x:%d\nmem@%p=0x%.2x:%d\n",
           &p_rax, p_rax, &addr, addr, &immediate, immediate, immediate, &memory, memory, memory);
#endif

    switch (model)
    {
    case absolute:
        asm volatile(
            "movq %2, %%rax\n"
            "movq %%rax, %0\n"

            "movw $0x108, %1\n"
            : "=r"(p_rax), "=r"(immediate) /* output */
            : "r"(&a)                      /* input */
            : "%rax"                       /* clobbered register */
        );
        break;
    case indirect:
        asm volatile(
            "movq %2, %%rbx\n"
            "movl (%%rbx), %0\n"
            // "movl %1, %0\n"
            "movw 8(%%rbx), %1\n"
            : "=r"(memory), "=r"(immediate) /* output */
            : "r"(&b)                       /* input */
            : "%rbx"                        /* clobbered register */
        );
        break;
    case unexpected:
// char *regs[] = { // expected string literal before ‘regs’
//     "%rax",
//     "%rcx",
//     "%rdx",
//     "%rbx",
// };
#define regs "%rax", \
             "%rcx", \
             "%rdx", \
             "%rbx"
        asm volatile(
            "movq $0x1, %%rcx\n"
            "movq $0x3, %%rdx\n"

            "movq %2, %%rax\n"
            "movq %%rax, %0\n"

            "movq %3, %%rbx\n"
            "movl (%%rbx), %1\n"
            // "movl %3, %1\n"

            : "=r"(p_rax), "=r"(memory)
            : "r"(&a), "r"(&b)
            : regs // "%rax", "%rcx", "%rdx", "%rbx"
        );
        break;
    default:
#ifndef NO_DEBUG
        fprintf(stderr, "<<< unsupported addressing model:%s! >>>\n", addressing_model_name[model]);
#endif
        break;
    }

#ifndef NO_DEBUG
    printf("*** after asm ***\n");
    printf("a@%p=0x%.2x:%d\nb@%p=0x%.2x:%d\nc@%p=0x%.2x:%d\nd@%p=0x%.2x:%d\n", &a, a, a, &b, b, b, &c, c, c, &d, d, d);
    printf("rax@%p=%p\nadr@%p=%p\nimm@%p=0x%.2x:%d\nmem@%p=0x%.2x:%d\n",
           &p_rax, p_rax, &addr, addr, &immediate, immediate, immediate, &memory, memory, memory);
#endif
}

int main()
{
    printf("Hello, World!\n");
    init_regs();

    addressing(absolute);
    addressing(indirect);
    addressing(unexpected);
}
