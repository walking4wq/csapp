#include <stdio.h>
#include "vector.h"

int x[2] = {1, 2};
int y[2] = {3, 4};
int z[2];

int main()
{
    addvec(x, y, z, 2);
    printf("z = {%d,%d}\n", z[0], z[1]);
    return 0;
}

/*
gcc -g -o prog2 main.c ./lib/libvector.so

$ gdb prog2

gef➤  ctx code
──────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
   0x555555555164 <frame_dummy+4>  jmp    0x5555555550e0 <register_tm_clones>
   0x555555555169 <main+0>         endbr64 
   0x55555555516d <main+4>         push   rbp
   0x55555555516e <main+5>         mov    rbp, rsp
 → 0x555555555171 <main+8>         mov    ecx, 0x2
   0x555555555176 <main+13>        lea    rax, [rip+0x2eab]        # 0x555555558028 <z>
   0x55555555517d <main+20>        mov    rdx, rax
   0x555555555180 <main+23>        lea    rax, [rip+0x2e91]        # 0x555555558018 <y>
   0x555555555187 <main+30>        mov    rsi, rax
   0x55555555518a <main+33>        lea    rax, [rip+0x2e7f]        # 0x555555558010 <x>
   0x555555555191 <main+40>        mov    rdi, rax
   0x555555555194 <main+43>        call   0x555555555070 <addvec@plt>
─────────────────────────────────────────────────────────────────────────────────────────────────────────────

gef➤  hexdump qword 0x555555555070-0x10 --size 8
0x0000555555555060│+0x0000   <printf@plt+0000> 0x5d25fff2fa1e0ff3   
0x0000555555555068│+0x0008   <printf@plt+0008> 0x0000441f0f00002f   
0x0000555555555070│+0x0010   <addvec@plt+0000> 0x5525fff2fa1e0ff3                                           <
0x0000555555555078│+0x0018   <addvec@plt+0008> 0x0000441f0f00002f   
0x0000555555555080│+0x0020   <_start+0000> 0x8949ed31fa1e0ff3   
0x0000555555555088│+0x0028   <_start+0008> 0xe48348e289485ed1   
0x0000555555555090│+0x0030   <_start+0010> 0xc931c031455450f0   
0x0000555555555098│+0x0038   <_start+0018> 0xff000000ca3d8d48

$ objdump -Dx prog2 |less
...
Disassembly of section .plt.got:

0000000000001050 <__cxa_finalize@plt>:
    1050:       f3 0f 1e fa             endbr64 
    1054:       f2 ff 25 9d 2f 00 00    bnd jmp *0x2f9d(%rip)        # 3ff8 <__cxa_finalize@GLIBC_2.2.5>
    105b:       0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)

Disassembly of section .plt.sec:

0000000000001060 <printf@plt>:
    1060:       f3 0f 1e fa             endbr64 
    1064:       f2 ff 25 5d 2f 00 00    bnd jmp *0x2f5d(%rip)        # 3fc8 <printf@GLIBC_2.2.5>
    106b:       0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)

0000000000001070 <addvec@plt>:
    1070:       f3 0f 1e fa             endbr64 
    1074:       f2 ff 25 55 2f 00 00    bnd jmp *0x2f55(%rip)        # 3fd0 <addvec@Base>                   <
    107b:       0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)
...
0000000000001169 <main>:
    1169:       f3 0f 1e fa             endbr64 
    116d:       55                      push   %rbp
    116e:       48 89 e5                mov    %rsp,%rbp
    1171:       b9 02 00 00 00          mov    $0x2,%ecx
    1176:       48 8d 05 ab 2e 00 00    lea    0x2eab(%rip),%rax        # 4028 <z>
    117d:       48 89 c2                mov    %rax,%rdx
    1180:       48 8d 05 91 2e 00 00    lea    0x2e91(%rip),%rax        # 4018 <y>
    1187:       48 89 c6                mov    %rax,%rsi
    118a:       48 8d 05 7f 2e 00 00    lea    0x2e7f(%rip),%rax        # 4010 <x>
    1191:       48 89 c7                mov    %rax,%rdi
    1194:       e8 d7 fe ff ff          call   1070 <addvec@plt>                                            <
    1199:       8b 15 8d 2e 00 00       mov    0x2e8d(%rip),%edx        # 402c <z+0x4>
    119f:       8b 05 83 2e 00 00       mov    0x2e83(%rip),%eax        # 4028 <z>
    11a5:       89 c6                   mov    %eax,%esi
    11a7:       48 8d 05 56 0e 00 00    lea    0xe56(%rip),%rax        # 2004 <_IO_stdin_used+0x4>
    11ae:       48 89 c7                mov    %rax,%rdi
    11b1:       b8 00 00 00 00          mov    $0x0,%eax
    11b6:       e8 a5 fe ff ff          call   1060 <printf@plt>
    11bb:       b8 00 00 00 00          mov    $0x0,%eax
    11c0:       5d                      pop    %rbp
    11c1:       c3                      ret    
...

    1074:       f2 ff 25 55 2f 00 00    bnd jmp *0x2f55(%rip)        # 3fd0 <addvec@Base>

0x555555555070 │+0x0010   <addvec@plt+0000> 0x5525fff2fa1e0ff3
                                           55 25 ff f2|fa 1e 0f f3 = endbr64
0x555555555078 │+0x0018   <addvec@plt+0008> 0x0000441f0f00002f



   0x55555555518a <main+33>        lea    rax, [rip+0x2e7f]        # 0x555555558010 <x>
   0x555555555191 <main+40>        mov    rdi, rax
 → 0x555555555194 <main+43>        call   0x555555555070 <addvec@plt>
   ↳  0x555555555070 <addvec@plt+0>   endbr64 
      0x555555555074 <addvec@plt+4>   bnd    jmp QWORD PTR [rip+0x2f55]        # 0x555555557fd0 <addvec@got.plt>
      0x55555555507b <addvec@plt+11>  nop    DWORD PTR [rax+rax*1+0x0]


gef➤  hexdump qword 0x555555557fd0-0x10 --size 8
0x0000555555557fc0│+0x0000   <_GLOBAL_OFFSET_TABLE_+0010> 0x0000000000000000   
0x0000555555557fc8│+0x0008   <printf@got[plt]+0000> 0x00007ffff7ddf770   
0x0000555555557fd0│+0x0010   <addvec@got[plt]+0000> 0x00007ffff7fb70f9                       0x7ffff7fb70f9 <
0x0000555555557fd8│+0x0018   0x00007ffff7da8dc0   
0x0000555555557fe0│+0x0020   0x0000000000000000   
0x0000555555557fe8│+0x0028   0x0000000000000000   
0x0000555555557ff0│+0x0030   0x0000000000000000   
0x0000555555557ff8│+0x0038   0x00007ffff7dc49a0 

──────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
   0x7ffff7fb70e8 <__do_global_dtors_aux+56> ret    
   0x7ffff7fb70e9 <__do_global_dtors_aux+57> nop    DWORD PTR [rax+0x0]
   0x7ffff7fb70f0 <frame_dummy+0>  endbr64 
   0x7ffff7fb70f4 <frame_dummy+4>  jmp    0x7ffff7fb7070 <register_tm_clones>
 → 0x7ffff7fb70f9 <addvec+0>       endbr64 
   0x7ffff7fb70fd <addvec+4>       push   rbp
   0x7ffff7fb70fe <addvec+5>       mov    rbp, rsp
   0x7ffff7fb7101 <addvec+8>       mov    QWORD PTR [rbp-0x18], rdi
   0x7ffff7fb7105 <addvec+12>      mov    QWORD PTR [rbp-0x20], rsi
   0x7ffff7fb7109 <addvec+16>      mov    QWORD PTR [rbp-0x28], rdx
   0x7ffff7fb710d <addvec+20>      mov    DWORD PTR [rbp-0x2c], ecx
   0x7ffff7fb7110 <addvec+23>      mov    rax, QWORD PTR [rip+0x2ec9]        # 0x7ffff7fb9fe0
─────────────────────────────────────────────────────────────────────--─────────── source:lib/addvec.c+4 ────



gef➤  hexdump qword 0x7ffff7fb70f9-0x10 --size 8
0x00007ffff7fb70e9│+0x0000   <__do_global_dtors_aux+0039> 0xf300000000801f0f   
0x00007ffff7fb70f1│+0x0008   <frame_dummy+0001> 0xffffff77e9fa1e0f   
0x00007ffff7fb70f9│+0x0010   <addvec+0000> 0xe5894855fa1e0ff3                                               <   
0x00007ffff7fb7101│+0x0018   <addvec+0008> 0xe0758948e87d8948   
0x00007ffff7fb7109│+0x0020   <addvec+0010> 0x48d44d89d8558948   
0x00007ffff7fb7111│+0x0028   <addvec+0018> 0x008b00002ec9058b   
0x00007ffff7fb7119│+0x0030   <addvec+0020> 0x2ebd058b4801508d   
0x00007ffff7fb7121│+0x0038   <addvec+0028> 0x00fc45c710890000   



$ objdump -Dx lib/libvector.so |less
...
00000000000010f9 <addvec>:
    10f9:       f3 0f 1e fa             endbr64 
    10fd:       55                      push   %rbp
    10fe:       48 89 e5                mov    %rsp,%rbp
    1101:       48 89 7d e8             mov    %rdi,-0x18(%rbp)
    1105:       48 89 75 e0             mov    %rsi,-0x20(%rbp)
    1109:       48 89 55 d8             mov    %rdx,-0x28(%rbp)
    110d:       89 4d d4                mov    %ecx,-0x2c(%rbp)
    1110:       48 8b 05 c9 2e 00 00    mov    0x2ec9(%rip),%rax        # 3fe0 <addcnt-0x44>
    1117:       8b 00                   mov    (%rax),%eax
    1119:       8d 50 01                lea    0x1(%rax),%edx
    111c:       48 8b 05 bd 2e 00 00    mov    0x2ebd(%rip),%rax        # 3fe0 <addcnt-0x44>
    1123:       89 10                   mov    %edx,(%rax)
    1125:       c7 45 fc 00 00 00 00    movl   $0x0,-0x4(%rbp)
    112c:       eb 48                   jmp    1176 <addvec+0x7d>
    112e:       8b 45 fc                mov    -0x4(%rbp),%eax
    1131:       48 98                   cltq   
    1133:       48 8d 14 85 00 00 00    lea    0x0(,%rax,4),%rdx
    113a:       00 
    113b:       48 8b 45 e8             mov    -0x18(%rbp),%rax
    113f:       48 01 d0                add    %rdx,%rax
    1142:       8b 08                   mov    (%rax),%ecx
    1144:       8b 45 fc                mov    -0x4(%rbp),%eax
    1147:       48 98                   cltq   
    1149:       48 8d 14 85 00 00 00    lea    0x0(,%rax,4),%rdx
    1150:       00 
    1151:       48 8b 45 e0             mov    -0x20(%rbp),%rax
    1155:       48 01 d0                add    %rdx,%rax
    1158:       8b 10                   mov    (%rax),%edx
    115a:       8b 45 fc                mov    -0x4(%rbp),%eax
    115d:       48 98                   cltq   
    115f:       48 8d 34 85 00 00 00    lea    0x0(,%rax,4),%rsi
    1166:       00 
    1167:       48 8b 45 d8             mov    -0x28(%rbp),%rax
    116b:       48 01 f0                add    %rsi,%rax
    116e:       01 ca                   add    %ecx,%edx
    1170:       89 10                   mov    %edx,(%rax)
    1172:       83 45 fc 01             addl   $0x1,-0x4(%rbp)
    1176:       8b 45 fc                mov    -0x4(%rbp),%eax
    1179:       3b 45 d4                cmp    -0x2c(%rbp),%eax
    117c:       7c b0                   jl     112e <addvec+0x35>
    117e:       90                      nop
    117f:       90                      nop
    1180:       5d                      pop    %rbp
    1181:       c3                      ret    
...



https://blog.csdn.net/hanchaoman/article/details/103614665
https://www.technovelty.org/linux/plt-and-got-the-key-to-code-sharing-and-dynamic-libraries.html



gcc -g -rdynamic -o prog2r dll.c -ldl

$ objdump -Dx prog2r|less
...
0000000000001209 <main>:
    1209:       f3 0f 1e fa             endbr64 
    120d:       55                      push   %rbp
    120e:       48 89 e5                mov    %rsp,%rbp
    1211:       48 83 ec 20             sub    $0x20,%rsp
    1215:       be 01 00 00 00          mov    $0x1,%esi
    121a:       48 8d 05 e3 0d 00 00    lea    0xde3(%rip),%rax        # 2004 <_IO_stdin_used+0x4>
    1221:       48 89 c7                mov    %rax,%rdi
    1224:       e8 a7 fe ff ff          call   10d0 <dlopen@plt>
    1229:       48 89 45 e8             mov    %rax,-0x18(%rbp)
    122d:       48 83 7d e8 00          cmpq   $0x0,-0x18(%rbp)
    1232:       75 30                   jne    1264 <main+0x5b>
    1234:       e8 77 fe ff ff          call   10b0 <dlerror@plt>
    1239:       48 89 c2                mov    %rax,%rdx
    123c:       48 8b 05 dd 2d 00 00    mov    0x2ddd(%rip),%rax        # 4020 <stderr@GLIBC_2.2.5>
    1243:       48 8d 0d cd 0d 00 00    lea    0xdcd(%rip),%rcx        # 2017 <_IO_stdin_used+0x17>
    124a:       48 89 ce                mov    %rcx,%rsi
    124d:       48 89 c7                mov    %rax,%rdi
    1250:       b8 00 00 00 00          mov    $0x0,%eax
    1255:       e8 86 fe ff ff          call   10e0 <fprintf@plt>
    125a:       bf 01 00 00 00          mov    $0x1,%edi
    125f:       e8 9c fe ff ff          call   1100 <exit@plt>
    1264:       48 8b 45 e8             mov    -0x18(%rbp),%rax
    1268:       48 8d 15 ac 0d 00 00    lea    0xdac(%rip),%rdx        # 201b <_IO_stdin_used+0x1b>
    126f:       48 89 d6                mov    %rdx,%rsi
    1272:       48 89 c7                mov    %rax,%rdi
    1275:       e8 76 fe ff ff          call   10f0 <dlsym@plt>
    127a:       48 89 45 f0             mov    %rax,-0x10(%rbp)         ### set dlsym to %rax ###
    127e:       e8 2d fe ff ff          call   10b0 <dlerror@plt>
    1283:       48 89 45 f8             mov    %rax,-0x8(%rbp)
    1287:       48 83 7d f8 00          cmpq   $0x0,-0x8(%rbp)
    128c:       74 2c                   je     12ba <main+0xb1>
    128e:       48 8b 05 8b 2d 00 00    mov    0x2d8b(%rip),%rax        # 4020 <stderr@GLIBC_2.2.5>
    1295:       48 8b 55 f8             mov    -0x8(%rbp),%rdx
    1299:       48 8d 0d 77 0d 00 00    lea    0xd77(%rip),%rcx        # 2017 <_IO_stdin_used+0x17>
    12a0:       48 89 ce                mov    %rcx,%rsi
    12a3:       48 89 c7                mov    %rax,%rdi
    12a6:       b8 00 00 00 00          mov    $0x0,%eax
    12ab:       e8 30 fe ff ff          call   10e0 <fprintf@plt>
    12b0:       bf 01 00 00 00          mov    $0x1,%edi
    12b5:       e8 46 fe ff ff          call   1100 <exit@plt>
    12ba:       48 8b 45 f0             mov    -0x10(%rbp),%rax         ### call dlsym ###
    12be:       b9 02 00 00 00          mov    $0x2,%ecx
    12c3:       48 8d 15 66 2d 00 00    lea    0x2d66(%rip),%rdx        # 4030 <z>
    12ca:       48 8d 35 47 2d 00 00    lea    0x2d47(%rip),%rsi        # 4018 <y>
    12d1:       48 8d 3d 38 2d 00 00    lea    0x2d38(%rip),%rdi        # 4010 <x>
    12d8:       ff d0                   call   *%rax
    12da:       8b 15 54 2d 00 00       mov    0x2d54(%rip),%edx        # 4034 <z+0x4>
    12e0:       8b 05 4a 2d 00 00       mov    0x2d4a(%rip),%eax        # 4030 <z>
    12e6:       89 c6                   mov    %eax,%esi
    12e8:       48 8d 05 33 0d 00 00    lea    0xd33(%rip),%rax        # 2022 <_IO_stdin_used+0x22>
    12ef:       48 89 c7                mov    %rax,%rdi
    12f2:       b8 00 00 00 00          mov    $0x0,%eax
    12f7:       e8 c4 fd ff ff          call   10c0 <printf@plt>
    12fc:       48 8b 45 e8             mov    -0x18(%rbp),%rax
    1300:       48 89 c7                mov    %rax,%rdi
    1303:       e8 08 fe ff ff          call   1110 <dlclose@plt>
    1308:       85 c0                   test   %eax,%eax
    130a:       79 30                   jns    133c <main+0x133>
    130c:       e8 9f fd ff ff          call   10b0 <dlerror@plt>
    1311:       48 89 c2                mov    %rax,%rdx
    1314:       48 8b 05 05 2d 00 00    mov    0x2d05(%rip),%rax        # 4020 <stderr@GLIBC_2.2.5>
    131b:       48 8d 0d f5 0c 00 00    lea    0xcf5(%rip),%rcx        # 2017 <_IO_stdin_used+0x17>
    1322:       48 89 ce                mov    %rcx,%rsi
    1325:       48 89 c7                mov    %rax,%rdi
    1328:       b8 00 00 00 00          mov    $0x0,%eax
    132d:       e8 ae fd ff ff          call   10e0 <fprintf@plt>
    1332:       bf 01 00 00 00          mov    $0x1,%edi
    1337:       e8 c4 fd ff ff          call   1100 <exit@plt>
    133c:       b8 00 00 00 00          mov    $0x0,%eax
    1341:       c9                      leave  
    1342:       c3                      ret    

*/
