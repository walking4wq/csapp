#include <stdio.h>

void assign()
{
    int a = 10, b;
    asm("movl %1, %%eax\n"
        "movl %%eax, %0\n"
        : "=r"(b) /* output */
        : "r"(a)  /* input */
        : "%eax"  /* clobbered register */
    );

    printf("a=%d,b=%d!\n", a, b);
}

void five_times()
{
    int x = 3, rst;

    __asm__ __volatile__(
        "leal (%%ecx,%%ecx,4), %%ecx"
        : "=r"(x)
        : "0"(x));
    printf("x=%d,rst=%d!\n", x, rst);

    __asm__ __volatile__(
        "leal (%1,%1,4), %0"
        : "=r"(rst)
        : "r"(x));
    printf("x=%d,rst=%d!\n", x, rst);
}

int main()
{
    printf("Hello, World!\n");
    assign();
    five_times();
}
