extern int buf[];

int *bufp0 = &buf[0];
int *bufp1;

void swap()
{
    int temp;

    bufp1 = &buf[1];
    temp = *bufp0;
    *bufp0 = *bufp1;
    *bufp1 = temp;
}

/*
$ readelf -s swap.o 

Symbol table '.symtab' contains 7 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND 
     1: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS swap.c
     2: 0000000000000000     0 SECTION LOCAL  DEFAULT    1 .text
     3: 0000000000000000    45 FUNC    GLOBAL DEFAULT    1 swap
     4: 0000000000000000     0 NOTYPE  GLOBAL DEFAULT  UND buf
     5: 0000000000000000     8 OBJECT  GLOBAL DEFAULT    4 bufp1
     6: 0000000000000000     8 OBJECT  GLOBAL DEFAULT    5 bufp0

$ readelf -s m.o 

Symbol table '.symtab' contains 6 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND 
     1: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS m.c
     2: 0000000000000000     0 SECTION LOCAL  DEFAULT    1 .text
     3: 0000000000000000    28 FUNC    GLOBAL DEFAULT    1 main
     4: 0000000000000000     0 NOTYPE  GLOBAL DEFAULT  UND swap
     5: 0000000000000000     8 OBJECT  GLOBAL DEFAULT    3 buf


csapp:
Symbol  .symtab entry?  Symbol type     Module where defined    Section
buf     Yes             extern          m.o                     .data
bufp0   Yes             global          swap.o                  .data
bufp1   Yes             global          swap.o                  COMMON
swap    Yes             global          swap.o                  .text
temp    No              —               —                       —



            0   |ELF header
            1   |.text
            2   |.rodata
            3   |.data
            4   |.bss
Sections    5   |.symtab
            6   |.rel.text
            7   |.rel.data
            8   |.debug
            9   |.line
            10  |.strtab
Describes       |Section header table
object file
sections

*/
