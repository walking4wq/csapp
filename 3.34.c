#include <stdint.h>
#include <stdio.h>

/*
Recursive Procedures

1   movl    8(%ebp), %ebx       // x
2   movl    $0, %eax
3   testl   %ebx, %ebx
4   je      .L3
5   movl    %ebx, %eax
6   shrl    %eax                // Shift right by 1 // %eax = x >> 1
7   movl    %eax, (%esp)
8   call    rfun
9   movl    %ebx, %edx          // %edx = x
10  andl    $1, %edx            // %edx &= 1
11  leal    (%edx,%eax), %eax   // %eax = x&1 + x>>1
12  .L3:
*/
int rfun(uint32_t x)
{
    if (x == 0)
        return 0;
    uint32_t nx = x >> 1;
    int rv = rfun(nx);
    return (x & 1) + rv;
}
/*
(gdb) disass /m rfun
Dump of assembler code for function rfun:
warning: Source file is more recent than executable.
21      {
   0x0000000000001169 <+0>:     endbr64 
   0x000000000000116d <+4>:     push   %rbp
   0x000000000000116e <+5>:     mov    %rsp,%rbp
   0x0000000000001171 <+8>:     sub    $0x20,%rsp
   0x0000000000001175 <+12>:    mov    %edi,-0x14(%rbp)

22          if (x == 0)
   0x0000000000001178 <+15>:    cmpl   $0x0,-0x14(%rbp)
   0x000000000000117c <+19>:    jne    0x1185 <rfun+28>

23              return 0;
   0x000000000000117e <+21>:    mov    $0x0,%eax
   0x0000000000001183 <+26>:    jmp    0x11a7 <rfun+62>

24          uint32_t nx = x >> 1;
   0x0000000000001185 <+28>:    mov    -0x14(%rbp),%eax
   0x0000000000001188 <+31>:    shr    %eax
   0x000000000000118a <+33>:    mov    %eax,-0x8(%rbp)

25          int rv = rfun(nx);
   0x000000000000118d <+36>:    mov    -0x8(%rbp),%eax
   0x0000000000001190 <+39>:    mov    %eax,%edi
   0x0000000000001192 <+41>:    call   0x1169 <rfun>
   0x0000000000001197 <+46>:    mov    %eax,-0x4(%rbp)

26          return (x & 1) + rv;
   0x000000000000119a <+49>:    mov    -0x14(%rbp),%eax
   0x000000000000119d <+52>:    and    $0x1,%eax
   0x00000000000011a0 <+55>:    mov    %eax,%edx
   0x00000000000011a2 <+57>:    mov    -0x4(%rbp),%eax
   0x00000000000011a5 <+60>:    add    %edx,%eax

27      }
   0x00000000000011a7 <+62>:    leave  
   0x00000000000011a8 <+63>:    ret    

End of assembler dump.

%edi: x                                         x>>1
%eax:                         x     x>>1
%rbp: 0x60        0x38  0x38                                      0x08
%rsp: 0x40  0x38        0x18                    0x10        0x08
%rip: 0     5     8     12    31    33    36    41    0     5     8
0x40 |-----|rtad_|_____|_____|_____|_____|_____|_____|_____|_____|_____|
0x38 |     |0x60 |     |     |     |     |     |     |     |     |     |
0x30 |     +-----+-----|     |     |     |,x>>1|     |     |     |     |
0x28 |                 |     |     |     |     |     |     |     |     |
0x20 |                 |     |x,   |     |     |     |     |     |     |
0x18 |                 |     |     |     |     |     |     |     |     |
0x10 |                 +-----+-----+-----+-----+-----|46   |46___|_____|
0x08 |                                               +-----|0x38 |     |
0x00 |                                                     +-----+-----+

*/

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    printf("rfun:\n");
    for (int i = 0; i <= argc; i++)
    {
        int result = rfun(i);
        printf("%d\t>%d\n", i, result);
    }
    printf("end of rfun:\n");
}
