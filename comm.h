#ifndef __CSAPP2_COMM_H__
#define __CSAPP2_COMM_H__
/**
 * pic 3-35
 * https://blog.csdn.net/u014753756/article/details/124535192
 */
char *init_register =
    "movq $10, %%rax\n" // accumulator
    "movq $11, %%rcx\n" // base
    "movq $12, %%rdx\n" // counter
    "movq $13, %%rbx\n" // data
    "movq $14, %%rsi\n" // source index
    "movq $15, %%rdi\n" // destination index
    // "movq $16, %%rsp\n" // stack pointer
    // "movq $17, %%rbp\n" // base pointer
    "movq $18, %%r8\n"
    "movq $19, %%r9\n"
    "movq $20, %%r10\n"
    "movq $21, %%r11\n"
    "movq $22, %%r12\n"
    "movq $23, %%r13\n"
    "movq $24, %%r14\n"
    "movq $25, %%r15\n";

char *registers[] = {
    "%rax",
    "%rcx",
    "%rdx",
    "%rbx",
    "%rsi",
    "%rdi",
    // "%rsp",
    // "%rbp",
    "%r8",
    "%r9",
    "%r10",
    "%r11",
    "%r12",
    "%r13",
    "%r14",
    "%r15",
};
#define INIT_REGS       \
    "movq $10, %%rax\n" \
    "movq $11, %%rcx\n" \
    "movq $12, %%rdx\n" \
    "movq $13, %%rbx\n" \
    "movq $14, %%rsi\n" \
    "movq $15, %%rdi\n" \
    "movq $18, %%r8\n"  \
    "movq $19, %%r9\n"  \
    "movq $20, %%r10\n" \
    "movq $21, %%r11\n" \
    "movq $22, %%r12\n" \
    "movq $23, %%r13\n" \
    "movq $24, %%r14\n" \
    "movq $25, %%r15\n"

#define REGS "%rax", \
             "%rcx", \
             "%rdx", \
             "%rbx", \
             "%rsi", \
             "%rdi", \
             "%r8",  \
             "%r9",  \
             "%r10", \
             "%r11", \
             "%r12", \
             "%r13", \
             "%r14", \
             "%r15"

#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include <stdbool.h>

void print_bytes(void *ptr, int len, bool big_endian)
{
    if (len)
    {
        printf("0x");
    }
    uint8_t *ptr_ = (uint8_t *)ptr;
    if (big_endian)
    {
        for (int i = 0; i < len; i++)
        {
            printf("%.2x", ptr_[i]);
        }
    }
    else
    {
        for (int i = len - 1; i > -1; i--)
        {
            printf("%.2x", ptr_[i]);
        }
    }
}
void string_bytes(void *ptr, int len, char *buff)
{
    uint8_t *ptr_ = (uint8_t *)ptr;
    for (int i = 0; i < len; i++)
    {
        if (i)
        {
            buff += 3;
        }
        sprintf(buff, "%.2x ", ptr_[i]);
    }
    if (len)
    {
        buff[2] = 0;
    }
    // printf("\n");
}
typedef struct _reg_stk_ reg_stk;
struct _reg_stk_
{
    // void *rsp, *rbp, *rip;
    uint64_t rsp, rbp, rip;
    size_t len;
    void *adr, *stk;

    reg_stk *prev, *next;
};
reg_stk *new_reg_stk(reg_stk *prev)
{
    reg_stk *rs = (reg_stk *)malloc(sizeof(reg_stk));
    rs->prev = prev;
    if (prev)
        prev->next = rs;
    return rs;
}
void print_reg_stk(reg_stk *rs, size_t step)
{
    if (rs == NULL)
        return;
    if (step == 0)
        step = 8;
    while (rs && rs->prev)
    {
        reg_stk *tmp = rs->prev;
        tmp->next = rs;
        rs = tmp;
    }
    reg_stk *hdr = rs;
    bool first = true;
    uint64_t preval = 0;
    printf("rip:");
    do
    {
        if (first || preval != rs->rip)
        {
            print_bytes(&rs->rip, sizeof(rs->rip), false);
            printf("|");
            first = false;
            preval = rs->rip;
        }
        else
        {
            // 0x0000000000000000
            // 12345678901234567890
            printf("%19c", '|');
        }
    } while (rs = rs->next);
    rs = hdr;
    first = true;
    preval = 0;
    printf("\nrsp:");
    do
    {
        if (first || preval != rs->rsp)
        {
            print_bytes(&rs->rsp, sizeof(rs->rsp), false);
            printf("|");
            first = false;
            preval = rs->rsp;
        }
        else
        {
            printf("%18c", '|');
        }
    } while (rs = rs->next);
    rs = hdr;
    first = true;
    preval = 0;
    printf("\nrbp:");
    do
    {
        if (first || preval != rs->rbp)
        {
            print_bytes(&rs->rbp, sizeof(rs->rbp), false);
            printf("|");
            first = false;
            preval = rs->rbp;
        }
        else
        {
            printf("%18s", "|");
        }
    } while (rs = rs->next);

    // uint32_t size = 0;
    // do
    // {
    //     size++;
    //     rs = rs->next;
    // } while (rs->next);
    // void *voids[size];
    printf("\n");
}

reg_stk *trace_register2(reg_stk *rs)
{
    rs = new_reg_stk(rs);
    asm volatile(
        "movq %%rsp, %0\n"
        "movq %%rbp, %1\n"
        // "movq %%rip, %2\n"
        : "=r"(rs->rsp), "=r"(rs->rbp) //, "=r"(rip)
        :
        : //"%rsp", "%rbp", "%rip"
    );
    return rs;
}
#define trace_register(rs)                 \
    do                                     \
    {                                      \
        rs = new_reg_stk(rs);              \
        asm volatile(                      \
            "movq %%rsp, %0\n"             \
            "movq %%rbp, %1\n"             \
            : "=r"(rs->rsp), "=r"(rs->rbp) \
            :                              \
            :);                            \
    } while (0)

void cache4reg_stk(reg_stk *rs, size_t max_siz, size_t more_rbp, size_t more_rsp)
{
    if (rs->rbp < rs->rsp)
        rs->rbp = rs->rsp;
    uint64_t rbp = rs->rbp + more_rbp;
    uint64_t rsp = rs->rsp - more_rsp;
    if (rbp - rsp < max_siz)
        max_siz = rbp - rsp;
    rs->adr = (void *)rsp;
    rs->stk = malloc(max_siz);
    rs->len = max_siz;
    memcpy(rs->stk, rs->adr, max_siz);
}

// ref gdbsupport/common-types.h

/* * An address in the program being debugged.  Host byte order.  */
typedef uint64_t CORE_ADDR;

/* LONGEST must be at least as big as CORE_ADDR.  */

typedef int64_t LONGEST;
typedef uint64_t ULONGEST;

/* * The largest CORE_ADDR value.  */
#define CORE_ADDR_MAX (~(CORE_ADDR)0)

/* * The largest ULONGEST value, 0xFFFFFFFFFFFFFFFF for 64-bits.  */
#define ULONGEST_MAX (~(ULONGEST)0)

/* * The largest LONGEST value, 0x7FFFFFFFFFFFFFFF for 64-bits.  */
#define LONGEST_MAX ((LONGEST)(ULONGEST_MAX >> 1))

/* * The smallest LONGEST value, 0x8000000000000000 for 64-bits.  */
#define LONGEST_MIN ((LONGEST)(~(LONGEST)0 ^ LONGEST_MAX))

#define REG_STKS_LEN 8
#define STKS_LEN 12 // 0x60 = 6*16 = 12*8
typedef struct
{
    CORE_ADDR rax[REG_STKS_LEN];
    CORE_ADDR rbx[REG_STKS_LEN];
    CORE_ADDR rcx[REG_STKS_LEN];
    CORE_ADDR rdx[REG_STKS_LEN];

    CORE_ADDR rsi[REG_STKS_LEN];
    CORE_ADDR rdi[REG_STKS_LEN];

    CORE_ADDR r8[REG_STKS_LEN];
    CORE_ADDR r9[REG_STKS_LEN];
    CORE_ADDR r10[REG_STKS_LEN];
    CORE_ADDR r11[REG_STKS_LEN];
    CORE_ADDR r12[REG_STKS_LEN];
    CORE_ADDR r13[REG_STKS_LEN];
    CORE_ADDR r14[REG_STKS_LEN];
    CORE_ADDR r15[REG_STKS_LEN];

    CORE_ADDR rip[REG_STKS_LEN];
    CORE_ADDR rsp[REG_STKS_LEN];
    CORE_ADDR rbp[REG_STKS_LEN];
    size_t len[REG_STKS_LEN];
    CORE_ADDR adr[REG_STKS_LEN];
    CORE_ADDR stk[REG_STKS_LEN][STKS_LEN];
    uint8_t loc[REG_STKS_LEN][18];
} reg_stks;

#define DEFINE_REG_STKS(type_name, reg_stks_len, stk_len) \
    typedef struct                                        \
    {                                                     \
        CORE_ADDR rax[REG_STKS_LEN];                      \
        CORE_ADDR rbx[REG_STKS_LEN];                      \
        CORE_ADDR rcx[REG_STKS_LEN];                      \
        CORE_ADDR rdx[REG_STKS_LEN];                      \
                                                          \
        CORE_ADDR rsi[REG_STKS_LEN];                      \
        CORE_ADDR rdi[REG_STKS_LEN];                      \
                                                          \
        CORE_ADDR r8[REG_STKS_LEN];                       \
        CORE_ADDR r9[REG_STKS_LEN];                       \
        CORE_ADDR r10[REG_STKS_LEN];                      \
        CORE_ADDR r11[REG_STKS_LEN];                      \
        CORE_ADDR r12[REG_STKS_LEN];                      \
        CORE_ADDR r13[REG_STKS_LEN];                      \
        CORE_ADDR r14[REG_STKS_LEN];                      \
        CORE_ADDR r15[REG_STKS_LEN];                      \
                                                          \
        CORE_ADDR rip[reg_stks_len];                      \
        CORE_ADDR rsp[reg_stks_len];                      \
        CORE_ADDR rbp[reg_stks_len];                      \
        size_t len[reg_stks_len];                         \
        CORE_ADDR adr[reg_stks_len];                      \
        CORE_ADDR stk[reg_stks_len][stk_len];             \
        uint8_t loc[reg_stks_len][18];                    \
    } type_name

#define CACHE_REG_STK(rss, offset, stk_len, more_rbp, more_rsp)                               \
    do                                                                                        \
    {                                                                                         \
        asm volatile(                                                                         \
            "movq 0(%%rip), %0\n"                                                             \
            "movq %%rsp, %1\n"                                                                \
            "movq %%rbp, %2\n"                                                                \
                                                                                              \
            "movq %%rax, %3\n"                                                                \
            "movq %%rbx, %4\n"                                                                \
            "movq %%rcx, %5\n"                                                                \
            "movq %%rdx, %6\n"                                                                \
                                                                                              \
            "movq %%rsi, %7\n"                                                                \
            "movq %%rdi, %8\n"                                                                \
                                                                                              \
            "movq %%r8, %9\n"                                                                 \
            "movq %%r9, %10\n"                                                                \
            "movq %%r10, %11\n"                                                               \
            "movq %%r11, %12\n"                                                               \
            "movq %%r12, %13\n"                                                               \
            "movq %%r13, %14\n"                                                               \
                                                                                              \
            : "=r"(rss->rip[offset]),                                                         \
              "=r"(rss->rsp[offset]),                                                         \
              "=r"(rss->rbp[offset]),                                                         \
                                                                                              \
              "=r"(rss->rax[offset]),                                                         \
              "=r"(rss->rbx[offset]),                                                         \
              "=r"(rss->rcx[offset]),                                                         \
              "=r"(rss->rdx[offset]),                                                         \
                                                                                              \
              "=r"(rss->rsi[offset]),                                                         \
              "=r"(rss->rdi[offset]),                                                         \
                                                                                              \
              "=r"(rss->r8[offset]),                                                          \
              "=r"(rss->r9[offset]),                                                          \
              "=r"(rss->r10[offset]),                                                         \
              "=r"(rss->r11[offset]),                                                         \
              "=r"(rss->r12[offset]),                                                         \
              "=r"(rss->r13[offset])                                                          \
            :                                                                                 \
            :);                                                                               \
        CORE_ADDR rbp = rss->rbp[offset];                                                     \
        if (rbp < rss->rsp[offset])                                                           \
            rbp = rss->rsp[offset];                                                           \
        rbp = rbp + more_rbp;                                                                 \
        CORE_ADDR rsp = rss->rsp[offset] - more_rsp;                                          \
        size_t size = stk_len;                                                                \
        if (rbp - rsp < stk_len)                                                              \
            size = rbp - rsp;                                                                 \
        rss->adr[offset] = rsp;                                                               \
        rss->len[offset] = size;                                                              \
        memcpy((void *)rss->stk[offset], (void *)rss->adr[offset], size * sizeof(CORE_ADDR)); \
        snprintf(rss->loc[offset], sizeof(rss->loc[offset]), "%d@%s", __LINE__, __FILE__);    \
        offset++;                                                                             \
    } while (0)

void cache_reg_stk(reg_stks *rss, size_t offset, size_t stk_len, size_t more_rbp, size_t more_rsp)
{
    do
    {
        asm volatile(
            "movq 0(%%rip), %0\n"
            "movq %%rsp, %1\n"
            "movq %%rbp, %2\n"

            "movq %%rax, %3\n"
            "movq %%rbx, %4\n"
            "movq %%rcx, %5\n"
            "movq %%rdx, %6\n"

            "movq %%rsi, %7\n"
            "movq %%rdi, %8\n"

            "movq %%r8, %9\n"
            "movq %%r9, %10\n"
            "movq %%r10, %11\n"
            "movq %%r11, %12\n"
            "movq %%r12, %13\n"
            // "movq %%r13, %14\n"
            // "movq %%r14, %15\n"
            // "movq %%r15, %16\n"

            : "=r"(rss->rip[offset]),
              "=r"(rss->rsp[offset]),
              "=r"(rss->rbp[offset]),

              "=r"(rss->rax[offset]),
              "=r"(rss->rbx[offset]),
              "=r"(rss->rcx[offset]),
              "=r"(rss->rdx[offset]),

              "=r"(rss->rsi[offset]),
              "=r"(rss->rdi[offset]),

              "=r"(rss->r8[offset]),
              "=r"(rss->r9[offset]),
              "=r"(rss->r10[offset]),
              "=r"(rss->r11[offset]),
              "=r"(rss->r12[offset])
            //   "=r"(rss->r13[offset])
            //   "=r"(rss->r14[offset]), // error: ‘asm’ operand has impossible constraints
            //   "=r"(rss->r15[offset])
            :
            :);
        CORE_ADDR rbp = rss->rbp[offset];
        if (rbp < rss->rsp[offset])
            rbp = rss->rsp[offset];
        rbp = rbp + more_rbp;
        CORE_ADDR rsp = rss->rsp[offset] - more_rsp;
        if (rbp - rsp < stk_len)
            stk_len = rbp - rsp;
        rss->adr[offset] = rsp;
        rss->len[offset] = stk_len;
        memcpy((void *)rss->stk[offset], (void *)rss->adr[offset], stk_len * sizeof(CORE_ADDR));
        snprintf(rss->loc[offset], sizeof(rss->loc[offset]), "%s#%d", __FILE__, __LINE__);
        // offset++;
    } while (0);
}

// printf("register_name:");                                                \
// 0x0000000000000000
// 12345678901234567890
// printf("%19c", '|');
#define PRINT_REG_(register_name, len)                                       \
    for (int i = 0; i < len; i++)                                            \
    {                                                                        \
        if (first || preval != rss->register_name[i])                        \
        {                                                                    \
            print_bytes(&(rss->register_name[i]), sizeof(CORE_ADDR), false); \
            printf("|");                                                     \
            first = false;                                                   \
            preval = rss->register_name[i];                                  \
        }                                                                    \
        else                                                                 \
        {                                                                    \
            printf("%19c", '|');                                             \
        }                                                                    \
    }                                                                        \
    printf("\n");                                                            \
    first = true

#define CORE_ADDR_ALIGN 8

void print_reg_stks(reg_stks *rss, size_t len)
{
    if (rss == NULL || len == 0)
    {
        printf("reg_stks is NULL");
        return;
    }
    printf("%19c", ' ');
    for (int i = 0; i < len; i++)
    {
        printf("%18s ", rss->loc[i]);
    }
    printf("\nreg:\n");
    bool first = true;
    CORE_ADDR preval;

    printf("%19s", "rax:");
    PRINT_REG_(rax, len);
    printf("%19s", "rbx:");
    PRINT_REG_(rbx, len);
    printf("%19s", "rcx:");
    PRINT_REG_(rcx, len);
    printf("%19s", "rdx:");
    PRINT_REG_(rdx, len);

    printf("%19s", "rsi:");
    PRINT_REG_(rsi, len);
    printf("%19s", "rdi:");
    PRINT_REG_(rdi, len);

    printf("%19s", "r8 :");
    PRINT_REG_(r8, len);
    printf("%19s", "r9 :");
    PRINT_REG_(r9, len);
    printf("%19s", "r10:");
    PRINT_REG_(r10, len);
    printf("%19s", "r11:");
    PRINT_REG_(r11, len);
    printf("%19s", "r12:");
    PRINT_REG_(r12, len);
    printf("%19s", "r13:");
    PRINT_REG_(r13, len);
    printf("%19s", "r14:");
    PRINT_REG_(r14, len);
    printf("%19s", "r15:");
    PRINT_REG_(r15, len);
    printf("\n");
    printf("%19s", "rip:");
    PRINT_REG_(rip, len);
    printf("%19s", "rsp:");
    PRINT_REG_(rsp, len);
    printf("%19s", "rbp:");
    PRINT_REG_(rbp, len);
    // print stack
    CORE_ADDR min_adr = CORE_ADDR_MAX, max_adr = 0;
    for (int i = 0; i < len; i++)
    {
        if (rss->adr[i] < min_adr)
        {
            min_adr = rss->adr[i];
        }
        CORE_ADDR max = rss->adr[i] + rss->len[i] * CORE_ADDR_ALIGN;
        if (max > max_adr)
        {
            max_adr = max;
        }
    }
    size_t len4adr = (max_adr - min_adr) / CORE_ADDR_ALIGN + 1;
    CORE_ADDR stk[len][len4adr]; // = {0x00};
                                 // memset(stk, 0, sizeof(stk));
    memset(stk[0], 0, sizeof(stk[0]));
    for (int i = 0; i < len; i++)
    {
        // init
        if (i > 0)
        {
            for (int j = 0; j < len4adr; j++)
            {
                stk[i][j] = stk[i - 1][j];
            }
        }
        for (int j = 0; j < rss->len[i]; j++)
        {
            CORE_ADDR adr = rss->adr[i] + CORE_ADDR_ALIGN * j;
            size_t j_ = (adr - min_adr) / CORE_ADDR_ALIGN;
            stk[i][j_] = rss->stk[i][j];
        }
    }
    printf("stk:\n");
    for (int j = 0; j < len4adr; j++)
    {
        CORE_ADDR adr = min_adr + CORE_ADDR_ALIGN * j;
        print_bytes(&adr, sizeof(CORE_ADDR), false);
        printf("|");
        for (int i = 0; i < len; i++)
        {
            uint8_t delimiter = '|';
            if (adr == rss->rsp[i])
                delimiter = '+';
            else if (adr == rss->rbp[i])
                delimiter = '=';
            if (i > 0 && stk[i][j] == stk[i - 1][j])
            {
                printf("%19c", delimiter);
            }
            else
            {
                print_bytes(&stk[i][j], sizeof(CORE_ADDR), false);
                printf("%c", delimiter);
            }
        }
        printf("\n");
    }
}

#endif // __CSAPP2_COMM_H__
