#include <stdio.h>

/* Rearrange two vectors so that for each i, b[i] >= a[i] */
void minmax1(long a[], long b[], long n)
{
    long i;
    for (i = 0; i < n; i++)
    {
        if (a[i] > b[i])
        {
            long t = a[i];
            a[i] = b[i];
            b[i] = t;
        }
    }
}

/* Rearrange two vectors so that for each i, b[i] >= a[i] */
void minmax2(long a[], long b[], long n)
{
    long i;
    for (i = 0; i < n; i++)
    {
        long min = a[i] < b[i] ? a[i] : b[i];
        long max = a[i] < b[i] ? b[i] : a[i];
        a[i] = min;
        b[i] = max;
    }
}

void merge(long src1[], long src2[], long dest[], long n)
{
    long i1 = 0;
    long i2 = 0;
    long id = 0;
    while (i1 < n && i2 < n)
    {
        if (src1[i1] < src2[i2])
            dest[id++] = src1[i1++];
        else
            dest[id++] = src2[i2++];
    }
    // while (i1 < n)
    //     dest[id++] = src1[i1++];
    // while (i2 < n)
    //     dest[id++] = src2[i2++];
}

void merge2(long src1[], long src2[], long dest[], long n)
{
    long i1 = 0;
    long i2 = 0;
    long id = 0;
    while (i1 < n && i2 < n)
    {
        // if (src1[i1] < src2[i2])
        //     dest[id++] = src1[i1++];
        // else
        //     dest[id++] = src2[i2++];

        dest[id++] = src1[i1] < src2[i2] ? src1[i1++] : src2[i2++];

        // long v1 = src1[i1];
        // long v2 = src2[i2];
        // long take1 = v1 < v2;
        // dest[id++] = take1 ? v1 : v2;
        // i1 += take1;
        // i2 += (1 - take1);
    }
    // while (i1 < n)
    //     dest[id++] = src1[i1++];
    // while (i2 < n)
    //     dest[id++] = src2[i2++];
}

void merge3(long src1[], long src2[], long dest[], long n)
{
    long i1 = 0;
    long i2 = 0;
    long id = 0;
    while (i1 < n && i2 < n)
    {
        // if (src1[i1] < src2[i2])
        //     dest[id++] = src1[i1++];
        // else
        //     dest[id++] = src2[i2++];

        // dest[id++] = src1[i1] < src2[i2] ? src1[i1++] : src2[i2++];

        long v1 = src1[i1];
        long v2 = src2[i2];
        long take1 = v1 < v2;
        dest[id++] = take1 ? v1 : v2;
        i1 += take1;
        i2 += (1 - take1);
    }
    // while (i1 < n)
    //     dest[id++] = src1[i1++];
    // while (i2 < n)
    //     dest[id++] = src2[i2++];
}

void print_array(char *name, long *arr, long limit, long len)
{

    printf("%s@%p[%ld]={", name, arr, len);
    long i = 0;
    for (; i < limit && i < len; i++)
    {
        printf("\t");
        printf("%ld", arr[i]);
    }
    if (i < len)
        printf("...");
    printf("}\n");
}

#include <stdint.h>

#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}
// https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
#define xstr(s) str(s)
#define str(s) #s

#include <stdlib.h> // atol
#include <string.h> // memcpy
#include <time.h>

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }

    long len = 0;
    if (argc > 1)
    {
        len = atol(argv[1]);
    }
    if (len < 1)
        len = 1024;

    printf("len:%ld\n", len);

    time_t t;
    srand((unsigned)time(&t));
    long a[len], b[len];
    for (long i = 0; i < len; i++)
    {
        a[i] = rand() % len;
        b[i] = rand() % len;
    }
    print_array("a", a, 8, len);
    print_array("b", b, 8, len);
    uint64_t tsc0, tsc1;

    long a2[len], b2[len];
    // printf("sizeof(a):%ld\n", sizeof(a));
    memcpy(a2, a, sizeof(a));
    memcpy(b2, b, sizeof(b));
    tsc0 = perf_counter();
    minmax1(a2, b2, len);
    tsc1 = perf_counter();
    printf("minmax1 cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / len, tsc1 - tsc0);

    long a3[len], b3[len];
    memcpy(a3, a, sizeof(a));
    memcpy(b3, b, sizeof(b));
    tsc0 = perf_counter();
    minmax2(a3, b3, len);
    tsc1 = perf_counter();
    printf("minmax2 cycle:%lf,\t\t%lu\n", (double)(tsc1 - tsc0) / len, tsc1 - tsc0);

    print_array("a2", a2, 8, len);
    print_array(str(b2), b2, 8, len);
    print_array(str(a3), a3, 8, len);
    print_array("b3", b3, 8, len);
    for (long i = 0; i < len; i++)
    {
        if (a2[i] != a3[i])
        {
            printf("err: [%ld]a2!=a3, %ld!=%ld\n", i, a2[i], a3[i]);
        }
        if (b2[i] != b3[i])
        {
            printf("err: [%ld]b2!=b3, %ld!=%ld\n", i, b2[i], b3[i]);
        }
    }

    for (long i = 0; i < len; i++)
    {
        a[i] = i * 2;
        b[i] = i * 2 + 1;
    }
    print_array(str(a), a, 8, len);
    print_array(str(b), b, 8, len);

    long dest[len * 2];
    long a4[len], b4[len];
    memcpy(a4, a, sizeof(a));
    memcpy(b4, b, sizeof(b));
    tsc0 = perf_counter();
    merge(a4, b4, dest, len);
    tsc1 = perf_counter();
    printf("merge cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / len, tsc1 - tsc0);

    long dest2[len * 2];
    long a5[len], b5[len];
    memcpy(a5, a, sizeof(a));
    memcpy(b5, b, sizeof(b));
    tsc0 = perf_counter();
    merge2(a5, b5, dest2, len);
    tsc1 = perf_counter();
    printf("merge2 cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / len, tsc1 - tsc0);

    long dest3[len * 2];
    long a6[len], b6[len];
    memcpy(a6, a, sizeof(a));
    memcpy(b6, b, sizeof(b));
    tsc0 = perf_counter();
    merge3(a6, b6, dest3, len);
    tsc1 = perf_counter();
    printf("merge3 cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / len, tsc1 - tsc0);

    print_array(str(dest), dest, 8, len);
    print_array(str(dest2), dest2, 8, len);
    print_array(str(dest3), dest3, 8, len);
    for (long i = 0; i < len * 2; i++)
    {
        if (dest[i] != dest2[i])
        {
            printf("err: [%ld]dest!=dest2, %ld!=%ld\n", i, dest[i], dest2[i]);
        }
        if (dest[i] != dest3[i])
        {
            printf("err: [%ld]dest!=dest3, %ld!=%ld\n", i, dest[i], dest3[i]);
        }
    }
    return 0;
}
