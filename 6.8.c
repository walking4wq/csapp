#include <stdio.h>

#include <stdint.h>
#include <x86intrin.h>
// https://blog.csdn.net/ithiker/article/details/119981737
__inline__ uint64_t perf_counter(void)
{
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");
    uint64_t r = __rdtsc();
    __asm__ __volatile__(""
                         :
                         :
                         : "memory");

    return r;
    // return __rdtsc();
}

// Locality

#define N 1000

typedef struct
{
    int vel[3];
    int acc[3];
} point;

/*
https://www.cyberciti.biz/faq/linux-check-the-size-of-pagesize/

$ getconf PAGESIZE
4096
        bytes

https://superuser.com/questions/422272/how-to-change-the-memory-page-size
*/
point p[N];

void clear1(point *p, int n)
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < 3; j++)
            p[i].vel[j] = 0;
        for (j = 0; j < 3; j++)
            p[i].acc[j] = 0;
    }
}

void clear2(point *p, int n)
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < 3; j++)
        {
            p[i].vel[j] = 0;
            p[i].acc[j] = 0;
        }
    }
}

void clear3(point *p, int n)
{
    int i, j;
    for (j = 0; j < 3; j++)
    {
        for (i = 0; i < n; i++)
            p[i].vel[j] = 0;
        for (i = 0; i < n; i++)
            p[i].acc[j] = 0;
    }
}
/*
0000000000001220 <clear1>:
    1220:       f3 0f 1e fa             endbr64 
    1224:       85 f6                   test   %esi,%esi                    # if n <= 0 then ret
    1226:       7e 42                   jle    126a <clear1+0x4a>
    1228:       8d 46 ff                lea    -0x1(%rsi),%eax              # %ax = n - 1
    122b:       48 8d 04 40             lea    (%rax,%rax,2),%rax           # %rax = 3(n-1)
    122f:       48 8d 44 c7 18          lea    0x18(%rdi,%rax,8),%rax       # %rax = p+24+8*3*(n-1)
    1234:       0f 1f 40 00             nopl   0x0(%rax)
    1238:       c7 07 00 00 00 00       movl   $0x0,(%rdi)                  # set 0 by 4byte * 6times
    123e:       48 83 c7 18             add    $0x18,%rdi                   # 3*4*2 = 24 = 0x18
    1242:       c7 47 ec 00 00 00 00    movl   $0x0,-0x14(%rdi)
    1249:       c7 47 f0 00 00 00 00    movl   $0x0,-0x10(%rdi)
    1250:       c7 47 f4 00 00 00 00    movl   $0x0,-0xc(%rdi)
    1257:       c7 47 f8 00 00 00 00    movl   $0x0,-0x8(%rdi)
    125e:       c7 47 fc 00 00 00 00    movl   $0x0,-0x4(%rdi)
    1265:       48 39 f8                cmp    %rdi,%rax
    1268:       75 ce                   jne    1238 <clear1+0x18>
    126a:       c3                      ret    
    126b:       0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)

0000000000001270 <clear2>:
    1270:       f3 0f 1e fa             endbr64 
    1274:       85 f6                   test   %esi,%esi
    1276:       7e 29                   jle    12a1 <clear2+0x31>
    1278:       8d 46 ff                lea    -0x1(%rsi),%eax              # %ax = n - 1
    127b:       48 8d 04 40             lea    (%rax,%rax,2),%rax
    127f:       48 8d 44 c7 18          lea    0x18(%rdi,%rax,8),%rax       # %rax = p+24+8*3*(n-1) = p+24*n
    1284:       0f 1f 40 00             nopl   0x0(%rax)
    1288:       66 0f ef c0             pxor   %xmm0,%xmm0
    128c:       48 c7 47 10 00 00 00    movq   $0x0,0x10(%rdi)              # movq 8byte
    1293:       00 
    1294:       48 83 c7 18             add    $0x18,%rdi
    1298:       0f 11 47 e8             movups %xmm0,-0x18(%rdi)            # movups 128bit=16byte, 8+16=24
    129c:       48 39 f8                cmp    %rdi,%rax
    129f:       75 e7                   jne    1288 <clear2+0x18>
    12a1:       c3                      ret    
    12a2:       66 66 2e 0f 1f 84 00    data16 cs nopw 0x0(%rax,%rax,1)
    12a9:       00 00 00 00 
    12ad:       0f 1f 00                nopl   (%rax)

00000000000012b0 <clear3>:
    12b0:       f3 0f 1e fa             endbr64 
    12b4:       8d 46 ff                lea    -0x1(%rsi),%eax              # %ax = n - 1
    12b7:       48 8d 4f 0c             lea    0xc(%rdi),%rcx               # %rcx = p + 12
    12bb:       48 8d 04 40             lea    (%rax,%rax,2),%rax
    12bf:       48 8d 54 c7 18          lea    0x18(%rdi,%rax,8),%rdx       # %rdx = p+24*n
    12c4:       48 89 f8                mov    %rdi,%rax                    # %rax = p
    12c7:       85 f6                   test   %esi,%esi                    # if n <= 0 then ret
    12c9:       7e 2d                   jle    12f8 <clear3+0x48>
    12cb:       0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)
    12d0:       c7 00 00 00 00 00       movl   $0x0,(%rax)                  # set 0 to p.vel[0]
    12d6:       48 83 c0 18             add    $0x18,%rax                   # %rax = p + 24
    12da:       48 39 d0                cmp    %rdx,%rax
    12dd:       75 f1                   jne    12d0 <clear3+0x20>
    12df:       48 89 f8                mov    %rdi,%rax
    12e2:       66 0f 1f 44 00 00       nopw   0x0(%rax,%rax,1)
    12e8:       c7 40 0c 00 00 00 00    movl   $0x0,0xc(%rax)               # set 0 to p.acc[0]
    12ef:       48 83 c0 18             add    $0x18,%rax
    12f3:       48 39 d0                cmp    %rdx,%rax
    12f6:       75 f0                   jne    12e8 <clear3+0x38>
    12f8:       48 83 c7 04             add    $0x4,%rdi                    # j++
    12fc:       48 83 c2 04             add    $0x4,%rdx                    # end of j
    1300:       48 39 cf                cmp    %rcx,%rdi
    1303:       75 bf                   jne    12c4 <clear3+0x14>
    1305:       c3                      ret    
    1306:       66 2e 0f 1f 84 00 00    cs nopw 0x0(%rax,%rax,1)
    130d:       00 00 00 

*/
void test_locality()
{
    uint64_t tsc0, tsc1;
    tsc0 = perf_counter();
    clear1(p, N);
    tsc1 = perf_counter();
    printf("after clear1 used cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / N, tsc1 - tsc0);
    tsc0 = perf_counter();
    clear2(p, N);
    tsc1 = perf_counter();
    printf("after clear2 used cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / N, tsc1 - tsc0);
    tsc0 = perf_counter();
    clear3(p, N);
    tsc1 = perf_counter();
    printf("after clear3 used cycle:%lf,\t%lu\n", (double)(tsc1 - tsc0) / N, tsc1 - tsc0);
}

#include <unistd.h> // getpagesize()

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d,>[%s]\n", i, argv[i]);
    }
    long sz;
    sz = getpagesize();
    sz = sysconf(_SC_PAGESIZE);
    printf("Page size:%ld bytes\n", sz);

    test_locality();
    return 0;
}
