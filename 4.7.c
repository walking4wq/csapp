#include <stdio.h>

/*
.text
.globl pushtest
pushtest:
    movq    %rsp, %rax
    pushq   %rsp
    popq    %rdx
    subq    %rdx, %rax
    ret

ref https://blog.csdn.net/qq_40695642/article/details/104425893

gcc -O1 -S 4.7.c -o pushtest.s
# -O2 will inline the func

as -o pushtest.o pushtest.s

ld -s -o pushtest pushtest.o

ld: warning: cannot find entry symbol _start; defaulting to 0000000000401000
ld: pushtest.o: in function `main':
4.7.c:(.text.startup+0x1c): undefined reference to `puts'
ld: 4.7.c:(.text.startup+0x45): undefined reference to `__printf_chk'
ld: 4.7.c:(.text.startup+0x5f): undefined reference to `__printf_chk'

ref https://blog.csdn.net/cecurio/article/details/112652609
gcc -v pushtest.o -o pushtest

is OK!

https://blog.csdn.net/qq_52300431/article/details/127397862
*/
long pushtest()
{
    long rst = 1;
    asm volatile(
        "movq   %%rsp, %%rax\n"
        "pushq  %%rsp\n"
        "popq   %%rdx\n"
        "subq   %%rdx, %%rax\n"
        "movq   %%rax, %0\n"
        : "=r"(rst)
        : // "r"(x), "r"(y)
        : "%rsp", "%rax", "%rdx");
    return rst;
}

/*
.text
.globl poptest
poptest
    movq    %rsp, %rdi
    pushq   $0xabcd
    popq    %rsp
    movq    %rsp, %rax
    movq    %rdi, %rsp
    ret
*/
long poptest()
{
    long rst = 0;
    asm volatile(
        "movq   %%rsp, %%rdi\n"
        "pushq  $0xabcd\n"
        "popq   %%rsp\n"
        "movq   %%rsp, %%rax\n"
        "movq   %%rdi, %%rsp\n"
        "movq   %%rax, %0\n"
        : "=r"(rst)
        : // "r"(x), "r"(y)
        : "%rsp", "%rax", "%rdx");
    return rst;
}

int main(int argc, char *argv[])
{
    printf("Hello, World!\n");
    for (int i = 0; i < argc; i++)
    {
        printf("%d\t>[%s]\n", i, argv[i]);
    }
    long rst;
    rst = pushtest();
    printf("pushtest return:%ld=0x%.2lx\n", rst, rst);
    rst = poptest();
    printf("pushtest return:%ld=0x%.2lx\n", rst, rst);
}
