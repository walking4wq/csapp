#include "csapp.h"

#include <string.h>

static void cli(char *host, char *port);
static void svr(char *port, char multiplex);
int main(int argc, char **argv)
{
    // switch (argc)
    // {
    // case 2:
    //     svr(argv[1]);
    //     break;
    // case 3:
    //     cli(argv[1], argv[2]);
    //     break;
    // default:
    //     fprintf(stderr, "\tusage:\nsvr: %s <port>\ncli: %s <host> <port>\n", argv[0], argv[0]);
    //     // break;
    // }
    if (argc != 4)
    {
        fprintf(stderr, "\tusage:\n %s svr [*|m|s|p] <port>\ncli: %s cli <host> <port>\n", argv[0], argv[0]);
        return 1;
    }
    if (strcmp(argv[1], "svr") == 0)
    {
        if (strlen(argv[2]) == 1)
            svr(argv[3], argv[2][0]);
        else
            svr(argv[3], 0);
    }
    else
    {
        cli(argv[2], argv[3]);
    }
    return 0;
}

static void cli(char *host, char *port)
{
    int clientfd;
    char buf[MAXLINE];
    rio_t rio;

    clientfd = Open_clientfd(host, port);
    Rio_readinitb(&rio, clientfd);

    printf(" >");
    while (Fgets(buf, MAXLINE, stdin) != NULL)
    {
        Rio_writen(clientfd, buf, strlen(buf));
        Rio_readlineb(&rio, buf, MAXLINE);
        printf(" <");
        Fputs(buf, stdout);
        printf(" >");
    }
    Close(clientfd);
    exit(0);
}

void sigchld_handler(int sig)
{
    while (waitpid(-1, 0, WNOHANG) > 0)
        ;
    return;
}

void print_fd_set(char *name, fd_set *fdset)
{
    printf("%s:", name);
    if (!fdset)
    {
        printf("NULL\n");
    }
    else
    {
        for (int i = 0; i < sizeof(fd_set) / sizeof(__fd_mask); i++)
            if (FD_ISSET(i, fdset))
                printf("1");
            else
                printf("0");
        printf("\n");
    }
}

typedef struct
{ /* Represents a pool of connected descriptors */
    int maxfd;
    /* Largest descriptor in read_set */
    fd_set read_set;  /* Set of all active descriptors */
    fd_set ready_set; /* Subset of descriptors ready for reading */
    int nready;
    /* Number of ready descriptors from select */
    int maxi;
    /* High water index into client array */
    int clientfd[FD_SETSIZE];
    /* Set of active descriptors */
    rio_t clientrio[FD_SETSIZE]; /* Set of active read buffers */
} pool;

void print_pool(pool *pool);
// int byte_cnt;
void init_pool(int listenfd, pool *p);
void add_client(int connfd, pool *p);
void check_clients(pool *p);

void echo(int connfd);
void command(void);

void *thread(void *vargp);

static void svr(char *port, char multiplex)
{
    int listenfd, connfd, *connfdp;
    socklen_t clientlen;
    struct sockaddr_storage clientaddr; /* Enough space for any address */
    char client_hostname[MAXLINE], client_port[MAXLINE];
    fd_set read_set, ready_set;

    static pool pool;

    if (multiplex == 'm')
        Signal(SIGCHLD, sigchld_handler);
    listenfd = Open_listenfd(port);

    FD_ZERO(&read_set);              // Clear read set
    FD_SET(STDIN_FILENO, &read_set); // Add to  read set
    FD_SET(listenfd, &read_set);     // Add to  read set

    print_fd_set("read_set", &read_set);

    init_pool(listenfd, &pool);
    printf("after init=");
    print_pool(&pool);

    pthread_t tid;

    while (1)
    {
        if (multiplex == 's')
        {
            /// i/o multiplexing version for select ///
            ready_set = read_set;
            Select(listenfd + 1, &ready_set, NULL, NULL, NULL);
            print_fd_set("ready_set", &ready_set);
            if (FD_ISSET(STDIN_FILENO, &ready_set))
                command(); // Read command line from stdin
            if (FD_ISSET(listenfd, &ready_set))
            {
                clientlen = sizeof(struct sockaddr_storage);
                connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
                Getnameinfo((SA *)&clientaddr, clientlen, client_hostname, MAXLINE,
                            client_port, MAXLINE, 0);
                printf("Select connected to (%s, %s)\n", client_hostname, client_port);
                echo(connfd);
                Close(connfd);
                printf("Select disconnected from (%s, %s)\n", client_hostname, client_port);
            }
        }
        else if (multiplex == 'p')
        {
            /* Wait for listening/connected descriptor(s) to become ready */
            pool.ready_set = pool.read_set;
            pool.nready = Select(pool.maxfd + 1, &pool.ready_set, NULL, NULL, NULL);

            printf("after select=");
            print_pool(&pool);

            /* If listening descriptor ready, add new client to pool */
            if (FD_ISSET(listenfd, &pool.ready_set))
            {
                clientlen = sizeof(struct sockaddr_storage);
                connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
                add_client(connfd, &pool);
                printf("after accept new client=");
                print_pool(&pool);
            }

            /* Echo a text line from each ready connected descriptor */
            check_clients(&pool);
            printf("after check_clients response=");
            print_pool(&pool);
        }
        else if (multiplex == 't')
        {
            clientlen = sizeof(struct sockaddr_storage);
            connfdp = Malloc(sizeof(int));
            *connfdp = Accept(listenfd, (SA *)&clientaddr, &clientlen);
            printf("after accept new client connfd:%d\n", *connfdp);
            Pthread_create(&tid, NULL, thread, connfdp);
        }
        else
        {
            clientlen = sizeof(struct sockaddr_storage);
            connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
            if (multiplex == 'm')
            {
                /// multi-process version ///
                if (Fork() == 0)
                {
                    Close(listenfd); // Child closes its listening socket
                    Getnameinfo((SA *)&clientaddr, clientlen, client_hostname, MAXLINE,
                                client_port, MAXLINE, 0);
                    printf("Multi-connected to (%s, %s)\n", client_hostname, client_port);
                    echo(connfd);  // Child services client
                    Close(connfd); // Child closes connection with client
                    printf("Multi-disconnected from (%s, %s)\n", client_hostname, client_port);
                    exit(0); // Child exits
                }
                Close(connfd); // Parent closes connected socket (important!)
            }
            else
            {
                /// singel process version ///
                Getnameinfo((SA *)&clientaddr, clientlen, client_hostname, MAXLINE,
                            client_port, MAXLINE, 0);
                printf("Connected to (%s, %s)\n", client_hostname, client_port);
                echo(connfd);
                Close(connfd);
                printf("Disconnected from (%s, %s)\n", client_hostname, client_port);
            }
        }
    }
    exit(0);
}
void echo(int connfd)
{
    size_t n;
    char buf[MAXLINE];
    rio_t rio;
    Rio_readinitb(&rio, connfd);
    while ((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)
    {
        printf("server received %d bytes:%s\n", (int)n, buf);
        Rio_writen(connfd, buf, n);
    }
}
void command()
{
    char buf[MAXLINE];
    if (!Fgets(buf, MAXLINE, stdin))
        exit(0);          // EOF
    printf("<%s\n", buf); // Process the input command
}

void print_pool(pool *pool)
{
    printf("\"pool\":");
    if (!pool)
    {
        printf("NULL\n");
    }
    else
    {
        printf("{\"maxfd\":%d", pool->maxfd);
        print_fd_set(",\n\"read_set\"", &pool->read_set);
        print_fd_set(",\"ready_set\"", &pool->ready_set);
        printf(",\"nready\":%d,\"maxi\":%d", pool->nready, pool->maxi);
        printf(",\n\"clientfd\":\"");
        for (int i = 0, j = 0; i < FD_SETSIZE; i++)
        {
            if (pool->clientfd[i] < 0)
                continue;
            if (j)
                printf(",");
            else
                j = 1;
            printf("%d:%d", i, pool->clientfd[i]);
        }
        printf("\",\n\"clientrio\":\"");
        for (int i = 0, j = 0; i < FD_SETSIZE; i++)
        {
            if (pool->clientfd[i] < 0)
                continue;
            if (j)
                printf(",");
            else
                j = 1;
            printf("%d:%d[%d]", i, pool->clientrio[i].rio_fd, pool->clientrio[i].rio_cnt);
        }
        printf("\"}\n");
    }
}

int byte_cnt = 0; /* Counts total bytes received by server */

void init_pool(int listenfd, pool *p)
{
    /* Initially, there are no connected descriptors */
    int i;
    p->maxi = -1;
    for (i = 0; i < FD_SETSIZE; i++)
        p->clientfd[i] = -1;

    /* Initially, listenfd is only member of select read set */
    p->maxfd = listenfd;
    FD_ZERO(&p->read_set);
    FD_SET(listenfd, &p->read_set);
}

void add_client(int connfd, pool *p)
{
    int i;
    p->nready--;
    for (i = 0; i < FD_SETSIZE; i++) /* Find an available slot */
        if (p->clientfd[i] < 0)
        {
            /* Add connected descriptor to the pool */
            p->clientfd[i] = connfd;
            Rio_readinitb(&p->clientrio[i], connfd);

            /* Add the descriptor to descriptor set */
            FD_SET(connfd, &p->read_set);

            /* Update max descriptor and pool high water mark */
            if (connfd > p->maxfd)
                p->maxfd = connfd;
            if (i > p->maxi)
                p->maxi = i;
            break;
        }
    if (i == FD_SETSIZE) /* Couldn’t find an empty slot */
        app_error("add_client error: Too many clients");
}

void check_clients(pool *p)
{
    int i, connfd, n;
    char buf[MAXLINE];
    rio_t rio;

    for (i = 0; (i <= p->maxi) && (p->nready > 0); i++)
    {
        connfd = p->clientfd[i];
        rio = p->clientrio[i];

        /* If the descriptor is ready, echo a text line from it */
        if ((connfd > 0) && (FD_ISSET(connfd, &p->ready_set)))
        {
            p->nready--;
            if ((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)
            {
                byte_cnt += n;
                printf("Server received %d (%d total) bytes on fd %d\n",
                       n, byte_cnt, connfd);
                Rio_writen(connfd, buf, n);
            }
            else /* EOF detected, remove descriptor from pool */
            {
                Close(connfd);
                FD_CLR(connfd, &p->read_set);
                p->clientfd[i] = -1;
            }
        }
    }
}

/* Thread routine */
void *thread(void *vargp)
{
    int connfd = *((int *)vargp);
    Pthread_detach(pthread_self());
    Free(vargp);
    echo(connfd);
    Close(connfd);
    return NULL;
}
