#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include "comm.h"

static reg_stk *rs = NULL;

static reg_stks rss_;
static reg_stks *rss = &rss_;
static size_t offset4rss = 0;

/* Sample implementation of library function gets() */
char *gets(char *s)
{
   // trace_register(rs);
   // // rs = trace_register2(rs);
   // cache4reg_stk(rs, 0x60, 0x20, 0x20);
   // CACHE_REG_STK(rss, offset4rss, STKS_LEN, 0x20, 0x20);
   int c;
   char *dest = s;
   int gotchar = 0; /* Has at least one character been read? */
   while ((c = getchar()) != '\n' && c != EOF)
   {
      *dest++ = c; /* No bounds checking! */
      gotchar = 1;
   }
   *dest++ = '\0'; /* Terminate string */
   if (c == EOF && !gotchar)
      return NULL; /* End of file or error */
   return s;
}

/* Read input line and write it back */
void echo()
{
   char buf[4]; /* Way too small! */ // 0x8/0x18/0x28
   // trace_register(rs);
   // // rs = trace_register2(rs);
   // cache4reg_stk(rs, 0x60, 0x20, 0x20);
   // CACHE_REG_STK(rss, offset4rss, STKS_LEN, 0x20, 0x20);
   gets(buf);
   // CACHE_REG_STK(rss, offset4rss, STKS_LEN, 0x20, 0x20);
   puts(buf);

   // string_bytes(u3, sizeof(*u3), buff);
}
/*
1   echo:
2   pushl   %ebp            // Save %ebp on stack
3   movl    %esp, %ebp
4   pushl   %ebx            // Save %ebx
5   subl    $20, %esp       // Allocate 20 bytes on stack
6   leal    -12(%ebp), %ebx // Compute buf as %ebp-12
7   movl    %ebx, (%esp)    // Store buf at top of stack
8   call    gets            // Call gets
9   movl    %ebx, (%esp)    // Store buf at top of stack
10  call    puts            // Call puts
11  addl    $20, %esp       // Deallocate stack space
12  popl    %ebx            // Restore %ebx
13  popl    %ebp            // Restore %ebp
14  ret                     // Return
*/

/*
This is very low-quality code.
It is intended to illustrate bad programming practices.
See Problem 3.43.

Procedure getline is called with the return address equal to 0x8048643,
 register %ebp equal to 0xbffffc94, register %ebx equal to 0x1,
 register %edi is equal to 0x2, and register %esi is equal to 0x3.
You type in the string "012345678901234567890123".
*/
char *getline_csapp()
{
   char buf[8];
   char *result;

   gets(buf);
   result = malloc(strlen(buf)); //(char *)
   strcpy(result, buf);
   return result;
}

/*
1  080485c0 <getline>:  
2  80485c0: 55             push  %ebp
3  80485c1: 89 e5          mov   %esp,%ebp
4  80485c3: 83 ec 28       sub   $0x28,%esp
5  80485c6: 89 5d f4       mov   %ebx,-0xc(%ebp)
6  80485c9: 89 75 f8       mov   %esi,-0x8(%ebp)
7  80485cc: 89 7d fc       mov   %edi,-0x4(%ebp)
 Diagram stack at this point
8  80485cf: 8d 75 ec       lea   -0x14(%ebp),%esi
9  80485d2: 89 34 24       mov   %esi,(%esp)
10 80485d5: e8 a3 ff ff ff call  804857d <gets>
 Modify diagram to show stack contents at this point

$28=$0x1c
%ebp:0xbffffc94
%ebx:0x1
%edi:0x2
%esi:0x3

return address:0x08048643

You type in the string "012345678901234567890123"
Attention: end of string is '\0'

'0' ascii is 0x30

%ebx:0x1       |                |
%edi:0x2       |                |
%esi:0x3       |                |

  00:          +                +0x      08+
  04:          |                |          |
  08:          |           %esi |0x33323130|
  0c:          |                |0x37363534|
  10:0x00000001| %ebx           |0x31303938|
  14:0x00000003| %esi           |0x35343332|
  18:0x00000002| %edi           |0x39383736|
  1c:0xbffffc94= Saved %ebp     |0x33323130|
  20:0x08048643| Return address |0x08048600|
  24:          |

* reg             :c/csapp/3.43.c@115|c/csapp/3.43.c@128|cc/csapp/3.43.c@78|cc/csapp/3.43.c@78|cc/csapp/3.43.c@78|cc/csapp/3.43.c@82|cc/csapp/3.43.c@83|cc/csapp/3.43.c@83|cc/csapp/3.43.c@83|cc/csapp/3.43.c@83|cc/csapp/3.43.c@83|cc/csapp/3.43.c@84|cc/csapp/3.43.c@85|cc/csapp/3.43.c@86|cc/csapp/3.43.c@86
$rax              :0x00005555555572f7|0x0000000000000000|                  |                  |0x1b5769d309465b00|0x00007fffffffdae0|                  |                  |0x0000000000000009|                  |0x000055555555bac0|                  |                  |                  |                  |
$rbx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$rcx              :0x0000555555559d68|0x00007ffff7f99a00|                  |                  |                  |                  |0x00007ffff7e98992|                  |                  |                  |0x0000000000000021|                  |0x3837363534333231|                  |                  |
$rdx              :0x00007fffffffdc48|0x0000000000000000|                  |                  |                  |                  |0x00007fffffffdaea|                  |0x00007fffffffdae0|                  |0x0000000000000000|0x00007fffffffdae0|0x0000000000000009|                  |0xffffffffffffa539|
$rsp              :0x00007fffffffdb00|                  |0x00007fffffffdaf8|0x00007fffffffdad0|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$rbp              :0x00007fffffffdb20|                  |                  |0x00007fffffffdaf0|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$rsi              :0x00007fffffffdc38|0x0000555555558168|                  |                  |                  |                  |0x000055555555b6b0|                  |                  |                  |0x000055555555bad0|0x00007fffffffdae0|                  |                  |                  |
$rdi              :0x0000000000000001|0x00007fffffffd440|                  |                  |                  |0x00007fffffffdae0|0x0000000000000000|0x00007fffffffdae0|                  |0x0000000000000009|0x0000000000000000|0x000055555555bac0|                  |                  |                  |
$rip              :0x000055555555730a|0x0000555555557386|0x000055555555728b|0x0000555555557297|0x00005555555572a4|0x00005555555572ad|0x00005555555572b2|0x00005555555572b9|0x00005555555572be|0x00005555555572c1|0x00005555555572c6|0x00005555555572d8|0x00005555555572dd|0x00005555555572e1|0x00005555555572ee|
$r8               :0x00007ffff7f9ef10|0x00007ffff7f9fa70|                  |                  |                  |                  |0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |
$r9               :0x00007ffff7fc9040|0x000055555555b6b0|                  |                  |                  |                  |                  |                  |                  |                  |0x000055555555bac0|                  |0x0039383736353433|                  |                  |
$r10              :0x00007ffff7fc3908|0x0000555555558168|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r11              :0x00007ffff7fde680|0x0000000000000246|                  |                  |                  |                  |                  |                  |                  |                  |0x00007ffff7f9dce0|                  |                  |                  |                  |
$r12              :0x00007fffffffdc38|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r13              :0x00005555555572f7|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r14              :0x0000555555559d68|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$eflags           :0x0000000000000206|0x0000000000000246|                  |0x0000000000000202|                  |0x0000000000000246|0x0000000000000213|                  |0x0000000000000202|                  |                  |                  |                  |                  |0x0000000000000287|
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
* stk                                                                                                                                                                                                                                                                                                           
0x00007fffffffdab0:0x0000000000000000|                  |                  |                  |                  |                  |0x0000000a00000001|                  |                  |                  |0x0000000000000000|                  |                  |                  |                  |
0x00007fffffffdab8:0x0000000000000000|                  |                  |0x1b5769d309465b00|                  |                  |0x00007fffffffdaea|                  |                  |                  |0x00007fffffffdaf0|                  |                  |                  |                  |
0x00007fffffffdac0:0x0000000000000000|                  |                  |0x00007fffffffdc38|                  |                  |0x00007fffffffdaf0|                  |                  |                  |0x00007fffffffdc38|                  |                  |                  |                  |
0x00007fffffffdac8:0x0000000000000000|                  |                  |                  |                  |                  |0x00005555555572b2|                  |0x00005555555572be|                  |0x00005555555572c6|                  |0x00005555555572dd|                  |                  |
0x00007fffffffdad0:0x0000000000000000|                  |                  |0x00007fffffffdc38+                  +                  +                  +                  +                  +                  +                  +                  +                  +                  +                  +
0x00007fffffffdad8:0x0000000000000000|                  |0x00005555555572f7|                  |                  |                  |                  |                  |                  |                  |                  |0x000055555555bac0|                  |                  |                  |
0x00007fffffffdae0:0x00007ffff7fc1000|0x0000555555559d68|                  |                  |                  |                  |0x3837363534333231|                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdae8:0x0000010101000000|0x00007ffff7ffd040|                  |                  |0x1b5769d309465b00|                  |0x1b5769d309460039|                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdaf0:0x0000000000000002|0x00007fffffffdb20|                  |                  =                  =                  =                  =                  =                  =                  =                  =                  =                  =                  =                  =
0x00007fffffffdaf8:0x00000000178bfbff|0x0000555555557381|0x000055555555738b+                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb00:0x00007fffffffdc38+                  +                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb08:0x0000000100000064|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb10:0x0000000000001000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb18:0x00005555555551c0|0x00000001555551c0|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb20:0x0000000000000001=                  =                  =                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb28:0x00007ffff7dadd90|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb30:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb38:0x00005555555572f7|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb40:0x00000001ffffdc20|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb48:0x00007fffffffdc38|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb50:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb58:0xc63b70ed1f8679c1|0x8d02b4e4d20be196|                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |                  |
gef➤  
[0] 0:[tmux]*Z                                                                                                                                                                                                                                                   "walking" 16:33 22-Feb-23


Dump of assembler code for function main:
3.43.c:
114     {
   0x00005555555572f7 <+0>:     endbr64 
   0x00005555555572fb <+4>:     push   %rbp
   0x00005555555572fc <+5>:     mov    %rsp,%rbp
   0x00005555555572ff <+8>:     sub    $0x20,%rsp
   0x0000555555557303 <+12>:    mov    %edi,-0x14(%rbp)
   0x0000555555557306 <+15>:    mov    %rsi,-0x20(%rbp)

115        printf("Hello, World!\n");
=> 0x000055555555730a <+19>:    lea    0xece(%rip),%rax        # 0x5555555581df
   0x0000555555557311 <+26>:    mov    %rax,%rdi
   0x0000555555557314 <+29>:    call   0x555555555120 <puts@plt>

116        for (int i = 0; i < argc; i++)
   0x0000555555557319 <+34>:    movl   $0x0,-0x4(%rbp)
   0x0000555555557320 <+41>:    jmp    0x555555557356 <main+95>

117        {
118           printf("%d\t>[%s]\n", i, argv[i]);
   0x0000555555557322 <+43>:    mov    -0x4(%rbp),%eax
   0x0000555555557325 <+46>:    cltq   
   0x0000555555557327 <+48>:    lea    0x0(,%rax,8),%rdx
   0x000055555555732f <+56>:    mov    -0x20(%rbp),%rax
   0x0000555555557333 <+60>:    add    %rdx,%rax
   0x0000555555557336 <+63>:    mov    (%rax),%rdx
   0x0000555555557339 <+66>:    mov    -0x4(%rbp),%eax
   0x000055555555733c <+69>:    mov    %eax,%esi
   0x000055555555733e <+71>:    lea    0xea8(%rip),%rax        # 0x5555555581ed
   0x0000555555557345 <+78>:    mov    %rax,%rdi
   0x0000555555557348 <+81>:    mov    $0x0,%eax
   0x000055555555734d <+86>:    call   0x555555555150 <printf@plt>

116        for (int i = 0; i < argc; i++)
   0x0000555555557352 <+91>:    addl   $0x1,-0x4(%rbp)
   0x0000555555557356 <+95>:    mov    -0x4(%rbp),%eax
   0x0000555555557359 <+98>:    cmp    -0x14(%rbp),%eax
   0x000055555555735c <+101>:   jl     0x555555557322 <main+43>

119        }
120
121        // trace_register(rs);
122        // // rs = trace_register2(rs);
123        // cache4reg_stk(rs, 0x60, 0x20, 0x20);
124        // CACHE_REG_STK(rss, offset4rss, STKS_LEN, 0x20, 0x20);
125        echo();
   0x000055555555735e <+103>:   mov    $0x0,%eax
   0x0000555555557363 <+108>:   call   0x555555557241 <echo>

126        // print_reg_stk(rs, 0);
127        print_reg_stks(rss, offset4rss);
   0x0000555555557368 <+113>:   mov    0x35e1(%rip),%rdx        # 0x55555555a950 <offset4rss>
   0x000055555555736f <+120>:   mov    0x2d3a(%rip),%rax        # 0x55555555a0b0 <rss>
   0x0000555555557376 <+127>:   mov    %rdx,%rsi
   0x0000555555557379 <+130>:   mov    %rax,%rdi
   0x000055555555737c <+133>:   call   0x555555555a0b <print_reg_stks>

128        printf("getline_csapp():\n[%s]\n", getline_csapp());
   0x0000555555557381 <+138>:   mov    $0x0,%eax
   0x0000555555557386 <+143>:   call   0x55555555728b <getline_csapp>
   0x000055555555738b <+148>:   mov    %rax,%rsi
   0x000055555555738e <+151>:   lea    0xe62(%rip),%rax        # 0x5555555581f7
   0x0000555555557395 <+158>:   mov    %rax,%rdi
   0x0000555555557398 <+161>:   mov    $0x0,%eax
   0x000055555555739d <+166>:   call   0x555555555150 <printf@plt>
   0x00005555555573a2 <+171>:   mov    $0x0,%eax

129     }
   0x00005555555573a7 <+176>:   leave  
   0x00005555555573a8 <+177>:   ret    
End of assembler dump.
(gdb) disassemble /s getline_csapp
Dump of assembler code for function getline_csapp:
3.43.c:
78      {
   0x000055555555728b <+0>:     endbr64 
   0x000055555555728f <+4>:     push   %rbp
   0x0000555555557290 <+5>:     mov    %rsp,%rbp
   0x0000555555557293 <+8>:     sub    $0x20,%rsp
   0x0000555555557297 <+12>:    mov    %fs:0x28,%rax
   0x00005555555572a0 <+21>:    mov    %rax,-0x8(%rbp)
   0x00005555555572a4 <+25>:    xor    %eax,%eax

79         char buf[8];
80         char *result;
81
82         gets(buf);
   0x00005555555572a6 <+27>:    lea    -0x10(%rbp),%rax
   0x00005555555572aa <+31>:    mov    %rax,%rdi
   0x00005555555572ad <+34>:    call   0x5555555571cc <gets>

83         result = malloc(strlen(buf)); //(char *)
   0x00005555555572b2 <+39>:    lea    -0x10(%rbp),%rax
   0x00005555555572b6 <+43>:    mov    %rax,%rdi
   0x00005555555572b9 <+46>:    call   0x555555555130 <strlen@plt>
   0x00005555555572be <+51>:    mov    %rax,%rdi
   0x00005555555572c1 <+54>:    call   0x5555555551a0 <malloc@plt>
   0x00005555555572c6 <+59>:    mov    %rax,-0x18(%rbp)

84         strcpy(result, buf);
   0x00005555555572ca <+63>:    lea    -0x10(%rbp),%rdx
   0x00005555555572ce <+67>:    mov    -0x18(%rbp),%rax
   0x00005555555572d2 <+71>:    mov    %rdx,%rsi
   0x00005555555572d5 <+74>:    mov    %rax,%rdi
   0x00005555555572d8 <+77>:    call   0x555555555110 <strcpy@plt>

85         return result;
   0x00005555555572dd <+82>:    mov    -0x18(%rbp),%rax

86      }
   0x00005555555572e1 <+86>:    mov    -0x8(%rbp),%rdx
   0x00005555555572e5 <+90>:    sub    %fs:0x28,%rdx
   0x00005555555572ee <+99>:    je     0x5555555572f5 <getline_csapp+106>
   0x00005555555572f0 <+101>:   call   0x555555555140 <__stack_chk_fail@plt>
   0x00005555555572f5 <+106>:   leave  
   0x00005555555572f6 <+107>:   ret    
End of assembler dump.

*/

int main(int argc, char *argv[])
{
   printf("Hello, World!\n");
   for (int i = 0; i < argc; i++)
   {
      printf("%d\t>[%s]\n", i, argv[i]);
   }

   // trace_register(rs);
   // // rs = trace_register2(rs);
   // cache4reg_stk(rs, 0x60, 0x20, 0x20);
   // CACHE_REG_STK(rss, offset4rss, STKS_LEN, 0x20, 0x20);
   echo();
   // print_reg_stk(rs, 0);
   print_reg_stks(rss, offset4rss);
   printf("getline_csapp():\n[%s]\n", getline_csapp());
}



/*
ref https://zhuanlan.zhihu.com/p/25816426

* reg             :c/csapp/3.43.c@115|c/csapp/3.43.c@128|cc/csapp/3.43.c@78|cc/csapp/3.43.c@78|cc/csapp/3.43.c@78|cc/csapp/3.43.c@82|cc/csapp/3.43.c@83|cc/csapp/3.43.c@83|
$rax              :0x00005555555572f7|0x0000000000000000|                  |                  |0x1b5769d309465b00|0x00007fffffffdae0|                  |                  |
$rbx              :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
$rcx              :0x0000555555559d68|0x00007ffff7f99a00|                  |                  |                  |                  |0x00007ffff7e98992|                  |
$rdx              :0x00007fffffffdc48|0x0000000000000000|                  |                  |                  |                  |0x00007fffffffdaea|                  |
$rsp              :0x00007fffffffdb00|                  |0x00007fffffffdaf8|0x00007fffffffdad0|                  |                  |                  |                  |
$rbp              :0x00007fffffffdb20|                  |                  |0x00007fffffffdaf0|                  |                  |                  |                  |
$rsi              :0x00007fffffffdc38|0x0000555555558168|                  |                  |                  |                  |0x000055555555b6b0|                  |
$rdi              :0x0000000000000001|0x00007fffffffd440|                  |                  |                  |0x00007fffffffdae0|0x0000000000000000|0x00007fffffffdae0|
$rip              :0x000055555555730a|0x0000555555557386|0x000055555555728b|0x0000555555557297|0x00005555555572a4|0x00005555555572ad|0x00005555555572b2|0x00005555555572b9|
$r8               :0x00007ffff7f9ef10|0x00007ffff7f9fa70|                  |                  |                  |                  |0x0000000000000000|                  |
$r9               :0x00007ffff7fc9040|0x000055555555b6b0|                  |                  |                  |                  |                  |                  |
$r10              :0x00007ffff7fc3908|0x0000555555558168|                  |                  |                  |                  |                  |                  |
$r11              :0x00007ffff7fde680|0x0000000000000246|                  |                  |                  |                  |                  |                  |
$r12              :0x00007fffffffdc38|                  |                  |                  |                  |                  |                  |                  |
$r13              :0x00005555555572f7|                  |                  |                  |                  |                  |                  |                  |
$r14              :0x0000555555559d68|                  |                  |                  |                  |                  |                  |                  |
$r15              :0x00007ffff7ffd040|                  |                  |                  |                  |                  |                  |                  |
$eflags           :0x0000000000000206|0x0000000000000246|                  |0x0000000000000202|                  |0x0000000000000246|0x0000000000000213|                  |
$cs               :0x0000000000000033|                  |                  |                  |                  |                  |                  |                  |
$ss               :0x000000000000002b|                  |                  |                  |                  |                  |                  |                  |
$ds               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
$es               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
$fs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
$gs               :0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
* stk                                 call               endbr64                                                                                                           
0x00007fffffffdab0:0x0000000000000000|                  |                  |                  |                  |                  |0x0000000a00000001|                  |
0x00007fffffffdab8:0x0000000000000000|                  |                  |0x1b5769d309465b00|                  |                  |0x00007fffffffdaea|                  |
0x00007fffffffdac0:0x0000000000000000|                  |                  |0x00007fffffffdc38|                  |                  |0x00007fffffffdaf0|                  |
0x00007fffffffdac8:0x0000000000000000|                  |                  |                  |                  |                  |0x00005555555572b2|                  |
0x00007fffffffdad0:0x0000000000000000|                  |                  |0x00007fffffffdc38+                  +                  +                  +                  +
0x00007fffffffdad8:0x0000000000000000|                  |0x00005555555572f7|                  |                  |                  |                  |                  |
0x00007fffffffdae0:0x00007ffff7fc1000|0x0000555555559d68|                  |                  |                  |                  |0x3837363534333231| Address for get  |
0x00007fffffffdae8:0x0000010101000000|0x00007ffff7ffd040|                  |                  |0x1b5769d309465b00| canary           |0x1b5769d309460039| stack overflow   |
0x00007fffffffdaf0:0x0000000000000002|0x00007fffffffdb20|                  | Saved %rbp       =                  =                  =                  =                  =
0x00007fffffffdaf8:0x00000000178bfbff|0x0000555555557381|0x000055555555738b+ Return address   |                  |                  |0x00007fffffffdb30| Shellcode address|Address Space Layout Randomization (ASLR)
0x00007fffffffdb00:0x00007fffffffdc38+                  +                  |                  |                  |                  | nop              |                  | so Shellcode address is an random range
0x00007fffffffdb08:0x0000000100000064|                  |                  |                  |                  |                  | nop              | S                | but if it hit the Sled
0x00007fffffffdb10:0x0000000000001000|                  |                  |                  |                  |                  | nop              | l                | then sledding to the shellcode
0x00007fffffffdb18:0x00005555555551c0|0x00000001555551c0|                  |                  |                  |                  | nop              | e                |
0x00007fffffffdb20:0x0000000000000001=                  =                  =                  |                  |                  | nop              | d                |
0x00007fffffffdb28:0x00007ffff7dadd90|                  |                  |                  |                  |                  | nop              |                  |
0x00007fffffffdb30:0x0000000000000000|                  |                  |                  |                  |                  |                  | shellcode        |
0x00007fffffffdb38:0x00005555555572f7|                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb40:0x00000001ffffdc20|                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb48:0x00007fffffffdc38|                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb50:0x0000000000000000|                  |                  |                  |                  |                  |                  |                  |
0x00007fffffffdb58:0xc63b70ed1f8679c1|0x8d02b4e4d20be196|                  |                  |                  |                  |                  |                  |

*/
